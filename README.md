# Airline Booking System (ABS)

Welcome to the Airline Booking System (✈️ABS✈️) project! ABS is a comprehensive system designed to facilitate airline management, flight booking, and passenger handling. This README file will guide you through the features, validations, testing methodologies, and functionalities of ABS.

## Project Overview

ABS enables clients, whether users or other software systems, to perform the following tasks:

- Create airports, airlines, and flights
- Associate airlines with their respective flights
- Define flight sections (e.g., first class, business class)
- Organize seats within flight sections

## Validations

### Validate Input

- **Airport Name:** Must consist of exactly three alphabetic characters.
- **Airline Name:** Must have a length less than 6.
- **Flight Identifier:** Must be a string of alphanumeric characters.

### Uniqueness Validation

- Ensures unique values for airlines, airports, and flights within their respective categories using linear search.

### Sorting

- **Airports:** Implemented with bubble sort.
- **Airlines and Flights:** Utilizes selection sort.

### Search

- Allows users to search for specific input data using binary search, printing whether the data corresponds to an Airport, Airline, Flight, or not found.

## Unit Testing

### Validation

- **Airports, Airlines, and Flights:** Unit tests developed following the Arrange-Act-Assert pattern.

### Sorting and Searching Algorithms

- **Bubble and Selection Sort:** Unit tests ensure correctness using the Arrange-Act-Assert pattern.
- **Linear and Binary Search:** Unit tests validate the functionality of these search algorithms.

### Performance Testing

- **Sorting Algorithms:** Pseudo performance test comparing bubble and selection sort, measuring execution time and iteration counts.
- **Search Algorithms:** Evaluates performance between binary and linear search, reporting the number of searches completed within a fixed time frame.

## Flight Route Finder

**Flight Network:** ABS provides a comprehensive flight network, enabling users to find available routes between airports efficiently.

## Logging and Permissions

ABS implements logging functionalities to track system activities and manage user permissions effectively.

## Persistence

The system ensures data persistence, allowing for the storage and retrieval of information across sessions.

## Getting Started

To start using ABS, follow these steps:

1. Clone the repository.
2. Install dependencies.
3. Run the application.

## Contributors

- [Nikola Hadzhiev](https://gitlab.com/NikolaHadzhiev)

---

Thank you for choosing ABS for your airline management needs. Should you have any questions or feedback, feel free to reach out to our team. Happy flying! ✈️🛬🌍