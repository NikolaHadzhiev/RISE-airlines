using Airlines.API.Helpers;
using Airlines.API.Middlewares;
using Airlines.Business.Services;
using Airlines.Persistence.Basic.DataContext;
using Airlines.Persistence.Basic.Repositories.AirlineRepository;
using Airlines.Persistence.Basic.Repositories.AirportRepository;
using Airlines.Persistence.Basic.Repositories.FlightRepository;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

// Check if the environment variable is already set
string runningLocally = Environment.GetEnvironmentVariable("RUNNING_LOCALLY")!;

// If the environment variable is not set, set it to true
if (string.IsNullOrEmpty(runningLocally))
{
    Environment.SetEnvironmentVariable("RUNNING_LOCALLY", "true");
}

// Check if running locally
bool isLocal = Environment.GetEnvironmentVariable("RUNNING_LOCALLY") == "true";

string connectionString;

if (isLocal)
{
    // Use the local connection string
    connectionString = builder.Configuration.GetConnectionString("RISE_Airlines_DB") ?? throw new InvalidOperationException("Connection string 'RISE_Airlines_DB' not found.");
}
else
{
    // Use the container connection string
    connectionString = builder.Configuration.GetConnectionString("RISE_Airlines_DB_Docker") ?? throw new InvalidOperationException("Connection string 'RISE_Airlines_DB_Dockeer' not found.");
}

// Add services to the container.
builder.Services.AddDbContext<RISEAirlinesContext>(options =>
    options.UseSqlServer(connectionString));

const string allowAnyOrigin = "_allowAnyOriginPolicy";

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: allowAnyOrigin,
                      policy =>
                      {
                          _ = policy.AllowAnyOrigin()
                                .AllowAnyMethod()
                                .AllowAnyHeader();
                      });
});

builder.Services.AddControllers();

builder.Services.AddAutoMapper(typeof(MappingProfiles).Assembly);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IAirportRepositoryDB, AirportRepositoryDB>();
builder.Services.AddScoped<IAirlineRepositoryDB, AirlineRepositoryDB>();
builder.Services.AddScoped<IFlightRepositoryDB, FlightRepositoryDB>();

builder.Services.AddScoped<IAirportService, AirportService>();
builder.Services.AddScoped<IAirlineService, AirlineService>();
builder.Services.AddScoped<IFlightService, FlightService>();

var app = builder.Build();

app.UseMiddleware<ExceptionMiddleware>();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(allowAnyOrigin);

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
