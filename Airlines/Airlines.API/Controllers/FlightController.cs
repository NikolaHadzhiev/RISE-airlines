﻿using Airlines.API.DTOs.Flight;
using Airlines.Business.Services;
using Airlines.Persistence.Basic.DTO;
using AirlinesWeb.Models.Airport;
using AirlinesWeb.Models.Flight;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable IDE0290 // Use primary constructor

namespace Airlines.API.Controllers;
public class FlightController : BaseController
{
    private readonly IAirportService _airportService;
    private readonly IFlightService _flightService;

    public FlightController(IAirportService airportService, IFlightService flightService)
    {
        _airportService = airportService;
        _flightService = flightService;
    }

    [HttpGet]
    public async Task<ActionResult<List<FlightDTO>>> GetAll()
    {
        var flights = await _flightService.GetAllFlightsFromDB();

        List<FlightViewModel> allFlightsDTO = flights.Select(x => new FlightViewModel
        {
            ID = x.ID,
            FlightNumber = x.FlightNumber,
            Price = x.Price,
            DepartureDateTime = x.DepartureDateTime,
            ArrivalDateTime = x.ArrivalDateTime,
            FromAirport = new AirportViewModel
            {
                ID = x.FromAirport.ID,
                City = x.FromAirport.City,
                Country = x.FromAirport.Country,
                Code = x.FromAirport.Code,
                Founded = x.FromAirport.Founded,
                Name = x.FromAirport.Name,
                RunwaysCount = x.FromAirport.RunwaysCount
            },
            ToAirport = new AirportViewModel
            {
                ID = x.ToAirport.ID,
                City = x.ToAirport.City,
                Country = x.ToAirport.Country,
                Code = x.ToAirport.Code,
                Founded = x.ToAirport.Founded,
                Name = x.ToAirport.Name,
                RunwaysCount = x.ToAirport.RunwaysCount
            }
        }).ToList();

        return Ok(allFlightsDTO);
    }

    [HttpGet("{id}", Name = "GetFlight")]
    public async Task<ActionResult<FlightDTO>> GetOne(int id)
    {
        var flight = await _flightService.GetFlightByIdFromDB(id);

        if (flight == null) return NotFound();

        FlightViewModel flightDTO = new FlightViewModel
        {
            ID = flight.ID,
            FlightNumber = flight.FlightNumber,
            Price = flight.Price,
            DepartureDateTime = flight.DepartureDateTime,
            ArrivalDateTime = flight.ArrivalDateTime,
            FromAirport = new AirportViewModel
            {
                ID = flight.FromAirport.ID,
                City = flight.FromAirport.City,
                Country = flight.FromAirport.Country,
                Code = flight.FromAirport.Code,
                Founded = flight.FromAirport.Founded,
                Name = flight.FromAirport.Name,
                RunwaysCount = flight.FromAirport.RunwaysCount
            },
            ToAirport = new AirportViewModel
            {
                ID = flight.ToAirport.ID,
                City = flight.ToAirport.City,
                Country = flight.ToAirport.Country,
                Code = flight.ToAirport.Code,
                Founded = flight.ToAirport.Founded,
                Name = flight.ToAirport.Name,
                RunwaysCount = flight.ToAirport.RunwaysCount
            }
        };

        return Ok(flightDTO);
    }

    [HttpPost]
    public async Task<ActionResult<FlightDTO>> Create([FromBody] FlightCreateDTO flightCreateModel)
    {
        var allAiports = await _airportService.GetAllAirportsFromDB();

        var flightViewModel = new FlightViewModel
        {
            Airports = allAiports.Select(a => new AirportViewModel
            {
                ID = a.ID,
                City = a.City,
                Country = a.Country,
                Code = a.Code,
                Founded = a.Founded,
                Name = a.Name,
                RunwaysCount = a.RunwaysCount
            }).ToList()
        };

        //Since we are not having logic for
        //AircraftModelId and AirlineId for the moment
        //I am assiging 15 as AircraftModelId and 17 for AirlineId for each record
        FlightDTO flightDTO = new FlightDTO(
           flightCreateModel.FlightNumber!,
           15,
           17,
           flightViewModel.Airports.FirstOrDefault(a => a.Name!.Contains(flightViewModel.FromAirportName!, StringComparison.OrdinalIgnoreCase))!.ID,
           flightViewModel.Airports.FirstOrDefault(a => a.Name!.Contains(flightViewModel.ToAirportName!, StringComparison.OrdinalIgnoreCase))!.ID,
           flightCreateModel.Price,
           flightCreateModel.DepartureDateTime,
           flightCreateModel.ArrivalDateTime
           );

        var result = await _flightService.AddFlightToDB(flightDTO);

        if (result != null) return CreatedAtRoute("GetFlight", flightDTO);

        return StatusCode(500, new ProblemDetails { Title = "Problem creating new flight" });
    }

    [HttpPut]
    public async Task<ActionResult<FlightDTO>> Update([FromBody] FlightUpdateDTO flightUpdateDTO)
    {
        var flight = await _flightService.GetFlightByIdFromDB(flightUpdateDTO.ID);

        if (flight == null) return NotFound();

        var allAiports = await _airportService.GetAllAirportsFromDB();

        var flightViewModel = new FlightViewModel
        {
            Airports = allAiports.Select(a => new AirportViewModel
            {
                ID = a.ID,
                City = a.City,
                Country = a.Country,
                Code = a.Code,
                Founded = a.Founded,
                Name = a.Name,
                RunwaysCount = a.RunwaysCount
            }).ToList()
        };

        FlightDTO flightDTO = new FlightDTO(
           flightUpdateDTO.FlightNumber!,
           17,
           15,
           flightViewModel.Airports.FirstOrDefault(a => a.Name!.Contains(flightViewModel.FromAirportName!, StringComparison.OrdinalIgnoreCase))!.ID,
           flightViewModel.Airports.FirstOrDefault(a => a.Name!.Contains(flightViewModel.ToAirportName!, StringComparison.OrdinalIgnoreCase))!.ID,
           flightUpdateDTO.Price,
           flightUpdateDTO.DepartureDateTime,
           flightUpdateDTO.ArrivalDateTime
           );

        var result = await _flightService.UpdateFlightToDB(flightDTO.ID, flightDTO);


        if (result != null) return Ok(flightDTO);

        return StatusCode(500, new ProblemDetails { Title = "Problem updating flight" });
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(int id)
    {
        var flight = await _flightService.GetFlightByIdFromDB(id);

        if (flight == null) return NotFound();

        var result = await _flightService.DeleteFlightFromDB(id);

        if (result) return Ok();

        return StatusCode(500, new ProblemDetails { Title = "Problem deleting flight" });
    }
}
