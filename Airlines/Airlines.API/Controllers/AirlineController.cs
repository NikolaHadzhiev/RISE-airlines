﻿using Airlines.Business.Services;
using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable IDE0290 // Use primary constructor

namespace Airlines.API.Controllers;
public class AirlineController : BaseController
{
    private readonly IAirlineService _airlineService;
    private readonly IMapper _mapper;

    public AirlineController(IAirlineService airlineService, IMapper mapper)
    {
        _mapper = mapper;
        _airlineService = airlineService;
    }

    [HttpGet]
    public async Task<ActionResult<List<AirlineDTO>>> GetAll()
    {
        var airlines = await _airlineService.GetAllAirlinesFromDB();
        var airlinesDTO = _mapper.Map<List<AirlineDTO>>(airlines);

        return Ok(airlinesDTO);
    }

    [HttpGet("{id}", Name = "GetAirline")]
    public async Task<ActionResult<AirlineDTO>> GetOne(int id)
    {
        var airline = await _airlineService.GetAirlineByIdFromDB(id);

        var airlineDTO = _mapper.Map<AirlineDTO>(airline);

        if (airlineDTO == null) return NotFound();

        return Ok(airlineDTO);
    }

    [HttpPost]
    public async Task<ActionResult<AirlineDTO>> Create([FromBody] AirlineDTO airlineDTO)
    {
        var airline = _mapper.Map<Airline>(airlineDTO);

        var result = await _airlineService.AddAirlineToDB(airlineDTO);

        if (result != null) return CreatedAtRoute("GetAirline", new { Id = airline.ID }, airlineDTO);

        return StatusCode(500, new ProblemDetails { Title = "Problem creating new airline" });
    }

    [HttpPut]
    public async Task<ActionResult<AirlineDTO>> Update([FromBody] AirlineDTO airlineDTO)
    {
        var airline = await _airlineService.GetAirlineByIdFromDB(airlineDTO.ID);

        if (airline == null) return NotFound();

        var result = await _airlineService.UpdateAirlineToDB(airlineDTO.ID, airlineDTO);

        var airlineViewDTO = _mapper.Map<AirlineDTO>(airline);

        if (result != null) return Ok(airlineViewDTO);

        return StatusCode(500, new ProblemDetails { Title = "Problem updating airport" });
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(int id)
    {
        var airline = await _airlineService.GetAirlineByIdFromDB(id);

        if (airline == null) return NotFound();

        var result = await _airlineService.DeleteAirlineFromDB(id);

        if (result) return Ok();

        return StatusCode(500, new ProblemDetails { Title = "Problem deleting airline" });
    }
}
