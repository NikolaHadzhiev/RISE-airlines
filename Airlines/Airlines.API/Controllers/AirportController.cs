﻿using Microsoft.AspNetCore.Mvc;
using Airlines.Business.Services;
using AutoMapper;
using Airlines.Persistence.Basic.EntitiesDB;
using Airlines.Persistence.Basic.DTO;

#pragma warning disable IDE0290 // Use primary constructor

namespace Airlines.API.Controllers
{
    public class AirportController : BaseController
    {
        private readonly IAirportService _airportService;
        private readonly IMapper _mapper;
        public AirportController(IAirportService airportService, IMapper mapper)
        {
            _mapper = mapper;
            _airportService = airportService;
        }

        [HttpGet]
        public async Task<ActionResult<List<AirportDTO>>> GetAll()
        {
            var airports = await _airportService.GetAllAirportsFromDB();
            var airportsDTO = _mapper.Map<List<AirportDTO>>(airports);

            return Ok(airportsDTO);
        }

        [HttpGet("{id}", Name = "GetAirport")]
        public async Task<ActionResult<AirportDTO>> GetOne(int id)
        {
            var airport = await _airportService.GetAirportByIdFromDB(id);

            var airportDTO = _mapper.Map<AirportDTO>(airport);

            if (airportDTO == null) return NotFound();

            return Ok(airportDTO);
        }

        [HttpPost]
        public async Task<ActionResult<AirportDTO>> Create([FromBody] AirportDTO airportDTO)
        {
            var airport = _mapper.Map<Airport>(airportDTO);

            var result = await _airportService.AddAirportToDB(airportDTO);

            if (result != null) return CreatedAtRoute("GetAirport", new { Id = airport.ID }, airportDTO);

            return StatusCode(500, new ProblemDetails { Title = "Problem creating new airport" });
        }

        [HttpPut]
        public async Task<ActionResult<AirportDTO>> Update([FromBody] AirportDTO airportDTO)
        {
            var airport = await _airportService.GetAirportByIdFromDB(airportDTO.ID);

            if (airport == null) return NotFound();

            var result = await _airportService.UpdateAirportToDB(airportDTO.ID, airportDTO);

            var airportViewDTO = _mapper.Map<AirportDTO>(airport);

            if (result != null) return Ok(airportViewDTO);

            return StatusCode(500, new ProblemDetails { Title = "Problem updating airport" });
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var airport = await _airportService.GetAirportByIdFromDB(id);

            if (airport == null) return NotFound();

            var result = await _airportService.DeleteAirportFromDB(id);

            if (result) return Ok();

            return StatusCode(500, new ProblemDetails { Title = "Problem deleting airport" });
        }
    }
}
