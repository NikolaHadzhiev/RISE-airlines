﻿using Airlines.Persistence.Basic.EntitiesDB;
using Airlines.Persistence.Basic.DTO;
using AutoMapper;

#pragma warning disable IDE0058 // Expression value is never used

namespace Airlines.API.Helpers;

public class MappingProfiles : Profile
{

    public MappingProfiles()
    {
        CreateMap<AirportDTO, Airport>();
        CreateMap<Airport, AirportDTO>();
        CreateMap<AirlineDTO, Airline>();
        CreateMap<Airline, AirlineDTO>();
        CreateMap<Flight, FlightDTO>();
        CreateMap<FlightDTO, Flight>();
    }
}
