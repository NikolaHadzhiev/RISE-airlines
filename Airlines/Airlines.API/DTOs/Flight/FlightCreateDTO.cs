﻿using AirlinesWeb.Models.Flight.Validation;
using System.ComponentModel.DataAnnotations;

namespace Airlines.API.DTOs.Flight;

public class FlightCreateDTO
{
    [Required(ErrorMessage = "Flight number is required")]
    [StringLength(6, ErrorMessage = "Flight number should be at most 6 characters long")]
    public string? FlightNumber { get; set; }

    [Required(ErrorMessage = "Price is required")]
    [Range(0.01, double.MaxValue, ErrorMessage = "Price should be a positive number")]
    public decimal Price { get; set; }

    [Required(ErrorMessage = "Departure date and time are required")]
    [DataType(DataType.DateTime)]
    [DepartureDateNotPast(ErrorMessage = "Departure date and time cannot be in the past")]
    public DateTime DepartureDateTime { get; set; }

    [Required(ErrorMessage = "Arrival date and time are required")]
    [DataType(DataType.DateTime)]
    [ArrivalDateNotPast(ErrorMessage = "Arrival date and time cannot be in the past")]
    public DateTime ArrivalDateTime { get; set; }

    [Required(ErrorMessage = "From airport name is required")]
    [RegularExpression(@"^[A-Za-z\s]+$", ErrorMessage = "From airport name should only contain letters and spaces")]
    public string? FromAirportName { get; set; }

    [Required(ErrorMessage = "To airport name is required")]
    [RegularExpression(@"^[A-Za-z\s]+$", ErrorMessage = "To airport name should only contain letters and spaces")]
    public string? ToAirportName { get; set; }
}
