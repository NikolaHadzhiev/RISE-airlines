﻿using Airlines.Persistence.Basic.Custom.Exceptions;
using Airlines.Persistence.Basic.Entities;
using System.Text.RegularExpressions;

namespace Airlines.Persistence.Basic;
public static class Validations
{
    internal static void IsAirportValid(string identifier, string name, string city, string country)
    {
        bool correctAirport = Regex.IsMatch(identifier, "^[a-zA-Z0-9]{2,4}$") &&
                              Regex.IsMatch(name, "^[a-zA-Z\\.'\\- ]+$") &&
                              Regex.IsMatch(city, "^[a-zA-Z\\.'\\- ]+$") &&
                              Regex.IsMatch(country, "^[a-zA-Z\\.'\\- ]+$") &&
                              !string.IsNullOrEmpty(identifier) &&
                              !string.IsNullOrEmpty(name) &&
                              !string.IsNullOrEmpty(city) &&
                              !string.IsNullOrEmpty(country);

        if (!correctAirport)
        {
            throw new InvalidAirportException($"Invalid airport with ID {identifier}!");
        }
    }

    internal static void IsAirlineValid(string name)
    {
        if (!Regex.IsMatch(name, "^.{1,5}$") && string.IsNullOrEmpty(name))
        {
            throw new InvalidAirlineException($"Invalid airline with name {name}!");
        }
    }

    internal static bool IsFlightValid(FlightCSV current, FlightCSV next) => current.ArrivalAirport == next.DepartureAirport;
}
