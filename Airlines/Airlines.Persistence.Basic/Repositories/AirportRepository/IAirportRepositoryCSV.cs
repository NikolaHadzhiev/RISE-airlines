﻿using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Repositories.AirportRepository;
public interface IAirportRepositoryCSV
{
    public (Dictionary<string, AirportCSV> airports,
            Dictionary<string, HashSet<AirportCSV>> airportsByCity,
            Dictionary<string, HashSet<AirportCSV>> airportsByCountry) ReadAirports(string filePath);
}
