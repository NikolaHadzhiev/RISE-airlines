﻿using Airlines.Persistence.Basic.Custom.Exceptions;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Repositories.AirportRepository;

#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable CA1854 // Prefer the 'IDictionary.TryGetValue(TKey, out TValue)' method

public class AirportRepositoryCSV : IAirportRepositoryCSV
{
    public (Dictionary<string, AirportCSV> airports, Dictionary<string, HashSet<AirportCSV>> airportsByCity, Dictionary<string, HashSet<AirportCSV>> airportsByCountry) ReadAirports(string filePath)
    {
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        using StreamReader reader = new StreamReader(filePath);

        // Skip the first line
        reader.ReadLine();

        while (!reader.EndOfStream)
        {
            string line = reader.ReadLine()!;
            string[] values = line.Split(',');


            string identifier = values[0].Trim();
            string name = values[1].Trim();
            string city = values[2].Trim();
            string country = values[3].Trim();

            // Validate properties
            try
            {
                Validations.IsAirportValid(identifier, name, city, country);
            }
            catch (InvalidAirportException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.ValidIdentifier);
                Console.WriteLine(ex.ValidName);
                Console.WriteLine(ex.ValidCountry);
                Console.WriteLine();

                continue;
            }

            if (airports.ContainsKey(identifier))
            {
                Console.WriteLine($"AirportCSV with such identifier already exists. Must be unique");
                Console.WriteLine();

                continue;
            }

            AirportCSV airport = AirportCSV.CreateAirport(identifier, name, city, country);
            airports.Add(identifier, airport);

            // Index airports by city
            if (!airportsByCity.ContainsKey(city))
            {
                airportsByCity[city] = [];
            }

            airportsByCity[city].Add(airport);

            // Index airports by country
            if (!airportsByCountry.ContainsKey(country))
            {
                airportsByCountry[country] = [];
            }

            airportsByCountry[country].Add(airport);
        }

        return (airports, airportsByCity, airportsByCountry);
    }
}



