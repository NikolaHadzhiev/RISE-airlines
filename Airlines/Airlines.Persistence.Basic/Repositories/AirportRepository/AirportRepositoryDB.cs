﻿using Airlines.Persistence.Basic.DataContext;
using Airlines.Persistence.Basic.EntitiesDB;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace Airlines.Persistence.Basic.Repositories.AirportRepository;

#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0058 // Expression value is never used

public class AirportRepositoryDB : IAirportRepositoryDB
{
    private readonly RISEAirlinesContext _context;

    public AirportRepositoryDB(RISEAirlinesContext context)
    {
        _context = context;
    }

    public async Task<List<Airport>> GetAllAirportsAsync()
    {
        try
        {
            List<Airport> allAirports = await _context.Airports.ToListAsync();

            return allAirports;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airports from the database: {ex.Message}");
            return [];
        }
    }

    public async Task<List<Airport>> GetAllAirportsAsync(string searchParam, string searchFilter)
    {
        try
        {
            IQueryable<Airport> query = _context.Airports;

            // Apply search filter based on the selected option
            switch (searchFilter)
            {
                case "Name":
                    query = query.Where(x => x.Name.Contains(searchParam));
                    break;

                case "City":
                    query = query.Where(x => x.City.Contains(searchParam));
                    break;

                case "Code":
                    query = query.Where(x => x.Code.Contains(searchParam));
                    break;

                case "Runways":
                    int runwaysCount;
                    if (int.TryParse(searchParam, out runwaysCount))
                    {
                        query = query.Where(x => x.RunwaysCount == runwaysCount);
                    }
                    else
                    {
                        return [];
                    }
                    break;

                case "Founded":

                    query = query.Where(x => x.Founded.ToString()!.Contains(searchParam.Replace("/", "-")));
                    break;

                default:
                    // No specific search filter selected, apply general search
                    query = query.Where(x => x.Name.Contains(searchParam)
                                            || x.Country.Contains(searchParam)
                                            || x.City.Contains(searchParam)
                                            || x.Code.Contains(searchParam)
                                            || x.RunwaysCount.ToString()!.Contains(searchParam)
                                            || x.Founded.ToString()!.Contains(searchParam.Replace("/", "-")));
                    break;
            }

            // Execute the query and return the results
            return await query.ToListAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airports from the database: {ex.Message}");
            return []; // Return an empty list on exception
        }
    }


    public async Task<Airport?> GetAirportByIdAsync(int id)
    {
        try
        {
            Airport? airport = await _context.Airports.FirstOrDefaultAsync(a => a.ID == id);

            return airport;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airport by id from the database: {ex.Message}");
            return null;
        }
    }

    public async Task<List<Airport>> GetAirportsByCountryAsync(string country)
    {
        try
        {
            List<Airport> airportsByCountry = await _context.Airports.Where(a => a.Country == country).ToListAsync();

            return airportsByCountry;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airports by country from the database: {ex.Message}");
            return [];
        }
    }

    public async Task<bool> AddAirportAsync(Airport airport)
    {
        try
        {
            await _context.Airports.AddAsync(airport);

            bool success = await _context.SaveChangesAsync() > 0;

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error adding airport into the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> UpdateAirportAsync(int id, Airport airport)
    {
        try
        {
            Airport? airportToUpdate = await _context.Airports.FirstOrDefaultAsync(a => a.ID == id);

            if (airportToUpdate is null) return false;

            airportToUpdate.Name = airport.Name;
            airportToUpdate.Country = airport.Country;
            airportToUpdate.City = airport.City;
            airportToUpdate.Code = airport.Code;
            airportToUpdate.RunwaysCount = airport.RunwaysCount;
            airportToUpdate.Founded = airport.Founded;

            _context.Airports.Update(airportToUpdate);

            bool success = await _context.SaveChangesAsync() > 0;

            return success;

        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating airport in the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> DeleteAirportAsync(int id)
    {
        try
        {
            Airport? airport = await _context.Airports.FirstOrDefaultAsync(a => a.ID == id);

            if (airport is null) return false;

            _context.Airports.Remove(airport);

            bool success = await _context.SaveChangesAsync() > 0;

            return success;

        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error deleting airport from the database: {ex.Message}");
            return false;
        }
    }
}
