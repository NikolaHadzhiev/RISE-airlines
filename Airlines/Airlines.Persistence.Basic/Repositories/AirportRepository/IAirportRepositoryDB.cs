﻿using Airlines.Persistence.Basic.EntitiesDB;

namespace Airlines.Persistence.Basic.Repositories.AirportRepository;
public interface IAirportRepositoryDB
{
    public Task<List<Airport>> GetAllAirportsAsync();
    public Task<List<Airport>> GetAllAirportsAsync(string searchParam, string searchFilter);
    public Task<List<Airport>> GetAirportsByCountryAsync(string country);
    public Task<Airport?> GetAirportByIdAsync(int id);
    public Task<bool> AddAirportAsync(Airport airport);
    public Task<bool> UpdateAirportAsync(int id, Airport airport);
    public Task<bool> DeleteAirportAsync(int id);

}
