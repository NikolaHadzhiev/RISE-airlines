﻿using Airlines.Persistence.Basic.Custom;
using Airlines.Persistence.Basic.Custom.Exceptions;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Repositories.FlightRouteRepository;
public class FlightRouteRepositoryCSV : IFlightRouteRepositoryCSV
{
    public List<string> ReadFlightRoute(string filePath, FlightLinkedList flights)
    {
        List<string> flightRouteList = [];

        using StreamReader reader = new StreamReader(filePath);

        string airportIdentifier = reader.ReadLine()!.Trim();

        flightRouteList.Add(airportIdentifier);

        while (!reader.EndOfStream)
        {
            string flightIdentifier = reader.ReadLine()!.Trim();

            FlightCSV flight = flights.SearchFlight(flightIdentifier)!;

            if (flight == null)
            {
                throw new InvalidFlightRouteIdentifierException($"FlightCSV with identifier {flightIdentifier} was not found");
            }
            else
            {
                if (flight.DepartureAirport != airportIdentifier)
                {
                    throw new InvalidFlightRouteIdentifierException($"Departure airport of flight {flightIdentifier} is not {airportIdentifier}");
                }
            }

            flightRouteList.Add(flightIdentifier);
        }

        return flightRouteList;
    }
}
