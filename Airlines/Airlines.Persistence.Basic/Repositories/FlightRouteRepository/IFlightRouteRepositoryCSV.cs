﻿using Airlines.Persistence.Basic.Custom;

namespace Airlines.Persistence.Basic.Repositories.FlightRouteRepository;
public interface IFlightRouteRepositoryCSV
{
    List<string> ReadFlightRoute(string filePath, FlightLinkedList flights);
}
