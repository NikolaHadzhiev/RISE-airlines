﻿using Airlines.Persistence.Basic.Configuration;
using Airlines.Persistence.Basic.Repositories.AirlineRepository;
using Airlines.Persistence.Basic.Repositories.AirportRepository;
using Airlines.Persistence.Basic.Repositories.FlightRepository;

namespace Airlines.Persistence.Basic.Repositories;

public class RepositoryFactory : IRepositoryFactory
{
    public IAirportRepositoryDB GetAirportRepository() => ConfigurationManager.GetAirportRepository();
    public IAirlineRepositoryDB GetAirlineRepository() => ConfigurationManager.GetAirlineRepository();
    public IFlightRepositoryDB GetFlightRepository() => ConfigurationManager.GetFlightRepository();
}
