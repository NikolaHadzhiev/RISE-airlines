﻿using Airlines.Persistence.Basic.Entities.Aircrafts;

namespace Airlines.Persistence.Basic.Repositories.AircraftRepository;

#pragma warning disable IDE0058 // Expression value is never used
public class AircraftRepositoryCSV : IAircraftRepositoryCSV
{
    public Dictionary<string, Aircraft> ReadAircrafts(string filePath)
    {
        Dictionary<string, Aircraft> aircrafts = [];

        using StreamReader reader = new StreamReader(filePath);

        // Skip the first line
        reader.ReadLine();

        while (!reader.EndOfStream)
        {
            string line = reader.ReadLine()!;
            string[] values = line.Split(',');

            string model = values[0].Trim();
            string cargoWeightStr = values[1].Trim();
            string cargoVolumeStr = values[2].Trim();
            string seatsStr = values[3].Trim();

            if (cargoWeightStr.Trim().Equals("-") && cargoVolumeStr.Trim().Equals("-") && !seatsStr.Trim().Equals("-"))
            {
                // Private Aircraft
                int seats = int.Parse(seatsStr);

                aircrafts.Add(model, PrivateAircraft.CreatePrivateAircraft(model, seats));
            }
            else if (!cargoWeightStr.Trim().Equals("-") && !cargoVolumeStr.Trim().Equals("-") && seatsStr.Trim().Equals("-"))
            {
                // Cargo Aircraft
                double cargoWeight = double.Parse(cargoWeightStr);
                double cargoVolume = double.Parse(cargoVolumeStr);

                aircrafts.Add(model, CargoAircraft.CreateCargoAircraft(model, cargoWeight, cargoVolume));
            }
            else
            {
                // Passenger Aircraft
                double cargoWeight = cargoWeightStr.Trim().Equals("-") ? 0 : double.Parse(cargoWeightStr);
                double cargoVolume = cargoVolumeStr.Trim().Equals("-") ? 0 : double.Parse(cargoVolumeStr);
                int seats = seatsStr.Trim().Equals("-") ? 0 : int.Parse(seatsStr);

                aircrafts.Add(model, PassengerAircraft.CreatePassengerAircraft(model, cargoWeight, cargoVolume, seats));
            }
        }

        return aircrafts;
    }
}
