﻿using Airlines.Persistence.Basic.Entities.Aircrafts;

namespace Airlines.Persistence.Basic.Repositories.AircraftRepository;
public interface IAircraftRepositoryCSV
{
    Dictionary<string, Aircraft> ReadAircrafts(string filePath);
}
