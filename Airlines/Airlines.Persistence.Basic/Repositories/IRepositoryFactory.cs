﻿using Airlines.Persistence.Basic.Repositories.AirlineRepository;
using Airlines.Persistence.Basic.Repositories.AirportRepository;
using Airlines.Persistence.Basic.Repositories.FlightRepository;

namespace Airlines.Persistence.Basic.Repositories;
public interface IRepositoryFactory
{
    IAirportRepositoryDB GetAirportRepository();
    IAirlineRepositoryDB GetAirlineRepository();
    IFlightRepositoryDB GetFlightRepository();
}
