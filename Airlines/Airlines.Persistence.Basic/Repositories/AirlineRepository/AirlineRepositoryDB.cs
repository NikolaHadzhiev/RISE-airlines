﻿using Airlines.Persistence.Basic.DataContext;
using Airlines.Persistence.Basic.EntitiesDB;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace Airlines.Persistence.Basic.Repositories.AirlineRepository;

#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0058 // Expression value is never used
public class AirlineRepositoryDB : IAirlineRepositoryDB
{
    private readonly RISEAirlinesContext _context;

    public AirlineRepositoryDB(RISEAirlinesContext context)
    {
        _context = context;
    }

    public async Task<List<Airline>> GetAllAirlinesAsync()
    {
        try
        {
            return await _context.Airlines.ToListAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines from the database: {ex.Message}");
            return [];
        }
    }

    public async Task<List<Airline>> GetAllAirlinesAsync(string searchParam, string searchFilter)
    {
        try
        {
            IQueryable<Airline> query = _context.Airlines;

            // Apply search filter based on the selected option
            switch (searchFilter)
            {
                case "Name":
                    query = query.Where(x => x.Name.Contains(searchParam));
                    break;
                case "Founded":
                    query = query.Where(x => x.Founded.ToString()!.Contains(searchParam.Replace("/", "-")));
                    break;
                case "Fleet_Size":
                    int fleetSize;
                    if (int.TryParse(searchParam, out fleetSize))
                    {
                        query = query.Where(x => x.FleetSize == fleetSize);
                    }
                    else
                    {
                        // Handle invalid input for fleet size
                        return [];
                    }
                    break;
                case "Description":
                    query = query.Where(x => x.Description.Contains(searchParam));
                    break;
                default:
                    // No specific search filter selected, apply general search
                    query = query.Where(x => x.Name.Contains(searchParam)
                                            || x.Founded.ToString()!.Contains(searchParam.Replace("/", "-"))
                                            || x.FleetSize.ToString().Contains(searchParam)
                                            || x.Description.Contains(searchParam));
                    break;
            }

            // Execute the query and return the results
            return await query.ToListAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines from the database: {ex.Message}");
            return []; // Return an empty list on exception
        }
    }


    public async Task<Airline?> GetAirlineByIdAsync(int id)
    {
        try
        {
            return await _context.Airlines.FirstOrDefaultAsync(a => a.ID == id);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airline from the database: {ex.Message}");
            return null;
        }
    }

    public async Task<bool> AddAirlineAsync(Airline airline)
    {
        try
        {
            await _context.Airlines.AddAsync(airline);

            bool success = await _context.SaveChangesAsync() > 0;

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error adding airline into the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> UpdateAirlineAsync(int id, Airline airline)
    {
        try
        {
            Airline? airlineToUpdate = await _context.Airlines.FirstOrDefaultAsync(a => a.ID == id);

            if (airlineToUpdate is null) return false;

            airlineToUpdate.Name = airline.Name;
            airlineToUpdate.Founded = airline.Founded;
            airlineToUpdate.FleetSize = airline.FleetSize;
            airlineToUpdate.Description = airline.Description;

            _context.Airlines.Update(airlineToUpdate);

            bool success = await _context.SaveChangesAsync() > 0;

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating airline in the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> DeleteAirlineAsync(int id)
    {
        try
        {
            Airline? airline = await _context.Airlines.FirstOrDefaultAsync(a => a.ID == id);

            if (airline is null) return false;

            _context.Airlines.Remove(airline);

            bool success = await _context.SaveChangesAsync() > 0;

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error deleting airline from the database: {ex.Message}");
            return false;
        }
    }
}
