﻿using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Repositories.AirlineRepository;

public interface IAirlineRepositoryCSV
{
    Dictionary<string, AirlineCSV> ReadAirlines(string filePath);
}
