﻿using Airlines.Persistence.Basic.Custom.Exceptions;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Repositories.AirlineRepository;

#pragma warning disable IDE0058 // Expression value is never used
public class AirlineRepositoryCSV : IAirlineRepositoryCSV
{
    public Dictionary<string, AirlineCSV> ReadAirlines(string filePath)
    {
        Dictionary<string, AirlineCSV> airlines = [];

        using StreamReader reader = new StreamReader(filePath);

        // Skip the first line
        reader.ReadLine();
        while (!reader.EndOfStream)
        {
            string line = reader.ReadLine()!;
            string[] values = line.Split(',');

            string identifier = values[0].Trim();
            string name = values[1].Trim();

            // Validate properties

            try
            {
                Validations.IsAirlineValid(name);
            }
            catch (InvalidAirlineException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.ValidName);
                Console.WriteLine();

                continue;
            }

            if (airlines.ContainsKey(name))
            {
                Console.WriteLine($"Airline with such name already exists. Must be unique");
                continue;
            }

            airlines.Add(name, AirlineCSV.CreateAirline(identifier, name));
        }

        return airlines;
    }
}
