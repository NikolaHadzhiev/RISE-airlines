﻿using Airlines.Persistence.Basic.EntitiesDB;

namespace Airlines.Persistence.Basic.Repositories.AirlineRepository;
public interface IAirlineRepositoryDB
{
    public Task<List<Airline>> GetAllAirlinesAsync();

    public Task<List<Airline>> GetAllAirlinesAsync(string searchParam, string searchFilter);
    public Task<Airline?> GetAirlineByIdAsync(int id);
    public Task<bool> AddAirlineAsync(Airline airline);
    public Task<bool> UpdateAirlineAsync(int id, Airline airline);
    public Task<bool> DeleteAirlineAsync(int id);
}
