﻿using Airlines.Persistence.Basic.Custom;
using Airlines.Persistence.Basic.Custom.Exceptions;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Entities.Aircrafts;

namespace Airlines.Persistence.Basic.Repositories.FlightRepository;

#pragma warning disable IDE0058 // Expression value is never used
public class FlightRepositoryCSV : IFlightRepositoryCSV
{
    public (FlightLinkedList flights, Dictionary<string, FlightCSV> flightsForTree)
        ReadFlights(string filePath, Dictionary<string, Aircraft> aircrafts)
    {
        FlightLinkedList flights = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];

        using StreamReader reader = new StreamReader(filePath);

        // Skip the first line
        reader.ReadLine();

        while (!reader.EndOfStream)
        {
            string line = reader.ReadLine()!;
            string[] values = line.Split(',');

            string identifier = values[0].Trim();
            string departureAirport = values[1].Trim();
            string arrivalAirport = values[2].Trim();
            string aircraftModel = values[3].Trim();
            double price;
            double time;

            try
            {
                bool priceSuccesfullyParsed = double.TryParse(values[4].Trim(), out price);
                bool timeSuccesfullyParsed = double.TryParse(values[5].Trim(), out time);

                if (priceSuccesfullyParsed && timeSuccesfullyParsed)
                {
                    price = Math.Abs(price);
                    time = Math.Abs(time);
                }
                else
                {
                    throw new InvalidFlightException($"FlightCSV {identifier} price/time not in valid format.");
                }

                FlightCSV flight = FlightCSV.CreateFlight(identifier, departureAirport, arrivalAirport, price, time);

                if (flightsForTree.ContainsKey(identifier))
                {
                    throw new InvalidFlightException($"FlightCSV {flight.Identifier} already exists in the list. It will be skipped.");
                }

                flightsForTree.Add(identifier, flight);

                // initialRead (true/false by default) is a flag indicating whether the flight data is being read for the first time from a file.
                flights.AddFlight(flight, aircrafts, aircraftModel, true);

            }
            catch (InvalidFlightException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine();
            }
        }

        return (flights, flightsForTree);
    }
}
