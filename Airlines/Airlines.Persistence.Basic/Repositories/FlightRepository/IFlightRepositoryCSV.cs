﻿using Airlines.Persistence.Basic.Entities.Aircrafts;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Persistence.Basic.Repositories.FlightRepository;
public interface IFlightRepositoryCSV
{
    (FlightLinkedList flights, Dictionary<string, FlightCSV> flightsForTree) ReadFlights(string filePath, Dictionary<string, Aircraft> aircrafts);
}
