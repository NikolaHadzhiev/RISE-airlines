﻿using Airlines.Persistence.Basic.EntitiesDB;
namespace Airlines.Persistence.Basic.Repositories.FlightRepository;
public interface IFlightRepositoryDB
{
    public Task<List<Flight>> GetAllFlightsAsync();
    public Task<List<Flight>> GetAllFlightsAsync(string searchParam, string searchFilter);
    public Task<Flight?> GetFlightByIdAsync(int id);
    public Task<bool> AddFlightAsync(Flight flight);
    public Task<bool> UpdateFlightAsync(int id, Flight flight);
    public Task<bool> UpdateFlightDepartureDateTimeAsync(int id, DateTime departureDateTime);
    public Task<bool> UpdateFlightArrivalDateTimeAsync(int id, DateTime arrivalDateTime);
    public Task<bool> UpdateFlightPriceAsync(int id, decimal price);
    public Task<bool> UpdateFlightAircraftModelAsync(int id, int airrcraftModelId);
    public Task<bool> DeleteFlightAsync(int id);

}
