﻿using Airlines.Persistence.Basic.DataContext;
using Airlines.Persistence.Basic.EntitiesDB;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace Airlines.Persistence.Basic.Repositories.FlightRepository;

#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0058 // Expression value is never used

public class FlightRepositoryDB : IFlightRepositoryDB
{
    private readonly RISEAirlinesContext _context;

    public FlightRepositoryDB(RISEAirlinesContext context)
    {
        _context = context;
    }

    public async Task<List<Flight>> GetAllFlightsAsync()
    {
        List<Flight> allFlights = await _context.Flights
                                                .Include(f => f.FromAirport)
                                                .Include(f => f.ToAirport)
                                                .ToListAsync();

        return allFlights;
    }

    public async Task<List<Flight>> GetAllFlightsAsync(string searchParam, string searchFilter)
    {
        try
        {
            IQueryable<Flight> query = _context.Flights
                .Include(f => f.FromAirport)
                .Include(f => f.ToAirport);

            // Apply search filter based on the selected option
            switch (searchFilter)
            {
                case "Flight_Number":
                    query = query.Where(x => x.FlightNumber.Contains(searchParam));
                    break;
                case "From":
                    query = query.Where(x => x.FromAirport.Name.Contains(searchParam));
                    break;
                case "To":
                    query = query.Where(x => x.ToAirport.Name.Contains(searchParam));
                    break;
                case "Price":
                    decimal price;
                    if (decimal.TryParse(searchParam, out price))
                    {
                        query = query.Where(x => x.Price == price);
                    }
                    else
                    {
                        // Handle invalid input for price
                        return [];
                    }
                    break;
                case "Departure":
                    query = query.Where(x => x.DepartureDateTime.ToString().Contains(searchParam.Replace("/", "-")));
                    break;
                case "Arrival":
                    query = query.Where(x => x.ArrivalDateTime.ToString().Contains(searchParam.Replace("/", "-")));
                    break;
                default:
                    // No specific search filter selected, apply general search
                    query = query.Where(x =>
                        x.FlightNumber.Contains(searchParam)
                        || x.FromAirport.Name.Contains(searchParam)
                        || x.ToAirport.Name.Contains(searchParam)
                        || x.Price.ToString().Contains(searchParam)
                        || x.DepartureDateTime.ToString().Contains(searchParam.Replace("/", "-"))
                        || x.ArrivalDateTime.ToString().Contains(searchParam.Replace("/", "-"))
                    );
                    break;
            }

            // Execute the query and return the results
            return await query.ToListAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting flights from the database: {ex.Message}");
            return []; // Return an empty list on exception
        }
    }


    public async Task<Flight?> GetFlightByIdAsync(int id)
    {
        try
        {
            return await _context.Flights
                                 .Include(f => f.FromAirport)
                                 .Include(f => f.ToAirport)
                                 .FirstOrDefaultAsync(f => f.ID == id);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting flight from the database: {ex.Message}");
            return null;
        }
    }

    public async Task<bool> AddFlightAsync(Flight flight)
    {
        try
        {
            await _context.Flights.AddAsync(flight);

            bool success = await _context.SaveChangesAsync() > 0;

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error adding flight into the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> UpdateFlightAsync(int id, Flight flight)
    {
        try
        {
            Flight? flightToUpdate = await _context.Flights.FirstOrDefaultAsync(f => f.ID == id);

            if (flightToUpdate is null) return false;

            flightToUpdate.AircraftModelID = flight.AircraftModelID;
            flightToUpdate.AirlineID = flight.AirlineID;
            flightToUpdate.FromAirportID = flight.FromAirportID;
            flightToUpdate.ToAirportID = flight.ToAirportID;
            flightToUpdate.Price = flight.Price;
            flightToUpdate.DepartureDateTime = flight.DepartureDateTime;
            flightToUpdate.ArrivalDateTime = flight.ArrivalDateTime;

            _context.Flights.Update(flightToUpdate);

            bool success = await _context.SaveChangesAsync() > 0;

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating flight in the database: {ex.Message}");
            return false;
        }
    }
    public async Task<bool> UpdateFlightDepartureDateTimeAsync(int id, DateTime departureDateTime)
    {
        try
        {
            Flight? flight = await _context.Flights.FirstOrDefaultAsync(f => f.ID == id);

            if (flight is null) return false;

            flight.DepartureDateTime = departureDateTime;

            bool success = await _context.SaveChangesAsync() > 0;

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating flight departure time in the database: {ex.Message}");
            return false;
        }
    }
    public async Task<bool> UpdateFlightArrivalDateTimeAsync(int id, DateTime arrivalDateTime)
    {
        try
        {
            Flight? flight = await _context.Flights.FirstOrDefaultAsync(f => f.ID == id);

            if (flight is null) return false;

            flight.ArrivalDateTime = arrivalDateTime;

            bool success = await _context.SaveChangesAsync() > 0;

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating flight arrival time in the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> UpdateFlightAircraftModelAsync(int id, int airrcraftModelId)
    {
        try
        {
            Flight? flight = await _context.Flights.FirstOrDefaultAsync(f => f.ID == id);

            if (flight is null) return false;

            flight.AircraftModelID = airrcraftModelId;

            bool success = await _context.SaveChangesAsync() > 0;

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating flight aircraft model in the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> UpdateFlightPriceAsync(int id, decimal price)
    {
        try
        {
            Flight? flight = await _context.Flights.FirstOrDefaultAsync(f => f.ID == id);

            if (flight is null) return false;

            flight.Price = price;

            bool success = await _context.SaveChangesAsync() > 0;

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating flight price in the database: {ex.Message}");
            return false;
        }
    }
    public async Task<bool> DeleteFlightAsync(int id)
    {
        try
        {
            Flight? flight = await _context.Flights.FirstOrDefaultAsync(f => f.ID == id);

            if (flight is null) return false;

            _context.Flights.Remove(flight);

            bool success = await _context.SaveChangesAsync() > 0;

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error deleting flight from the database: {ex.Message}");
            return false;
        }
    }
}
