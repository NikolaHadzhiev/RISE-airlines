﻿using Airlines.Persistence.Basic.Custom.Exceptions;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Entities.Aircrafts;
using System.Collections;

namespace Airlines.Persistence.Basic.Custom;
public class FlightLinkedList : IEnumerable<FlightCSV>
{
    public FlightCSV? Head;

    internal int Count()
    {
        int count = 0;

        FlightCSV? current = Head;
        while (current != null)
        {
            count++;
            current = current.NextFlight;
        }

        return count;
    }

    /// <summary>
    /// Adds a new flight to the route, assigning an aircraft if both aircrafts dictionary and aircraft model are provided.
    /// Ensures that the flight does not already exist, the aircraft model is valid, and the flight logically connects to the route.
    /// Provides console feedback about the operation's success or failure, except when flights are loaded initially from a file.
    /// </summary>
    /// <param name="flight">The flight object to be added.</param>
    /// <param name="aircrafts">An optional dictionary of aircrafts keyed by aircraft model. Used to assign an aircraft to the flight if the model is specified.</param>
    /// <param name="aircraftModel">An optional aircraft model identifier. If provided along with the aircrafts dictionary, the specified aircraft is assigned to the flight.</param>
    /// <param name="initialRead">A flag indicating whether the flight data is being read for the first time from a file. Suppresses console feedback if true.</param>
    /// <remarks>
    /// The method first checks if the route is empty and sets the flight as the head if true. 
    /// For subsequent flights, it checks for the flight's existence, valid aircraft model, and logical route connection before adding the flight to the route.
    /// Feedback is provided through the console for operations unless initialRead is true.
    /// </remarks>
    internal void AddFlight(FlightCSV flight,
                          Dictionary<string, Aircraft>? aircrafts = null,
                          string? aircraftModel = null,
                          bool initialRead = false)
    {
        if (Head == null)
        {
            Head = flight;

            if (aircrafts != null && aircraftModel != null)
            {
                flight.Aircraft = aircrafts[aircraftModel];
            }
        }
        else
        {
            FlightCSV? current = Head;

            bool flightAlreadyExists = false;

            while (current.NextFlight != null)
            {
                if (current.Identifier == flight.Identifier)
                {
                    flightAlreadyExists = true;
                }

                current = current.NextFlight;
            }

            //If only one node exists in the linked list
            if (current.Identifier == flight.Identifier)
            {
                flightAlreadyExists = true;
            }

            if (flightAlreadyExists)
            {
                throw new InvalidFlightException($"FlightCSV {flight.Identifier} already exists in the list. It will be skipped.");
            }

            if (aircrafts != null && aircraftModel != null && !aircrafts.ContainsKey(aircraftModel))
            {
                throw new InvalidFlightException($"Aircraft {aircraftModel} does not exist in the list of aircrafts. It will be skipped.");
            }

            if (!Validations.IsFlightValid(current, flight) && !initialRead)
            {
                throw new InvalidFlightException($"DepartureAirport of the new flight should match the ArrivalAirport of the last flight in the route");
            }

            if (aircrafts != null && aircraftModel != null)
            {
                flight.Aircraft = aircrafts[aircraftModel];
            }

            current.NextFlight = flight;
        }

        if (!initialRead)
        {
            Console.WriteLine($"{flight} succesfully added");
            Console.WriteLine();
        }
    }

    internal void RemoveFlightAtEnd()
    {
        if (Head == null)
        {
            Console.WriteLine("Route is empty. No flight to remove.");
            Console.WriteLine();

            return;
        }

        Console.WriteLine("Last flight removed from the route.");
        Console.WriteLine();

        if (Head.NextFlight == null)
        {
            Head = null;
            return;
        }

        FlightCSV? current = Head;

        while (current.NextFlight!.NextFlight != null)
        {
            current = current.NextFlight;
        }

        current.NextFlight = null;
    }

    internal FlightCSV? SearchFlight(string identifier)
    {
        FlightCSV? current = Head;

        while (current != null)
        {
            if (current.Identifier == identifier)
            {
                return current;
            }

            current = current.NextFlight;
        }

        return null;
    }

    internal void DisplayFlights()
    {
        if (Head == null)
        {
            Console.WriteLine("Route is empty.");
            Console.WriteLine();
            return;
        }

        FlightCSV? current = Head;

        Console.WriteLine();
        Console.WriteLine("Flights: ");
        Console.WriteLine();

        while (current != null)
        {
            Console.WriteLine($"{current}");
            current = current.NextFlight;
        }

        Console.WriteLine();
    }

    internal void ClearRoute() => Head = null;

    public IEnumerator<FlightCSV> GetEnumerator()
    {
        FlightCSV? current = Head;

        while (current != null)
        {
            yield return current;
            current = current.NextFlight;
        }
    }

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


    //public FlightLinkedList DeepCopy()
    //{
    //    FlightLinkedList copiedList = new FlightLinkedList();

    //    FlightCSV originalCurrent = Head!;
    //    FlightCSV? previous = null;
    //    FlightCSV? newHead = null;

    //    while (originalCurrent != null)
    //    {
    //        FlightCSV? newNode = new FlightCSV(originalCurrent.Identifier, originalCurrent.DepartureAirport, originalCurrent.ArrivalAirport);
    //        if (previous == null)
    //        {
    //            newHead = newNode;
    //            copiedList.Head = newHead;
    //        }
    //        else
    //        {
    //            previous.NextFlight = newNode;
    //        }

    //        previous = newNode;
    //        originalCurrent = originalCurrent.NextFlight;
    //    }

    //    return copiedList;
    //}
}
