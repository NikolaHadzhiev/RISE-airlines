﻿namespace Airlines.Persistence.Basic.Custom.Exceptions;
public class InvalidFlightRouteIdentifierException : Exception
{
    public InvalidFlightRouteIdentifierException() { }

    public InvalidFlightRouteIdentifierException(string message)
        : base(message) { }

    public InvalidFlightRouteIdentifierException(string message, Exception innerException)
        : base(message, innerException) { }
}
