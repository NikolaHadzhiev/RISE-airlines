﻿namespace Airlines.Persistence.Basic.Custom.Exceptions;
public class InvalidAircraftException : Exception
{
    public InvalidAircraftException() { }

    public InvalidAircraftException(string message)
        : base(message) { }

    public InvalidAircraftException(string message, Exception innerException)
        : base(message, innerException) { }
}
