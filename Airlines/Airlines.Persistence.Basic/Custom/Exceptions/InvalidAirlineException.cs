﻿namespace Airlines.Persistence.Basic.Custom.Exceptions;
public class InvalidAirlineException : Exception
{
    public string ValidName { get; } = "Name should be between 1 and 5 characters and not empty";

    public InvalidAirlineException() { }

    public InvalidAirlineException(string message)
        : base(message) { }

    public InvalidAirlineException(string message, Exception innerException)
        : base(message, innerException) { }
}
