﻿namespace Airlines.Persistence.Basic.Custom.Exceptions;
public class InvalidAirportException : Exception
{
    public string ValidIdentifier { get; } = "Identifier can contain only alphanumeric characters between 2 and 4 alphanumeric characters";
    public string ValidName { get; } = "Name can contain only alphabet and space characters";
    public string ValidCountry { get; } = "Country can contain only alphabet and space characters";

    public string UniqueAirport { get; } = "AirportCSV with such identifier already exists. Must be unique";

    public InvalidAirportException() { }

    public InvalidAirportException(string message)
        : base(message) { }

    public InvalidAirportException(string message, Exception innerException)
        : base(message, innerException) { }
}
