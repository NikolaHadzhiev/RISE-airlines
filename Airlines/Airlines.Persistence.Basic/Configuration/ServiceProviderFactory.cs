﻿using Airlines.Persistence.Basic.DataContext;
using Airlines.Persistence.Basic.Repositories.AirlineRepository;
using Airlines.Persistence.Basic.Repositories.AirportRepository;
using Airlines.Persistence.Basic.Repositories.FlightRepository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Airlines.Persistence.Basic.Configuration;

#pragma warning disable IDE0058 // Expression value is never used
public class ServiceProviderFactory
{
    private static IServiceProvider? _serviceProvider;
    public static IServiceProvider GetServiceProvider()
    {
        if (_serviceProvider == null)
        {
            string connectionString = ConfigurationManager.GetConnectionString("RISE_Airlines_DB")!;

            // Create service collection
            ServiceCollection services = new ServiceCollection();

            // Add services
            services.AddScoped<IAirportRepositoryDB, AirportRepositoryDB>(); // Add repository as a scoped service
            services.AddScoped<IAirlineRepositoryDB, AirlineRepositoryDB>(); // Add repository as a scoped service
            services.AddScoped<IFlightRepositoryDB, FlightRepositoryDB>(); // Add repository as a scoped service

            services.AddDbContext<RISEAirlinesContext>(options =>
                options.UseSqlServer(connectionString));

            // Build service provider
            _serviceProvider = services.BuildServiceProvider();
        }

        return _serviceProvider;
    }
}
