﻿using Airlines.Persistence.Basic.Repositories.AirlineRepository;
using Airlines.Persistence.Basic.Repositories.AirportRepository;
using Airlines.Persistence.Basic.Repositories.FlightRepository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Airlines.Persistence.Basic.Configuration;
public class ConfigurationManager
{
    private static readonly IConfigurationRoot _configuration;

    static ConfigurationManager()
    {
        string basePath = Path.Combine(AppContext.BaseDirectory, "..", "..", "..", "..", "Airlines.Persistence.Basic");

        // Build configuration once
        _configuration = new ConfigurationBuilder()
            .SetBasePath(basePath)
            .AddJsonFile("appsettings.json")
            .Build();
    }

    // Get connection string from configuration
    public static string? GetConnectionString(string name)
        => _configuration.GetConnectionString(name);

    public static IAirportRepositoryDB GetAirportRepository()
    {
        // Get service provider
        IServiceProvider serviceProvider = ServiceProviderFactory.GetServiceProvider();

        // Resolve repository
        IAirportRepositoryDB repository = serviceProvider.GetRequiredService<IAirportRepositoryDB>();

        return repository;
    }
    public static IAirlineRepositoryDB GetAirlineRepository()
    {
        // Get service provider
        IServiceProvider serviceProvider = ServiceProviderFactory.GetServiceProvider();

        // Resolve repository
        IAirlineRepositoryDB repository = serviceProvider.GetRequiredService<IAirlineRepositoryDB>();

        return repository;
    }

    public static IFlightRepositoryDB GetFlightRepository()
    {
        // Get service provider
        IServiceProvider serviceProvider = ServiceProviderFactory.GetServiceProvider();

        // Resolve repository
        IFlightRepositoryDB repository = serviceProvider.GetRequiredService<IFlightRepositoryDB>();

        return repository;
    }
}
