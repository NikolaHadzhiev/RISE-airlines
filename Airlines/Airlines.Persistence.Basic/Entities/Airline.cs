﻿namespace Airlines.Persistence.Basic.Entities;
public class AirlineCSV
{
    public string Identifier { get; }
    public string Name { get; }

    private AirlineCSV(string identifier, string name)
    {
        Identifier = identifier;
        Name = name;
    }

    public override string ToString() => $"{Identifier}-{Name}";

    internal static AirlineCSV CreateAirline(string identifier, string name) => new(identifier, name);
}
