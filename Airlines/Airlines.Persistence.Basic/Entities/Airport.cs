﻿namespace Airlines.Persistence.Basic.Entities;
public class AirportCSV
{
    public string Identifier { get; }
    public string Name { get; }
    public string City { get; }
    public string Country { get; }

    private AirportCSV(string identifier, string name, string city, string country)
    {
        Identifier = identifier;
        Name = name;
        City = city;
        Country = country;
    }

    public override string ToString() => $"{Identifier}-{Name}-{City}-{Country}";
    internal static AirportCSV CreateAirport(string identifier, string name, string city, string country) => new(identifier, name, city, country);
}
