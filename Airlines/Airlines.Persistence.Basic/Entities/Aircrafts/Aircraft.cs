﻿namespace Airlines.Persistence.Basic.Entities.Aircrafts;
public abstract class Aircraft
{
    public string? Model { get; protected set; }

    // Abstract method to display aircraft details
    public abstract override string ToString();
}
