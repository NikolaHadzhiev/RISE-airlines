﻿namespace Airlines.Persistence.Basic.Entities.Aircrafts;
public class CargoAircraft : Aircraft
{
    public double CargoWeight { get; set; }
    public double CargoVolume { get; set; }

    private CargoAircraft(string model, double cargoWeight, double cargoVolume)
    {
        Model = model;
        CargoWeight = cargoWeight;
        CargoVolume = cargoVolume;
    }
    public override string ToString() => $"{Model} - {CargoWeight} kg - {CargoVolume} m^3";

    internal static CargoAircraft CreateCargoAircraft(string model, double cargoWeight, double cargoVolume) => new(model, cargoWeight, cargoVolume);
}
