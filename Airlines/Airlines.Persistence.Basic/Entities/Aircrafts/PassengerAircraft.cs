﻿namespace Airlines.Persistence.Basic.Entities.Aircrafts;
public class PassengerAircraft : Aircraft
{
    public int Seats { get; set; }
    public double CargoWeight { get; set; }
    public double CargoVolume { get; set; }

    private PassengerAircraft(string model, double cargoWeight, double cargoVolume, int seats)
    {
        Model = model;
        CargoWeight = cargoWeight;
        CargoVolume = cargoVolume;
        Seats = seats;
    }
    public override string ToString() => $"{Model} - {CargoWeight} kg - {CargoVolume} m^3 - {Seats} seats";

    internal static PassengerAircraft CreatePassengerAircraft(
        string model, double cargoWeight, double cargoVolume, int seats) => new(model, cargoWeight, cargoVolume, seats);
}
