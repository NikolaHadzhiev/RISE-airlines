﻿namespace Airlines.Persistence.Basic.Entities.Aircrafts;
public class PrivateAircraft : Aircraft
{
    public int Seats { get; private set; }
    private PrivateAircraft(string model, int seats)
    {
        Model = model;
        Seats = seats;
    }
    public override string ToString() => $"{Model} - {Seats} seats";
    internal static PrivateAircraft CreatePrivateAircraft(string model, int seats) => new(model, seats);
}
