﻿using Airlines.Persistence.Basic.Entities.Aircrafts;

namespace Airlines.Persistence.Basic.Entities;
public class FlightCSV
{
    public string Identifier { get; }
    public string DepartureAirport { get; }
    public string ArrivalAirport { get; }
    public double Price { get; }
    public double Time { get; }
    public FlightCSV? NextFlight { get; set; }

    public Aircraft? Aircraft { get; set; }

    private FlightCSV(string identifier, string departureAirport, string arrivalAirport, double price, double time)
    {
        Identifier = identifier;
        DepartureAirport = departureAirport;
        ArrivalAirport = arrivalAirport;
        Price = price;
        Time = time;
    }

    public override string ToString() => $"FlightCSV {Identifier}: {DepartureAirport} to {ArrivalAirport}";

    internal static FlightCSV CreateFlight(string identifier, string departureAirport, string arrivalAirport, double price, double time)
        => new(identifier, departureAirport, arrivalAirport, price, time);
}
