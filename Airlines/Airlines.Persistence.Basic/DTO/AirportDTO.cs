﻿namespace Airlines.Persistence.Basic.DTO;

#pragma warning disable IDE0290 // Use primary constructor
public class AirportDTO
{
    public int ID { get; set; }

    public string? Name { get; set; }

    public string? Country { get; set; }

    public string? City { get; set; }

    public string? Code { get; set; }

    public int? RunwaysCount { get; set; }

    public DateOnly? Founded { get; set; }

    public AirportDTO(string? name, string? country, string? city, string? code, int? runwaysCount, DateOnly? founded)
    {
        Name = name;
        Country = country;
        City = city;
        Code = code;
        RunwaysCount = runwaysCount;
        Founded = founded;
    }
}
