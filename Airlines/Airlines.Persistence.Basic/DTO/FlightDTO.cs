﻿using Airlines.Persistence.Basic.EntitiesDB;
using System.ComponentModel.DataAnnotations.Schema;

namespace Airlines.Persistence.Basic.DTO;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
public class FlightDTO
{
    public int ID { get; set; }

    public string FlightNumber { get; set; }

    public int AircraftModelID { get; set; }

    public int AirlineID { get; set; }

    public int FromAirportID { get; set; }

    public int ToAirportID { get; set; }

    [Column(TypeName = "decimal(18, 0)")]
    public decimal Price { get; set; }

    public DateTime? DepartureDateTime { get; set; }

    public DateTime? ArrivalDateTime { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public virtual Aircraft_Model AircraftModel { get; set; }

    public virtual Airport FromAirport { get; set; }

    public virtual Airport ToAirport { get; set; }

    public FlightDTO(string flightNumber, int aircraftModelID, int airlineID, int fromAirportID, int toAirportID, decimal price, DateTime? departureDateTime, DateTime? arrivalDateTime)
    {
        FlightNumber = flightNumber;
        AircraftModelID = aircraftModelID;
        AirlineID = airlineID;
        FromAirportID = fromAirportID;
        ToAirportID = toAirportID;
        Price = price;
        DepartureDateTime = departureDateTime;
        ArrivalDateTime = arrivalDateTime;
    }

    public FlightDTO(int aircraftModelID, int airlineID, int fromAirportID, int toAirportID, decimal price, DateTime? departureDateTime, DateTime? arrivalDateTime)
    {
        AircraftModelID = aircraftModelID;
        AirlineID = airlineID;
        FromAirportID = fromAirportID;
        ToAirportID = toAirportID;
        Price = price;
        DepartureDateTime = departureDateTime;
        ArrivalDateTime = arrivalDateTime;
    }
}
