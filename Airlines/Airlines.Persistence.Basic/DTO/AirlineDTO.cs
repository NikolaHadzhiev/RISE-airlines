﻿namespace Airlines.Persistence.Basic.DTO;

#pragma warning disable IDE0290 // Use primary constructor
public class AirlineDTO
{
    public int ID { get; set; }

    public string? Name { get; set; }

    public DateOnly? Founded { get; set; }

    public int FleetSize { get; set; }

    public string? Description { get; set; }

    public AirlineDTO(string? name, DateOnly? founded, int fleetSize, string? description)
    {
        Name = name;
        Founded = founded;
        FleetSize = fleetSize;
        Description = description;
    }
}
