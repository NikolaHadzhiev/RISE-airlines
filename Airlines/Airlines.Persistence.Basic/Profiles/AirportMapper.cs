﻿using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;
using AutoMapper;

namespace Airlines.Persistence.Basic.Profiles;

#pragma warning disable IDE0058 // Expression value is never used

public class AirportMapper
{
    private readonly IMapper _mapper;
    public AirportMapper()
    {
        MapperConfiguration mapperConfig = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Airport, AirportDTO>().ReverseMap();

        });

        _mapper = mapperConfig.CreateMapper();
    }
    public Airport MapAirport(AirportDTO airportDTO)
    {
        Airport airport = _mapper.Map<Airport>(airportDTO);

        return airport;
    }
}
