﻿using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;
using AutoMapper;

namespace Airlines.Persistence.Basic.Profiles;

#pragma warning disable IDE0058 // Expression value is never used
public class AirlineMapper
{
    private readonly IMapper _mapper;
    public AirlineMapper()
    {
        MapperConfiguration mapperConfig = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Airline, AirlineDTO>().ReverseMap();
        });

        _mapper = mapperConfig.CreateMapper();
    }
    public Airline MapAirline(AirlineDTO airlineDTO)
    {
        Airline airline = _mapper.Map<Airline>(airlineDTO);

        return airline;
    }
}
