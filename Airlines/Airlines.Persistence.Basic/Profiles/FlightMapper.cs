﻿using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;
using AutoMapper;

namespace Airlines.Persistence.Basic.Profiles;

#pragma warning disable IDE0058 // Expression value is never used
public class FlightMapper
{
    private readonly IMapper _mapper;

    public FlightMapper()
    {
        MapperConfiguration mapperConfig = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Flight, FlightDTO>().ReverseMap();
            cfg.CreateMap<Airport, AirportDTO>().ReverseMap();
        });

        _mapper = mapperConfig.CreateMapper();
    }
    public Flight MapFlight(FlightDTO flightDto)
    {
        Flight flight = _mapper.Map<Flight>(flightDto);

        return flight;
    }
}
