USE RISE_Airlines;

-- Create nonclustered indexes for Airports
CREATE NONCLUSTERED INDEX IX_Airport_Name ON Airports (Name);
CREATE NONCLUSTERED INDEX IX_Airport_Code ON Airports (Code);

-- Create nonclustered index for Airlines
CREATE NONCLUSTERED INDEX IX_Airline_Name ON Airlines (Name);

-- Create nonclustured indexes for Flights
CREATE NONCLUSTERED INDEX IX_Flight_FlightNumber ON Flights (FlightNumber);
CREATE NONCLUSTERED INDEX IX_Flight_FromAirportId ON Flights (FromAirportID);
CREATE NONCLUSTERED INDEX IX_Flight_ToAirportId ON Flights (ToAirportID);
