-- Insert data for Flights
USE RISE_Airlines;

INSERT INTO Flights (FlightNumber, FromAirportID, ToAirportID, AircraftModelID, Price, DepartureDateTime, ArrivalDateTime, AirlineID) VALUES
('FL111', 10, 17, 1, 350, '2024-06-19 08:00:00', '2024-06-19 14:00:00', 1), 
('FL100', 2, 17, 7, 180,  '2024-06-19 09:00:00', '2024-06-19 17:00:00', 2),
('FL110', 2, 12, 8, 280,  '2024-06-19 10:00:00', '2024-06-19 14:00:00', 3),
('FL101', 1, 2, 8, 280,  '2024-06-19 11:00:00', '2024-06-19 15:00:00', 4),
('FL122', 9, 1, 15, 220,  '2024-06-19 12:00:00', '2024-06-19 15:00:00', 5),
('FL123', 11, 1, 12, 200,  '2024-06-19 13:00:00', '2024-06-19 15:00:00', 6),
('FL133', 1, 15, 3, 360, '2024-06-19 14:00:00', '2024-06-19 18:30:00', 7),
('FL541', 12, 21, 18, 550, '2024-06-20 15:00:00', '2024-06-20 22:30:00', 8),
('FL626', 14, 9, 16, 500, '2024-06-20 16:00:00', '2024-06-20 22:30:00', 9),
('FL890', 3, 19, 17, 380, '2024-06-20 17:00:00', '2024-06-20 22:00:00', 10),
('FL931', 14, 19, 1, 350, '2024-06-20 18:00:00', '2024-06-21 00:00:00', 11),
('FL535', 3, 21, 11, 350, '2024-06-20 19:00:00', '2024-06-21 01:00:00', 12),
('FL120', 14, 21, 4, 280, '2024-06-20 20:00:00', '2024-06-21 00:00:00', 13),
('FL804', 9, 6, 3, 360, '2024-06-20 21:00:00', '2024-06-21 01:30:00', 14),
('FL457', 4, 18, 10, 330, '2024-06-20 22:00:00', '2024-06-21 01:30:00', 15),
('FL785', 5, 12, 2, 200, '2024-06-15 23:00:00', '2024-06-16 01:00:00', 16),
('FL308', 5, 13, 14, 330, '2024-06-16 00:00:00', '2024-06-16 03:30:00', 17),
('FL768', 18, 20, 19, 360, '2024-06-16 01:00:00', '2024-06-16 05:30:00', 18),
('FL870', 20, 8, 2, 200, '2024-06-16 02:00:00', '2024-06-16 04:00:00', 19),
('FL989', 16, 7, 4, 280, '2024-06-16 03:00:00', '2024-06-16 07:00:00', 20),
('FL209', 11, 18, 13, 380, '2024-06-16 04:00:00', '2024-06-16 09:00:00', 20),
('FL404', 18, 16, 6, 500, '2024-06-16 05:00:00', '2024-06-16 11:30:00', 19),
('FL303', 8, 15, 20, 380, '2024-06-16 06:00:00', '2024-06-16 13:00:00', 18),
('FL777', 7, 15, 5, 400, '2024-06-16 07:00:00', '2024-06-16 12:30:00', 17),
('FL609', 20, 17, 9, 400, '2024-06-16 08:00:00', '2024-06-16 13:30:00', 16),
('FL299', 13, 20, 12, 200, '2024-06-16 09:00:00', '2024-06-16 11:00:00', 15),
('FL103', 18, 9, 16, 500, '2024-06-16 10:00:00', '2024-06-16 16:30:00', 14),
('FL811', 11, 7, 18, 550, '2024-06-16 11:00:00', '2024-06-16 18:30:00', 13),
('FL999', 10, 17, 1, 350, '2024-04-20 08:00:00', '2024-04-20 14:00:00', 1);

-- Select data with where clause
SELECT * FROM Flights WHERE FromAirportID = 1;

SELECT * FROM Flights;

SELECT FlightNumber, FromAirportId, ToAirportID FROM Flights;

-- Update final destination for one of the flights
UPDATE Flights SET ToAirportID = 2 WHERE FlightNumber = 'FL626';

-- Delete data for one of the flights
DELETE FROM Flights WHERE FlightNumber = 'FL123';

-- Show the count of all departed flights
SELECT COUNT(*) AS DepartedFlightsCount
FROM dbo.Flights f 
WHERE f.DepartureDateTime < GETDATE();

-- Show the most farther flight
SELECT
    f.ID,
    f.FlightNumber,
	f.Price,
    DATEDIFF(MINUTE, f.DepartureDateTime, f.ArrivalDateTime) AS DurationMinutes,
    depAirport.Code AS DepartureAirport,
    arrAirport.Code AS ArrivalAirport,
	am.[Name] AS AircraftModel,
    ase.Seats AS TotalSeats,
    asd.[Weight] AS AircraftWeight,
    asd.Volume AS AircraftVolume,
	al.[Name] as AirlineName

FROM
    dbo.Flights f
INNER JOIN
    dbo.Airports depAirport ON f.FromAirportID = depAirport.ID
INNER JOIN
    dbo.Airports arrAirport ON f.ToAirportID = arrAirport.ID
INNER JOIN
	dbo.Aircraft_Models am ON f.AircraftModelID = am.ID
INNER JOIN dbo.Airlines al ON f.AirlineID = al.ID
LEFT JOIN dbo.Aircraft_Seats ase ON f.AircraftModelID = ase.ID
LEFT JOIN dbo.Aircraft_Space_Dimentions asd ON f.AircraftModelID = asd.ID

WHERE
    DATEDIFF(MINUTE, f.DepartureDateTime, f.ArrivalDateTime) = (
        SELECT MAX(DATEDIFF(MINUTE, DepartureDateTime, ArrivalDateTime))
        FROM dbo.Flights
    );

-- Show all the flights scheduled for tomorrow with all possible details for Airports and Flights
SELECT 
	f.ID AS FlightID,
	f.FlightNumber,
	f.DepartureDateTime,
	f.ArrivalDateTime,
	f.Price,
	am.Name AS AircraftModel,
    ase.Seats AS TotalSeats,
    asd.Weight AS AircraftWeight,
    asd.Volume AS AircraftVolume,
	depAirport.Name AS DepartureAirportName,
    depAirport.Country AS DepartureAirportCountry,
	depAirport.City AS DepartureAirportCity,
	depAirport.Code AS DepartureAirportCode,
	depAirport.RunwaysCount AS DepartureAiportRunwaysCount,
	depAirport.Founded AS DepartureAirportFounded,
	arrAirport.Name AS ArrivalAirportName,
    arrAirport.Country AS ArrivalAirportCountry,
	arrAirport.City AS ArrivalAirportCity,
	arrAirport.Code AS ArrivalAirportCode,
	arrAirport.RunwaysCount AS ArrivalAiportRunwaysCount,
	arrAirport.Founded AS ArrivalAirportFounded,
	al.Name AS AirlineName
FROM 
	dbo.Flights f
INNER JOIN
    dbo.Airports depAirport ON f.FromAirportID = depAirport.ID
INNER JOIN
    dbo.Airports arrAirport ON f.ToAirportID = arrAirport.ID
INNER JOIN
	dbo.Aircraft_Models am ON f.AircraftModelID = am.ID
INNER JOIN 
	dbo.Airlines al ON f.AirlineID = al.ID
LEFT JOIN 
	dbo.Aircraft_Seats ase ON f.AircraftModelID = ase.ID
LEFT JOIN 
	dbo.Aircraft_Space_Dimentions asd ON f.AircraftModelID = asd.ID
WHERE CONVERT(DATE, f.DepartureDateTime) = DATEADD(DAY, 1, CONVERT(DATE, GETDATE()));
