-- Constraints to ensure that the departure and arrival time are not in the past
USE RISE_Airlines;

ALTER TABLE Flights
ADD CONSTRAINT CHK_DepartuteDateTime_NotPast CHECK (DepartureDateTime >= GETDATE());

ALTER TABLE Flights
ADD CONSTRAINT CHK_ArrivalDateTime_NotPast CHECK (ArrivalDateTime >= GETDATE());

-- Constraints to ensure that the arrival time is not before departure time
ALTER TABLE Flights
ADD CONSTRAINT CHK_ArrivalDateTime_After_DepartureDateTime CHECK (ArrivalDateTime > DepartureDateTime);