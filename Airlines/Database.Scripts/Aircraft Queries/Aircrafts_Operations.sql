-- Insert data for Aircraft tables
USE RISE_Airlines;

INSERT INTO Aircraft_Models ([Name]) VALUES 
('Bombardier Challenger 300'),
('Gulfstream G650'),
('Cessna Citation X'),
('Beechcraft King Air 350i'),
('Dassault Falcon 7X'),
('Pilatus PC-12'),
('Boeing 787-9'),
('Boeing 747-8F'),
('Boeing 777F'),
('Antonov An-124'),
('Airbus A330-200F'),
('Lockheed C-130 Hercules'),
('Airbus A350-900F'),
('Boeing 767-300F'),
('Airbus A320'),
('Embraer E175'),
('Airbus A380F'),
('Boeing 737-800'),
('Cessna 172'),
('Boeing 737-700C');

INSERT INTO Aircraft_Space_Dimentions (ID, [Weight], Volume) VALUES
(7, 116000, 618),
(8, 140000, 854.5),
(9, 102000, 683),
(10, 150000, 1300),
(11, 69000, 475),
(12, 20000, 92.9),
(13, 108000, 370),
(14, 140000, 438),
(15, 20000, 37.4),
(16, 16000, 26.9),
(17, 150000, 1400),
(18, 20000, 41.8),
(19, 30000, 390),
(20, 20000, 41.8);

INSERT INTO Aircraft_Seats (ID, Seats) VALUES
(1, 9),
(2, 18),
(3, 12),
(4, 9),
(5, 16),
(6, 9),
(15, 150),
(16, 76),
(17, 204),
(18, 189),
(19, 195),
(20, 126);
