-- Create Aircrafts tables
USE RISE_Airlines;

DROP TABLE IF EXISTS Aircraft_Models
CREATE TABLE Aircraft_Models(
    ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
    [Name] VARCHAR(256) NOT NULL
);

DROP TABLE IF EXISTS Aircraft_Seats
CREATE TABLE Aircraft_Seats (
    ID INT PRIMARY KEY NOT NULL,
    Seats INT NOT NULL
);

DROP TABLE IF EXISTS Aircraft_Space_Dimentions
CREATE TABLE Aircraft_Space_Dimentions (
    ID INT PRIMARY KEY NOT NULL,
    [Weight] DECIMAL NOT NULL,
    Volume DECIMAL NOT NULL
);

ALTER TABLE Aircraft_Seats
ADD FOREIGN KEY (ID) REFERENCES Aircraft_Models(ID);

ALTER TABLE Aircraft_Space_Dimentions
ADD FOREIGN KEY (ID) REFERENCES Aircraft_Models(ID);
