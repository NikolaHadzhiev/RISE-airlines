-- Create Airlines table
USE RISE_Airlines;

DROP TABLE IF EXISTS Airlines;
CREATE TABLE Airlines (
    ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
    [Name] VARCHAR(5) NOT NULL,
    Founded DATE,
    FleetSize INT NOT NULL,
    [Description] NVARCHAR(1000),
	CreatedDate DATETIME2 DEFAULT GETDATE(),
	ModifiedDate DATETIME2 DEFAULT GETDATE()
);