-- Create Airports table
USE RISE_Airlines;

DROP TABLE IF EXISTS Airports;
CREATE TABLE Airports (
    ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
    [Name] VARCHAR(255) NOT NULL,
    Country VARCHAR(100) NOT NULL,
    City VARCHAR(255) NOT NULL,
    Code CHAR(3) NOT NULL,
    RunwaysCount INT,
    Founded DATE,
	CreatedDate DATETIME2 DEFAULT GETDATE(),
	ModifiedDate DATETIME2 DEFAULT GETDATE()
);