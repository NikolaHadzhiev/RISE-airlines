-- Insert data for Airports
USE RISE_Airlines;

INSERT INTO Airports ("Name", Country, City, Code, RunwaysCount, Founded) VALUES
('Dallas Fort Worth International Airport', 'USA', 'Dallas', 'DFW', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('John F Kennedy International Airport', 'USA', 'New York', 'JFK', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Los Angeles International Airport', 'USA', 'Los Angeles', 'LAX', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Heathrow Airport', 'UK', 'London', 'LHR', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Charles de Gaulle Airport', 'France', 'Paris', 'CDG', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Dubai International Airport', 'UAE', 'Dubai', 'DXB', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Singapore Changi Airport', 'Singapore', 'Singapore', 'SIN', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Haneda Airport', 'Japan', 'Tokyo', 'HND', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Hartsfield Jackson Atlanta International Airport', 'USA', 'Atlanta', 'ATL', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('O Hare International Airport', 'USA', 'Chicago', 'ORD', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Amsterdam Airport Schiphol', 'Netherlands', 'Amsterdam', 'AMS', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Frankfurt Airport', 'Germany', 'Frankfurt', 'FRA', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Beijing Capital International Airport', 'China', 'Beijing', 'PEK', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Denver International Airport', 'USA', 'Denver', 'DEN', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Incheon International Airport', 'South Korea', 'Incheon', 'ICN', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Munich Airport', 'Germany', 'Munich', 'MUC', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Suvarnabhumi Airport', 'Thailand', 'Bangkok', 'BKK', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('OR Tambo International Airport', 'South Africa', 'Johannesburg', 'JNB', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Sydney Airport', 'Australia', 'Sydney', 'SYD', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Indira Gandhi International Airport', 'India', 'Delhi', 'DEL', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE())),
('Nelson Mandela International Airport', 'South Africa', 'Cape Town', 'KPT', ROUND(RAND() * 5, 0) + 2, DATEADD(YEAR, -ROUND(RAND() * 10, 0), GETDATE()));

SELECT * FROM Airports Where City = 'London';