﻿using Airlines.Business.Constants;
using Airlines.Persistence.Basic.Custom.Exceptions;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Entities.Aircrafts;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Business.Utils.FileDataUtilsMethods;

#pragma warning disable IDE0028 // Simplify collection initialization
public static class FileDataUtils
{
    internal static void ReadDataFromFileAirport(
        out Dictionary<string, AirportCSV>? data,
        out Dictionary<string, HashSet<AirportCSV>>? firstSearchCritereia,
        out Dictionary<string, HashSet<AirportCSV>>? secondSearchCriteria)

        => ReadUtils.ReadAirportData(PROJECT_CONSTANTS.AIRPORTS_FILE_PATH, out data, out firstSearchCritereia, out secondSearchCriteria);

    internal static void ReadDataFromFileAirline(out Dictionary<string, AirlineCSV>? data)

        => ReadUtils.ReadAirlineData(PROJECT_CONSTANTS.AIRLINES_FILE_PATH, out data);

    internal static void ReadDataFromFileFlight(out FlightLinkedList? data,
                                                Dictionary<string, Aircraft> aircrafts,
                                                out Dictionary<string, FlightCSV>? flightsForTree)

        => ReadUtils.ReadFlightData(PROJECT_CONSTANTS.FLIGHTS_FILE_PATH, out data, aircrafts, out flightsForTree);

    internal static void ReadDataFromAircraft(out Dictionary<string, Aircraft>? data)

        => ReadUtils.ReadAircraftData(PROJECT_CONSTANTS.AIRCRAFTS_FILE_PATH, out data);

    internal static void ReadDataFromFlightRoute(out List<string> data, FlightLinkedList flights)
    {
        data = new List<string>();

        try
        {
            ReadUtils.ReadFlightRouteData(PROJECT_CONSTANTS.FLIGHT_ROUTE_FILE_PATH, out data, flights);
        }
        catch (InvalidFlightRouteIdentifierException ex)
        {
            Console.WriteLine(ex.Message);
            Console.WriteLine();
        }
    }

    internal static void PrintDictionaryData<T>(Dictionary<string, T> data)
    {
        Console.WriteLine($"{typeof(T).Name} count: {data.Count}");

        Console.WriteLine();

        foreach ((string _, T value) in data)
        {
            Console.WriteLine($"- {value}");
        }

        Console.WriteLine();
    }

    internal static void PrintHashSetData<T>(HashSet<T> data)
    {
        Console.WriteLine();

        foreach (T value in data)
        {
            Console.WriteLine($"{value}");
        }

        Console.WriteLine();
    }

    internal static void PrintListData<T>(List<T> data)
    {
        Console.WriteLine();

        foreach (T value in data)
        {
            Console.WriteLine($"{value}");
        }

        Console.WriteLine();
    }
}
