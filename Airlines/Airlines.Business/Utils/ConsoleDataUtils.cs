﻿using Airlines.Business.Extensions;
using System.Text.RegularExpressions;

namespace Airlines.Business.Utils.ConsoleDataUtils;

#pragma warning disable IDE1006 // Naming Styles

public static class ConsoleDataUtils
{
    private const int START_INDEX = 6;
    private const string AIRPORTS_STRING = "Airports";
    private const string AIRLINES_STRING = "Airlines";
    private const string FLIGHTS_STRING = "Flights";
    private static readonly char[] SEPARATOR = [' '];

    // Function to read input data for a specific data type
    internal static T[] ReadDataFromConsole<T>(string dataType) where T : class
    {
        Console.WriteLine($"Enter {dataType} (type 'End {dataType}' to finish reading {dataType}): ");
        T[] data = [];
        int count = 0;

        while (true)
        {
            string input = Console.ReadLine()!;

            if (Regex.Replace(input, @"\s+", "").Equals($"End{dataType}", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine();
                break;
            }

            string[] dataStringArray = data.Select(x => x.ToString()).ToArray()!;

            switch (dataType)
            {
                case AIRPORTS_STRING:
                    if (!input.IsAirportValid())
                    {
                        continue;
                    }
                    break;
                case AIRLINES_STRING:
                    if (!input.IsAirlineValid())
                    {
                        continue;
                    }
                    break;
                default:
                    if (!input.IsFlightValid())
                    {
                        continue;
                    }
                    break;
            }

            if (!dataStringArray.IsUnique(input))
            {
                continue;
            }

            // Creates an instance of type T - (Airplane, Airline, FlightCSV)
            T item = (T)Activator.CreateInstance(typeof(T), input)!;

            Array.Resize(ref data, data.Length + 1);
            data[count++] = item;

            count++;
        }

        return data;
    }

    // Function to search for user input data
    internal static void SortAndDisplayDataFromConsole(string[] airportsToSort, string[] airlinesToSort, string[] flightsToSort)
    {
        Console.WriteLine("Enter command sort <input data> [order] (order is optional - 'ascending'/'descending') " +
                          "to sort input data (type 'End Sort' to finish sorting): ");

        while (true)
        {
            string sortData = Console.ReadLine()!;

            if (Regex.Replace(sortData, @"\s+", "").Equals($"EndSort", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine();
                break;
            }

            if (!sortData.StartsWith("sort", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Invalid sort command.");
                continue;
            }

            string[] tokens = sortData.Split(SEPARATOR, StringSplitOptions.RemoveEmptyEntries);

            if (tokens.Length < 2)
            {
                Console.WriteLine("Invalid sort command.");
                continue;
            }

            string input = tokens[1];
            string order = tokens.Length > 2 ? tokens[2] : "ascending";

            if (!input.Equals(AIRPORTS_STRING, StringComparison.OrdinalIgnoreCase) &&
                !input.Equals(AIRLINES_STRING, StringComparison.OrdinalIgnoreCase) &&
                !input.Equals(FLIGHTS_STRING, StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Invalid input data. Input data should be Airports, Airlines, or Flights.");
                continue;
            }

            if (!order.Equals("ascending", StringComparison.OrdinalIgnoreCase)
             && !order.Equals("descending", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Invalid sort order. Order must be 'ascending' or 'descending'.");
                continue;
            }

            if (order.Equals("ascending", StringComparison.OrdinalIgnoreCase))
            {
                // Print collected data for airports, airlines, and flights
                if (input.Equals(AIRPORTS_STRING, StringComparison.OrdinalIgnoreCase))
                {
                    airportsToSort.BubbleSort();
                    Console.WriteLine(string.Concat(Enumerable.Repeat("-", 20)));
                    PrintData(AIRPORTS_STRING, airportsToSort);
                }
                else if (input.Equals(AIRLINES_STRING, StringComparison.OrdinalIgnoreCase))
                {
                    airlinesToSort.SelectionSort();
                    Console.WriteLine(string.Concat(Enumerable.Repeat("-", 20)));
                    PrintData(AIRLINES_STRING, airlinesToSort);
                }
                else
                {
                    flightsToSort.SelectionSort();
                    Console.WriteLine(string.Concat(Enumerable.Repeat("-", 20)));
                    PrintData(FLIGHTS_STRING, flightsToSort);
                }
            }
            else
            {
                // Print collected data for airports, airlines, and flights
                if (input.Equals(AIRPORTS_STRING, StringComparison.OrdinalIgnoreCase))
                {
                    airportsToSort.BubbleSort(false);
                    Console.WriteLine(string.Concat(Enumerable.Repeat("-", 20)));
                    PrintData(AIRPORTS_STRING, airportsToSort);
                }
                else if (input.Equals(AIRLINES_STRING, StringComparison.OrdinalIgnoreCase))
                {
                    airlinesToSort.SelectionSort(false);
                    Console.WriteLine(string.Concat(Enumerable.Repeat("-", 20)));
                    PrintData(AIRLINES_STRING, airlinesToSort);
                }
                else
                {
                    flightsToSort.SelectionSort(false);
                    Console.WriteLine(string.Concat(Enumerable.Repeat("-", 20)));
                    PrintData(FLIGHTS_STRING, flightsToSort);
                }
            }
        }
    }

    // Function to search for user input data
    internal static void SearchDataFromConsole(string[] airportsNames, string[] airlinesNames, string[] flightsNames)
    {
        Console.WriteLine("Enter command search <search term> to search (type 'End Search' to finish searching): ");

        while (true)
        {
            bool foundInAirports = false;
            bool foundInAirlines = false;
            bool foundInFlights = false;

            string searchData = Console.ReadLine()!;

            if (Regex.Replace(searchData, @"\s+", "").Equals($"EndSearch", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine();
                break;
            }

            if (!searchData.StartsWith("search", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Invalid search command.");
                continue;
            }

            string searchTerm = searchData.Substring(START_INDEX).Trim();

            // Perform binary search on airports
            int airportIndex = airportsNames.BinarySearch(searchTerm, 0, airportsNames.Length - 1);
            if (airportIndex != -1)
            {
                Console.WriteLine($"Found '{searchTerm}' in Airports.");
                foundInAirports = true;
            }

            // Perform binary search on airlines
            int airlineIndex = airlinesNames.BinarySearch(searchTerm, 0, airlinesNames.Length - 1);
            if (airlineIndex != -1)
            {
                Console.WriteLine($"Found '{searchTerm}' in Airlines.");
                foundInAirlines = true;
            }

            // Perform binary search on flights
            int flightIndex = flightsNames.BinarySearch(searchTerm, 0, flightsNames.Length - 1);
            if (flightIndex != -1)
            {
                Console.WriteLine($"Found '{searchTerm}' in Flights.");
                foundInFlights = true;
            }

            if (!foundInAirports && !foundInAirlines && !foundInFlights)
            {
                Console.WriteLine($"'{searchTerm}' not found.");
            }
        }
    }

    // Function to print input data for a specific data type
    internal static void PrintData<T>(string dataType, T[] data) where T : class
    {
        Console.WriteLine($"{dataType} count: {data.Length}");
        Console.WriteLine();
        foreach (T item in data)
        {
            Console.WriteLine($"- {item}");
        }
        Console.WriteLine();
    }
}
