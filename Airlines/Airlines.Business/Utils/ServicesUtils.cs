﻿namespace Airlines.Business.Utils;
public class ServicesUtils
{
    public static List<T> ApplySorting<T>(List<T> items, string filterValue, Func<T, object> orderByFunc)
    {
        if (string.IsNullOrEmpty(filterValue))
        {
            return items;
        }

        if (filterValue.Equals("ASC", StringComparison.OrdinalIgnoreCase))
        {
            return items.OrderBy(orderByFunc).ToList()!;
        }
        else if (filterValue.Equals("DESC", StringComparison.OrdinalIgnoreCase))
        {
            return items.OrderByDescending(orderByFunc).ToList()!;
        }

        return items;
    }
}
