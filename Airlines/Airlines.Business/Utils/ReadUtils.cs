﻿using Airlines.Persistence.Basic.Custom;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Entities.Aircrafts;
using Airlines.Persistence.Basic.Entities.Commands;
using Airlines.Persistence.Basic.Repositories.AircraftRepository;
using Airlines.Persistence.Basic.Repositories.AirlineRepository;
using Airlines.Persistence.Basic.Repositories.AirportRepository;
using Airlines.Persistence.Basic.Repositories.FlightRepository;
using Airlines.Persistence.Basic.Repositories.FlightRouteRepository;

namespace Airlines.Business.Utils.ReadUtilsMethods;

public static class ReadUtils
{
    private static readonly IAirlineRepositoryCSV _airlineRepositoryCSV = new AirlineRepositoryCSV();
    private static readonly IAirportRepositoryCSV _airportRepositoryCSV = new AirportRepositoryCSV();
    private static readonly IFlightRepositoryCSV _flightRepositoryCSV = new FlightRepositoryCSV();
    private static readonly IAircraftRepositoryCSV _aircraftRepositoryCSV = new AircraftRepositoryCSV();
    private static readonly IFlightRouteRepositoryCSV _flightRouteRepositoryCSV = new FlightRouteRepositoryCSV();

    internal static void ReadAirportData(
        string filePath,
        out Dictionary<string, AirportCSV>? airports,
        out Dictionary<string, HashSet<AirportCSV>>? airportsByCity,
        out Dictionary<string, HashSet<AirportCSV>>? airportsByCountry)
    {
        try
        {
            (airports, airportsByCity, airportsByCountry) = _airportRepositoryCSV.ReadAirports(filePath);
        }
        catch (IndexOutOfRangeException)
        {
            airports = [];
            airportsByCity = [];
            airportsByCountry = [];

            Console.WriteLine("Not all needed values are provided in the airports.csv");
            Console.WriteLine();
        }
    }

    internal static void ReadAirlineData(string filePath, out Dictionary<string, AirlineCSV>? airlines)
    {
        try
        {
            airlines = _airlineRepositoryCSV.ReadAirlines(filePath);
        }
        catch (IndexOutOfRangeException)
        {
            airlines = [];

            Console.WriteLine("Not all needed values are provided in the airlines.csv");
            Console.WriteLine();
        }
    }

    internal static void ReadFlightData(string filePath,
                                        out FlightLinkedList? flights,
                                        Dictionary<string, Aircraft> aircrafts,
                                        out Dictionary<string, FlightCSV>? flightsForTree)
    {
        try
        {
            (flights, flightsForTree) = _flightRepositoryCSV.ReadFlights(filePath, aircrafts);
        }
        catch (IndexOutOfRangeException)
        {
            flights = new FlightLinkedList();
            flightsForTree = [];

            Console.WriteLine("Not all needed values are provided in the flights.csv");
            Console.WriteLine();
        }
    }

    internal static void ReadAircraftData(string filePath, out Dictionary<string, Aircraft>? aircrafts)
    {
        try
        {
            aircrafts = _aircraftRepositoryCSV.ReadAircrafts(filePath);
        }
        catch (IndexOutOfRangeException)
        {
            aircrafts = null;

            Console.WriteLine("Not all needed values are provided in the aircrafts.csv");
            Console.WriteLine();
        }
    }

    internal static void ReadFlightRouteData(string filePath, out List<string> flightRouteList, FlightLinkedList flights)

        => flightRouteList = _flightRouteRepositoryCSV.ReadFlightRoute(filePath, flights);


    internal static void ReadUserCommands(Dictionary<string, AirportCSV> airports,
                                        Dictionary<string, AirlineCSV> airlines,
                                        FlightLinkedList flights,
                                        FlightLinkedList flightRoute,
                                        Dictionary<string, FlightCSV> flightsForTree,
                                        List<string> flightRouteListForTree,
                                        Dictionary<string, HashSet<AirportCSV>> airportsByCity,
                                        Dictionary<string, HashSet<AirportCSV>> airportsByCountry)
    {
        ReadModeCommand readModeCommand = ReadModeCommand.CreateReadModeCommand(airports,
                                                                                airlines,
                                                                                flights,
                                                                                flightRoute,
                                                                                flightsForTree,
                                                                                flightRouteListForTree,
                                                                                airportsByCity,
                                                                                airportsByCountry);
        readModeCommand.Execute();
    }
}


