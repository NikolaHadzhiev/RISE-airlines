﻿using Airlines.Business.Business.Commands;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Persistence.Basic.Entities.Commands;
public class SearchCommand : ICommand
{
    public string Input { get; }
    private Dictionary<string, AirportCSV> Airports { get; }
    private Dictionary<string, AirlineCSV> Airlines { get; }
    private FlightLinkedList Flights { get; }

    private SearchCommand(string input, Dictionary<string, AirportCSV> airports, Dictionary<string, AirlineCSV> airlines, FlightLinkedList flights)
    {
        Input = input;
        Airports = airports;
        Airlines = airlines;
        Flights = flights;
    }
    public void Execute() => SearchCommands.SearchTerm(Input, Airports, Airlines, Flights);

    internal static SearchCommand CreateSearchCommand(string input, Dictionary<string, AirportCSV> airports, Dictionary<string, AirlineCSV> airlines, FlightLinkedList flights)
        => new(input, airports, airlines, flights);
}