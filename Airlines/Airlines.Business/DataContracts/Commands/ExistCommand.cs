﻿using Airlines.Business.Business.Commands;

namespace Airlines.Persistence.Basic.Entities.Commands;
public class ExistCommand : ICommand
{
    private string Input { get; }
    private Dictionary<string, AirlineCSV> Airlines { get; }

    private ExistCommand(string input, Dictionary<string, AirlineCSV> airlines)
    {
        Input = input;
        Airlines = airlines;
    }

    public void Execute() => ExistCommands.HandleExist(Input, Airlines);

    internal static ExistCommand CreateExistCommand(string input, Dictionary<string, AirlineCSV> airlines) => new(input, airlines);
}
