﻿using Airlines.Business.Business.Commands;

namespace Airlines.Persistence.Basic.Entities.Commands;
public class ListCommand : ICommand
{
    private string Input { get; }
    private Dictionary<string, HashSet<AirportCSV>> AirportsByCity { get; }
    private Dictionary<string, HashSet<AirportCSV>> AirportsByCountry { get; }

    private ListCommand(string input, Dictionary<string, HashSet<AirportCSV>> airportsByCity, Dictionary<string, HashSet<AirportCSV>> airportsByCountry)
    {
        Input = input;
        AirportsByCity = airportsByCity;
        AirportsByCountry = airportsByCountry;
    }
    public void Execute() => ListCommands.HandleList(Input, AirportsByCity, AirportsByCountry);

    internal static ListCommand CreateListCommand(string input, Dictionary<string, HashSet<AirportCSV>> airportsByCity, Dictionary<string, HashSet<AirportCSV>> airportsByCountry)
        => new(input, airportsByCity, airportsByCountry);
}
