﻿namespace Airlines.Persistence.Basic.Entities.Commands;
public interface ICommand
{
    void Execute();
}
