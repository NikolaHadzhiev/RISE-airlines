﻿using Airlines.Business.Business.Commands;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Persistence.Basic.Entities.Commands;
public class RouteCommand : ICommand
{
    private string Input { get; }
    private FlightLinkedList FlightLinkedList { get; }
    private FlightLinkedList FlightRoute { get; }
    private Dictionary<string, FlightCSV> FlightsForTree { get; }
    private List<string> FlightRouteListForTree { get; }

    private RouteCommand(string input, FlightLinkedList flightList, FlightLinkedList flightRoute, Dictionary<string, FlightCSV> flightsForTree, List<string> flightRouteListForTree)
    {
        Input = input;
        FlightLinkedList = flightList;
        FlightRoute = flightRoute;
        FlightsForTree = flightsForTree;
        FlightRouteListForTree = flightRouteListForTree;
    }

    public void Execute() => RouteCommands.HandleRouteCommands(Input, FlightLinkedList, FlightRoute, FlightsForTree, FlightRouteListForTree);

    internal static RouteCommand CreateRouteCommand(string input, FlightLinkedList flightList, FlightLinkedList flightRoute, Dictionary<string, FlightCSV> flightsForTree, List<string> flightRouteListForTree)
        => new(input, flightList, flightRoute, flightsForTree, flightRouteListForTree);
}
