﻿using Airlines.Business.Business.Commands;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Persistence.Basic.Entities.Commands;
public class ReadModeCommand : ICommand
{
    private Dictionary<string, AirportCSV> Airports { get; }
    private Dictionary<string, AirlineCSV> Airlines { get; }
    private FlightLinkedList Flights { get; }
    private FlightLinkedList FlightRoute { get; }
    private Dictionary<string, FlightCSV> FlightsForTree { get; }
    private List<string> FlightRouteListForTree { get; }
    private Dictionary<string, HashSet<AirportCSV>> AirportsByCity { get; }
    private Dictionary<string, HashSet<AirportCSV>> AirportsByCountry { get; }

    private ReadModeCommand(Dictionary<string, AirportCSV> airports,
                            Dictionary<string, AirlineCSV> airlines,
                            FlightLinkedList flights,
                            FlightLinkedList flightRoute,
                            Dictionary<string, FlightCSV> flightsForTree,
                            List<string> flightRouteListForTree,
                            Dictionary<string, HashSet<AirportCSV>> airportsByCity,
                            Dictionary<string, HashSet<AirportCSV>> airportsByCountry)
    {
        Airports = airports;
        Airlines = airlines;
        Flights = flights;
        FlightRoute = flightRoute;
        FlightsForTree = flightsForTree;
        FlightRouteListForTree = flightRouteListForTree;
        AirportsByCity = airportsByCity;
        AirportsByCountry = airportsByCountry;
    }
    public void Execute() => ReadModeCommands.HandleReadMode(
        Airports, Airlines, Flights, FlightRoute, FlightsForTree, FlightRouteListForTree, AirportsByCity, AirportsByCountry);

    internal static ReadModeCommand CreateReadModeCommand(Dictionary<string, AirportCSV> airports,
                                                        Dictionary<string, AirlineCSV> airlines,
                                                        FlightLinkedList flights,
                                                        FlightLinkedList flightRoute,
                                                        Dictionary<string, FlightCSV> flightsForTree,
                                                        List<string> flightRouteListForTree,
                                                        Dictionary<string, HashSet<AirportCSV>> airportsByCity,
                                                        Dictionary<string, HashSet<AirportCSV>> airportsByCountry)

        => new(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);
}