﻿using Airlines.Persistence.Basic.Business.Commands;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Persistence.Basic.Entities.Commands;
public class ReserveCommand : ICommand
{
    private string Input { get; }
    private FlightLinkedList Flights { get; }
    private ReserveCommand(string input, FlightLinkedList flights)
    {
        Input = input;
        Flights = flights;
    }
    public void Execute() => ReserveCommands.HandleReserve(Input, Flights);

    internal static ReserveCommand CreateReserveCommand(string input, FlightLinkedList flights) => new(input, flights);
}