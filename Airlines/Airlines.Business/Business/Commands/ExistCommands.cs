﻿using Airlines.Business.Constants;
using Airlines.Business.Custom.Exceptions;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Business.Commands;
public static class ExistCommands
{
    internal static void HandleExist(string input, Dictionary<string, AirlineCSV> airlines)
    {
        string[] tokens_exist = input.Split(PROJECT_CONSTANTS.SEPARATOR, StringSplitOptions.RemoveEmptyEntries);

        if (tokens_exist.Length < 2)
        {
            throw new InvalidCommandException("Invalid exist command. Missing arguments");
        }

        string airlineName = tokens_exist[1];

        bool exists = airlines.ContainsKey(airlineName);

        if (exists)
        {
            Console.WriteLine($"Airline with name '{airlineName}' exists");
            Console.WriteLine();
        }
        else
        {
            Console.WriteLine("Airline does not exist");
            Console.WriteLine();
        }
    }
}
