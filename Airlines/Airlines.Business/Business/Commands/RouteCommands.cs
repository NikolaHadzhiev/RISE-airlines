﻿using Airlines.Business.Constants;
using Airlines.Business.Custom.Graph;
using Airlines.Business.Custom.Tree;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Custom;
using Airlines.Business.Custom.Exceptions;

namespace Airlines.Business.Business.Commands;

public static class RouteCommands
{
    internal static void HandleRouteCommands(string input,
                                            FlightLinkedList flightList,
                                            FlightLinkedList flightRoute,
                                            Dictionary<string, FlightCSV> flightsForTree,
                                            List<string> flightRouteListForTree)
    {
        AirportGraph airportGraph = AirportGraph.CreateAirportGraph(flightList);
        string[] route_list = input.Split(PROJECT_CONSTANTS.SEPARATOR, StringSplitOptions.RemoveEmptyEntries);

        if (route_list.Length == 1)
        {
            throw new InvalidCommandException("Invalid route command. Missing arguments.");
        }

        switch (route_list[1])
        {
            case "new":

                Console.WriteLine("New route initialized.");
                Console.WriteLine();
                flightRoute.ClearRoute();

                break;

            case "add":

                if (route_list.Length < 3)
                {
                    throw new InvalidCommandException("Route command is missing flight identifier.");
                }

                string flightIdentifier = route_list[2];

                FlightCSV? flightFound = flightList.SearchFlight(flightIdentifier);

                if (flightFound == null)
                {
                    Console.WriteLine($"FlightCSV with identifier '{flightIdentifier}' not found in the list");
                    Console.WriteLine();

                    return;
                }

                FlightCSV? newFlight = FlightCSV.CreateFlight(flightFound.Identifier, flightFound.DepartureAirport,
                                                        flightFound.ArrivalAirport, flightFound.Price, flightFound.Time);

                newFlight.Aircraft = flightFound.Aircraft;

                flightRoute.AddFlight(newFlight);
                break;

            case "remove":

                flightRoute.RemoveFlightAtEnd();
                break;

            case "find":

                if (route_list.Length < 3)
                {
                    throw new InvalidCommandException("Find command is missing desired destination airport.");
                }

                string startAirport = flightRouteListForTree[0];
                string destinationAirport = route_list[2];

                FlightRouteTree flightRouteTree = FlightRouteTree.CreateFlightRouteTree(startAirport, flightsForTree);

                FlightNode node = flightRouteTree.PopulateFlightRouteTree();

                List<string> desiredRouteToDestinationAirport;

                bool desiredPathFound = flightRouteTree.FindRouteBFS(node, destinationAirport, out desiredRouteToDestinationAirport!);

                if (desiredPathFound)
                {
                    Console.WriteLine();
                    Console.WriteLine($"Route found: {string.Join(" -> ", desiredRouteToDestinationAirport)}");
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("No route found.");
                    Console.WriteLine();
                }

                flightRouteTree.PrintFlightRouteTree(node);

                break;

            case "check":

                if (route_list.Length < 4)
                {
                    throw new InvalidCommandException("Route check command is missing start/end airport.");
                }

                string start_airport_ckeck = route_list[2];
                string end_airport_check = route_list[3];

                if (airportGraph.IsConnected(start_airport_ckeck, end_airport_check))
                {
                    Console.WriteLine($"{start_airport_ckeck} is connected to {end_airport_check}");
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine($"{start_airport_ckeck} is not connected to {end_airport_check}");
                    Console.WriteLine();
                }

                break;

            case "search":

                if (route_list.Length < 5)
                {
                    throw new InvalidCommandException("Route search command is missing start airport/end airport/strategy option.");
                }

                string start_airport_search = route_list[2];
                string end_airport_search = route_list[3];
                string strategy = route_list[4];

                List<string>? route = airportGraph.CalculatePath(start_airport_search, end_airport_search, strategy);

                if (route != null)
                {
                    airportGraph.PrintRoute(strategy, route);
                }
                else
                {
                    Console.WriteLine($"No route found.");
                    Console.WriteLine();
                }

                break;

            case "print":

                flightRoute.DisplayFlights();
                break;

            default:
                throw new InvalidCommandException("Invalid route command.");
        }
    }
}
