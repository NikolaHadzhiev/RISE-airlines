﻿using Airlines.Business.Constants;
using Airlines.Business.Custom.Exceptions;
using Airlines.Persistence.Basic.Custom;
using Airlines.Persistence.Basic.Custom.Exceptions;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Entities.Aircrafts;

namespace Airlines.Persistence.Basic.Business.Commands;
public static class ReserveCommands
{
    internal static void HandleReserve(string input, FlightLinkedList flights)
    {
        string[] reserve_list = input.Split(PROJECT_CONSTANTS.SEPARATOR, StringSplitOptions.RemoveEmptyEntries);

        if (reserve_list.Length == 1)
        {
            throw new InvalidCommandException("Invalid reserve command. Missing arguments");
        }

        switch (reserve_list[1])
        {
            case "cargo":

                if (reserve_list.Length < 5)
                {
                    throw new InvalidCommandException("Reserve command is missing flight identifier, cargo weight or cargo volume.");
                }

                string flightIdentifier = reserve_list[2];
                double cargoWeight = double.Parse(reserve_list[3]);
                double cargoVolume = double.Parse(reserve_list[4]);

                FlightCSV? cargoFlight = flights.SearchFlight(flightIdentifier);
                if (cargoFlight == null)
                {
                    Console.WriteLine($"FlightCSV '{flightIdentifier}' not found.");
                    Console.WriteLine();

                    break;
                }

                if (cargoFlight.Aircraft is not CargoAircraft)
                {
                    throw new InvalidAircraftException($"Aircraft '{cargoFlight?.Aircraft?.Model}' is not compatible for cargo requests.");
                }

                CargoAircraft cargoAircraft = (CargoAircraft)cargoFlight.Aircraft;

                bool cAircraftReachedMaxWeight = cargoAircraft.CargoWeight - cargoWeight < 0;
                bool cAircraftReachedMaxVolume = cargoAircraft.CargoVolume - cargoVolume < 0;

                if (cAircraftReachedMaxWeight || cAircraftReachedMaxVolume)
                {
                    throw new InvalidAircraftException($"Aircraft {cargoFlight?.Aircraft?.Model} cannot be reserved due to insufficient weight/volume");
                }

                cargoAircraft.CargoWeight -= cargoWeight;
                cargoAircraft.CargoVolume -= cargoVolume;

                Console.WriteLine($"{flightIdentifier} succesfully reserved");
                Console.WriteLine();

                break;

            case "ticket":

                if (reserve_list.Length < 6)
                {
                    throw new InvalidCommandException("Reserve command is missing flight identifier, seats, small baggage count or large baggage count.");
                }

                string flightPassengerIdentifier = reserve_list[2];
                int seats = int.Parse(reserve_list[3]);
                int smallBagageCount = int.Parse(reserve_list[4]);
                int largeBagageCount = int.Parse(reserve_list[5]);

                FlightCSV? passengerFlight = flights.SearchFlight(flightPassengerIdentifier);

                if (passengerFlight == null)
                {
                    Console.WriteLine($"FlightCSV '{flightPassengerIdentifier}' not found.");
                    Console.WriteLine();

                    break;
                }

                if (passengerFlight.Aircraft is not PassengerAircraft)
                {
                    throw new InvalidAircraftException($"Aircraft '{passengerFlight?.Aircraft?.Model}' is not compatible for passenger requests.");
                }

                PassengerAircraft passengerAircraft = (PassengerAircraft)passengerFlight.Aircraft;

                int passangerAllSmallBagagesWeight = smallBagageCount * PROJECT_CONSTANTS.SMALL_BAGGAGE_MAX_WEIGHT;
                double passangerAllSmallBagagesVolume = smallBagageCount * PROJECT_CONSTANTS.SMALL_BAGGAGE_MAX_VOLUME;

                int passangerAllLargeBagagesWeight = largeBagageCount * PROJECT_CONSTANTS.LARGE_BAGGAGE_MAX_WEIGHT;
                double passangerAllLargeBagagesVolume = largeBagageCount * PROJECT_CONSTANTS.LARGE_BAGGAGE_MAX_VOLUME;

                int passengerAllBagageWeight = passangerAllSmallBagagesWeight + passangerAllLargeBagagesWeight;
                double passengerAllBagageVolume = passangerAllSmallBagagesVolume + passangerAllLargeBagagesVolume;

                bool pAircraftReachedMaxSeats = passengerAircraft.Seats - seats < 0;
                bool pAircraftReachedMaxWeight = passengerAircraft.CargoWeight - passengerAllBagageWeight < 0;
                bool pAircraftReachedMaxVolume = passengerAircraft.CargoVolume - passengerAllBagageVolume < 0;

                if (pAircraftReachedMaxSeats || pAircraftReachedMaxWeight || pAircraftReachedMaxVolume)
                {
                    throw new InvalidAircraftException($"Aircraft {passengerFlight?.Aircraft?.Model} cannot be reserved due to insufficient weight/volume/seats");
                }

                passengerAircraft.Seats -= seats;
                passengerAircraft.CargoWeight -= passengerAllBagageWeight;
                passengerAircraft.CargoVolume -= passengerAllBagageVolume;

                Console.WriteLine($"{flightPassengerIdentifier} succesfully reserved");
                Console.WriteLine();

                break;

            default:
                throw new InvalidCommandException("Invalid reserve command.");
        }
    }
}
