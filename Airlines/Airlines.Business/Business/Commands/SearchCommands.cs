﻿using Airlines.Business.Constants;
using Airlines.Business.Custom.Exceptions;
using Airlines.Persistence.Basic.Custom;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Business.Commands;

public static class SearchCommands
{
    internal static void SearchTerm(string input,
                                  Dictionary<string, AirportCSV> airports,
                                  Dictionary<string, AirlineCSV> airlines,
                                  FlightLinkedList flights)
    {
        bool foundInAirports = false;
        bool foundInAirlines = false;
        bool foundInFlights = false;

        string searchTerm = input.Substring(PROJECT_CONSTANTS.START_INDEX).Trim();

        if (airports.ContainsKey(searchTerm))
        {
            Console.WriteLine($"AirportCSV with ID: '{searchTerm}' found");
            Console.WriteLine();

            foundInAirports = true;
        }

        if (airlines.ContainsKey(searchTerm))
        {
            Console.WriteLine($"Airline with Name: '{searchTerm}' found");
            Console.WriteLine();

            foundInAirlines = true;
        }

        if (flights.SearchFlight(searchTerm) != null)
        {
            Console.WriteLine($"FlightCSV with ID: '{searchTerm}' found");
            Console.WriteLine();

            foundInFlights = true;
        }

        if (!foundInAirports && !foundInAirlines && !foundInFlights)
        {
            throw new InvalidCommandException($"'{searchTerm}' not found.");
        }
    }
}
