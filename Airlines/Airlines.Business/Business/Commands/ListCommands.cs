﻿using Airlines.Business.Constants;
using Airlines.Business.Custom.Exceptions;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Business.Commands;
public static class ListCommands
{
    internal static void HandleList(string input, Dictionary<string, HashSet<AirportCSV>> airportsByCity, Dictionary<string, HashSet<AirportCSV>> airportsByCountry)
    {
        string[] tokens_list = input.Split(PROJECT_CONSTANTS.SEPARATOR, StringSplitOptions.RemoveEmptyEntries);

        if (tokens_list.Length < 3)
        {
            throw new InvalidCommandException("Invalid list command. Missing arguments");
        }

        string nameForCityOrCountry = tokens_list[1];
        string fromCityOrCountry = tokens_list[2];

        if (fromCityOrCountry.Equals("city", StringComparison.CurrentCultureIgnoreCase))
        {
            if (airportsByCity.TryGetValue(nameForCityOrCountry, out HashSet<AirportCSV>? airports))
            {
                PrintAirports(airports);
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine($"No airports found in the city '{nameForCityOrCountry}'.");
                Console.WriteLine();
            }
        }
        else if (fromCityOrCountry.Equals("country", StringComparison.CurrentCultureIgnoreCase))
        {
            if (airportsByCountry.TryGetValue(nameForCityOrCountry, out HashSet<AirportCSV>? airports))
            {
                PrintAirports(airports);
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine($"No airports found in the country '{nameForCityOrCountry}'.");
                Console.WriteLine();
            }
        }
        else
        {
            throw new InvalidCommandException("Invalid list command. Argument should be city or country");
        }
    }
    private static void PrintAirports<T>(HashSet<T> data)
    {
        Console.WriteLine();

        foreach (T value in data)
        {
            Console.WriteLine($"{value}");
        }

        Console.WriteLine();
    }
}
