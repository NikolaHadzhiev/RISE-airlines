﻿using Airlines.Business.Custom.Exceptions;
using Airlines.Persistence.Basic.Custom;
using Airlines.Persistence.Basic.Custom.Exceptions;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Entities.Commands;
using System.Text.RegularExpressions;

namespace Airlines.Business.Business.Commands;
public static class ReadModeCommands
{
    internal static void HandleReadMode(Dictionary<string, AirportCSV> airports,
                                      Dictionary<string, AirlineCSV> airlines,
                                      FlightLinkedList flights,
                                      FlightLinkedList flightRoute,
                                      Dictionary<string, FlightCSV> flightsForTree,
                                      List<string> flightRouteListForTree,
                                      Dictionary<string, HashSet<AirportCSV>> airportsByCity,
                                      Dictionary<string, HashSet<AirportCSV>> airportsByCountry)
    {
        Console.WriteLine("Enter 'End' to finish reading commands: ");
        Console.WriteLine("Enter command search <search term> (ID) to search by identifier: ");
        Console.WriteLine("Enter command sort <input data> [order] (order is optional - 'ascending'/'descending') to sort: ");
        Console.WriteLine("Enter command exist <airline name> to check if airline with a matching name is existing: ");
        Console.WriteLine("Enter command list <input data> <from> to list all airports in a city or a country: ");
        Console.WriteLine("Enter command route new to initialize new flight: ");
        Console.WriteLine("Enter command route add <flight identifier> to add a flight from the list to the end of the route: ");
        Console.WriteLine("Enter command route remove to remove the last flight from the current route: ");
        Console.WriteLine("Enter command route find <Destination AirportCSV> to search for a flight route: ");
        Console.WriteLine("Enter command route print to get all flights from the current route: ");
        Console.WriteLine("Enter command route check <Start AirportCSV> <End AirportCSV> to check for airport connectivity: ");
        Console.WriteLine("Enter command route search <Start AirportCSV> <End AirportCSV> <Strategy> to check for cheapest/shortest/least stops path: ");
        Console.WriteLine("Enter command reserve cargo <FlightCSV Identifier> <Cargo Weight> <Cargo Volume> to reserve cargo: ");
        Console.WriteLine("Enter command reserve ticket <FlightCSV Identifier> <Seats> <Small Baggage Count> <Large Baggage Count> to reserve ticket: ");
        Console.WriteLine("Enter command batch start to activates batch mode: ");
        Console.WriteLine("Enter command batch run to execute all commands in the batch queue: ");
        Console.WriteLine("Enter command batch cancel to clear the batch queue and cancel the execution of queued commands: ");
        Console.WriteLine();

        bool isBatchMode = false;
        Queue<string> batchCommands = new Queue<string>();

        string input = Console.ReadLine()!;

        while (!Regex.Replace(input!, @"\s+", "").Equals($"End", StringComparison.OrdinalIgnoreCase))
        {
            if (isBatchMode)
            {
                if (input!.StartsWith("batch run", StringComparison.CurrentCultureIgnoreCase))
                {
                    Console.WriteLine("Running batch commands...");
                    Console.WriteLine();

                    while (batchCommands.Count > 0)
                    {
                        string batchCommand = batchCommands.Dequeue();

                        try
                        {
                            ProcessCommand(batchCommand, airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);
                        }
                        catch (InvalidCommandException ex)
                        {
                            Console.WriteLine(ex.Message);
                            Console.WriteLine();
                        }
                    }

                    isBatchMode = false; // Exit batch mode after executing all commands

                }
                else if (input.StartsWith("batch cancel", StringComparison.CurrentCultureIgnoreCase))
                {
                    Console.WriteLine("Batch queue cleared and canceled");

                    batchCommands.Clear(); // Clear the queue
                    isBatchMode = false; // Exit batch mode

                    Console.WriteLine();
                }
                else if (!input.StartsWith("batch start", StringComparison.CurrentCultureIgnoreCase))
                {
                    Console.WriteLine("Command added to batch successfully");

                    batchCommands.Enqueue(input); // Add command to the batch queue

                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("Command skiped");
                    Console.WriteLine();
                }
            }
            else
            {
                if (input!.StartsWith("batch start", StringComparison.CurrentCultureIgnoreCase))
                {
                    Console.WriteLine("Batch mode activated");

                    isBatchMode = true; // Enter batch mode

                    Console.WriteLine();
                }
                else
                {
                    try
                    {
                        ProcessCommand(input, airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);
                    }
                    catch (InvalidCommandException ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine();
                    }
                }
            }

            input = Console.ReadLine()!;
        }

        // Stop the program after reading flights
        Console.WriteLine("End of program. Press any key to exit.");

        // To prevent the console window from closing immediately
        _ = Console.ReadLine();
    }

    private static void ProcessCommand(string input,
                                       Dictionary<string, AirportCSV> airports,
                                       Dictionary<string, AirlineCSV> airlines,
                                       FlightLinkedList flights,
                                       FlightLinkedList flightRoute,
                                       Dictionary<string, FlightCSV> flightsForTree,
                                       List<string> flightRouteListForTree,
                                       Dictionary<string, HashSet<AirportCSV>> airportsByCity,
                                       Dictionary<string, HashSet<AirportCSV>> airportsByCountry)
    {
        switch (input.ToLower())
        {
            case string s when s.StartsWith("search"):

                SearchCommand searchCommand = SearchCommand.CreateSearchCommand(input, airports, airlines, flights);
                searchCommand.Execute();

                break;

            case string s when s.StartsWith("sort"):

                SortCommand sortCommand = SortCommand.CreateSortCommand(input, airports, airlines, flights);
                sortCommand.Execute();

                break;

            case string s when s.StartsWith("exist"):

                ExistCommand existCommand = ExistCommand.CreateExistCommand(input, airlines);
                existCommand.Execute();

                break;

            case string s when s.StartsWith("list"):

                ListCommand listCommand = ListCommand.CreateListCommand(input, airportsByCity, airportsByCountry);
                listCommand.Execute();

                break;

            case string s when s.StartsWith("route"):

                try
                {
                    RouteCommand routeCommand = RouteCommand.CreateRouteCommand(input, flights, flightRoute, flightsForTree, flightRouteListForTree);
                    routeCommand.Execute();
                }
                catch (InvalidFlightException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }

                break;

            case string s when s.StartsWith("reserve"):

                try
                {
                    ReserveCommand reserveCommand = ReserveCommand.CreateReserveCommand(input, flights);
                    reserveCommand.Execute();
                }
                catch (InvalidAircraftException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }

                break;

            default:
                throw new InvalidCommandException("Invalid command.");
        }
    }
}
