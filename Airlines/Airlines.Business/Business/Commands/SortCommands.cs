﻿using Airlines.Business.Constants;
using Airlines.Business.Custom;
using Airlines.Business.Custom.Exceptions;
using Airlines.Persistence.Basic.Custom;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Business.Commands;

#pragma warning disable CS8714 // The type cannot be used as type parameter in the generic type or method.

public static class SortCommands
{
    internal static void HandleSort(string input,
                                  Dictionary<string, AirportCSV> airports,
                                  Dictionary<string, AirlineCSV> airlines,
                                  FlightLinkedList flights)
    {
        string[] tokens = input.Split(PROJECT_CONSTANTS.SEPARATOR, StringSplitOptions.RemoveEmptyEntries);

        if (tokens.Length < 2)
        {
            throw new InvalidCommandException("Invalid sort command. Missing arguments");
        }

        string data = tokens[1];
        string order = tokens.Length > 2 ? tokens[2] : "ascending";

        ValidateSortDataAndOrder(data, order);

        Console.WriteLine(string.Concat(Enumerable.Repeat("-", 20)));

        if (data.Equals(PROJECT_CONSTANTS.AIRPORTS_STRING, StringComparison.OrdinalIgnoreCase))
        {
            Dictionary<string, AirportCSV> orderedAirports = SortDictionary(airports, order);

            PrintSorted(orderedAirports);

            Console.WriteLine();
        }
        else if (data.Equals(PROJECT_CONSTANTS.AIRLINES_STRING, StringComparison.OrdinalIgnoreCase))
        {
            Dictionary<string, AirlineCSV> orderedAirlines = SortDictionary(airlines, order);

            PrintSorted(orderedAirlines);

            Console.WriteLine();
        }
        else if (data.Equals(PROJECT_CONSTANTS.FLIGHTS_STRING, StringComparison.OrdinalIgnoreCase))
        {
            SortFlightLinkedList.MergeSort(ref flights.Head!, order);

            flights.DisplayFlights();
        }
    }

    private static void ValidateSortDataAndOrder(string data, string order)
    {
        bool validData = data.Equals(PROJECT_CONSTANTS.AIRPORTS_STRING, StringComparison.OrdinalIgnoreCase) ||
                         data.Equals(PROJECT_CONSTANTS.AIRLINES_STRING, StringComparison.OrdinalIgnoreCase) ||
                         data.Equals(PROJECT_CONSTANTS.FLIGHTS_STRING, StringComparison.OrdinalIgnoreCase);

        if (!validData)
        {
            throw new InvalidCommandException("Invalid sort data. Input data should be Airports, Airlines or Flights.");
        }

        bool validOrder = order.Equals("ascending", StringComparison.OrdinalIgnoreCase) ||
                          order.Equals("descending", StringComparison.OrdinalIgnoreCase);

        if (!validOrder)
        {
            throw new InvalidCommandException("Invalid sort order. Order must be 'ascending' or 'descending'.");
        }
    }

    private static Dictionary<TKey, TValue> SortDictionary<TKey, TValue>(Dictionary<TKey, TValue> dictionary, string order)
    {
        Dictionary<TKey, TValue> orderedDictionary = order.Equals("ascending", StringComparison.OrdinalIgnoreCase) ?
            dictionary.OrderBy(pair => pair.Key).ToDictionary(pair => pair.Key, pair => pair.Value) :
            dictionary.OrderByDescending(pair => pair.Key).ToDictionary(pair => pair.Key, pair => pair.Value);

        return orderedDictionary;
    }

    private static void PrintSorted<T>(Dictionary<string, T> data)
    {
        Console.WriteLine($"{typeof(T).Name} count: {data.Count}");

        Console.WriteLine();

        foreach ((string _, T value) in data)
            Console.WriteLine($"- {value}");

        Console.WriteLine();
    }
}
