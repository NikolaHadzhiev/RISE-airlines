﻿using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;
using Airlines.Persistence.Basic.Profiles;
using Airlines.Persistence.Basic.Repositories.FlightRepository;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Airlines.Business.Services;

#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable CA1822 // Mark members as static

public class FlightService : IFlightService
{
    private readonly IFlightRepositoryDB _flightRepository;

    public FlightService(IFlightRepositoryDB repositoryFactory)
    {
        _flightRepository = repositoryFactory;
    }

    public async Task<List<Flight>> GetAllFlightsFromDB()
    {
        List<Flight> flightList = await _flightRepository.GetAllFlightsAsync();

        return flightList;
    }

    public async Task<List<Flight>> GetAllFlightsFromDB(string searchParam, string searchFilter)
    {
        List<Flight> flightList = await _flightRepository.GetAllFlightsAsync(searchParam, searchFilter);

        return flightList;
    }

    public async Task<Flight> GetFlightByIdFromDB(int id)
    {
        Flight? flight = await _flightRepository.GetFlightByIdAsync(id);

        if (flight == null)
        {
            Console.WriteLine($"Flight with ID {id} not found.");
        }
        else
        {
            Console.WriteLine($"Flight: {flight.ID} {flight.FlightNumber} found.");
        }

        return flight!;
    }

    public async Task<Flight?> AddFlightToDB(FlightDTO flightDTO)
    {
        // Validate the AirlineDTO properties
        List<ValidationResult> validationResults = ValidateFlightDTO(flightDTO);

        if (validationResults.Count > 0)
        {
            foreach (var validationResult in validationResults)
            {
                Console.WriteLine(validationResult.ErrorMessage);
            }

            return null;
        }

        FlightMapper flightMapper = new();
        Flight flight = flightMapper.MapFlight(flightDTO);

        bool success = await _flightRepository.AddFlightAsync(flight);

        if (success)
        {
            Console.WriteLine("Flight added successfully.");
            return flight;
        }
        else
        {
            Console.WriteLine("Flight is not added");
            return null;
        }
    }

    public async Task<Flight?> UpdateFlightToDB(int id, FlightDTO flightDTO)
    {
        FlightMapper flightMapper = new();
        Flight flight = flightMapper.MapFlight(flightDTO);

        bool success = await _flightRepository.UpdateFlightAsync(id, flight);

        if (success)
        {
            Console.WriteLine("Flight updated successfully.");
            return flight;
        }
        else
        {
            Console.WriteLine("Flight is not updated");
            return null;
        }
    }

    public async Task UpdateFlightDepartureTimeToDB(int id)
    {
        bool success = await _flightRepository.UpdateFlightDepartureDateTimeAsync(id, new DateTime(2025, 4, 20, 8, 30, 0));

        if (success)
        {
            Console.WriteLine("Flight departure time updated successfully.");
        }
        else
        {
            Console.WriteLine("Flight departure time is not updated");
        }
    }

    public async Task UpdateFlightArrivalTimeToDB(int id)
    {
        bool success = await _flightRepository.UpdateFlightArrivalDateTimeAsync(id, new DateTime(2025, 4, 21, 8, 30, 0));

        if (success)
        {
            Console.WriteLine("Flight arrival time updated successfully.");
        }
        else
        {
            Console.WriteLine("Flight arrival time is not updated");
        }
    }

    public async Task UpdateFlightAircraftModelToDB(int id, int aircraftModelID)
    {
        bool success = await _flightRepository.UpdateFlightAircraftModelAsync(id, aircraftModelID);

        if (success)
        {
            Console.WriteLine("Flight aircraft model updated successfully.");
        }
        else
        {
            Console.WriteLine("Flight aircraft model is not updated");
        }
    }

    public async Task UpdateFlightPricelToDB(int id, int price)
    {
        bool success = await _flightRepository.UpdateFlightPriceAsync(id, price);

        if (success)
        {
            Console.WriteLine("Flight price updated successfully.");
        }
        else
        {
            Console.WriteLine("Flight price is not updated");
        }
    }

    public async Task<bool> DeleteFlightFromDB(int id)
    {
        bool success = await _flightRepository.DeleteFlightAsync(id);

        if (success)
        {
            Console.WriteLine("Flight deleted successfully.");
            return true;
        }
        else
        {
            Console.WriteLine("Flight is not deleted");
            return false;
        }
    }

    private List<ValidationResult> ValidateFlightDTO(FlightDTO flightDTO)
    {
        var validationResults = new List<ValidationResult>();

        // Validate FlightNumber property
        if (string.IsNullOrEmpty(flightDTO.FlightNumber))
        {
            validationResults.Add(new ValidationResult("Flight number is required"));
        }
        else if (flightDTO.FlightNumber.Length > 6)
        {
            validationResults.Add(new ValidationResult("Flight number should be at most 6 characters long"));
        }

        // Validate Price property
        if (flightDTO.Price <= 0)
        {
            validationResults.Add(new ValidationResult("Price should be a positive number"));
        }

        // Validate DepartureDateTime property
        if (flightDTO.DepartureDateTime == null)
        {
            validationResults.Add(new ValidationResult("Departure date and time are required"));
        }
        else if (flightDTO.DepartureDateTime < DateTime.Today)
        {
            validationResults.Add(new ValidationResult("Departure date and time cannot be in the past"));
        }

        // Validate ArrivalDateTime property
        if (flightDTO.ArrivalDateTime == null)
        {
            validationResults.Add(new ValidationResult("Arrival date and time are required"));
        }
        else if (flightDTO.ArrivalDateTime < DateTime.Today)
        {
            validationResults.Add(new ValidationResult("Arrival date and time cannot be in the past"));
        }

        // Validate ArrivalDateTime is after DepartureDateTime
        if (flightDTO.DepartureDateTime != null && flightDTO.ArrivalDateTime != null && flightDTO.ArrivalDateTime <= flightDTO.DepartureDateTime)
        {
            validationResults.Add(new ValidationResult("Arrival date and time must be after departure date and time"));
        }

        return validationResults;
    }
}
