﻿using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;

namespace Airlines.Business.Services;
public interface IFlightService
{
    public Task<List<Flight>> GetAllFlightsFromDB();

    public Task<List<Flight>> GetAllFlightsFromDB(string searchParam, string searchFilter);
    public Task<Flight> GetFlightByIdFromDB(int id);
    public Task<Flight?> AddFlightToDB(FlightDTO flightDTO);
    public Task<Flight?> UpdateFlightToDB(int id, FlightDTO flightDTO);
    public Task UpdateFlightDepartureTimeToDB(int id);
    public Task UpdateFlightArrivalTimeToDB(int id);
    public Task UpdateFlightAircraftModelToDB(int id, int aircraftModelID);
    public Task UpdateFlightPricelToDB(int id, int price);
    public Task<bool> DeleteFlightFromDB(int id);
}
