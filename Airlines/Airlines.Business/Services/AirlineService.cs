﻿using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;
using Airlines.Persistence.Basic.Profiles;
using Airlines.Persistence.Basic.Repositories.AirlineRepository;
using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.Services;

#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable CA1822 // Mark members as static

public class AirlineService : IAirlineService
{
    private readonly IAirlineRepositoryDB _airlineRepository;

    public AirlineService(IAirlineRepositoryDB repositoryFactory)
    {
        _airlineRepository = repositoryFactory;
    }

    public async Task<List<Airline>> GetAllAirlinesFromDB()
    {
        List<Airline> airlineList = await _airlineRepository.GetAllAirlinesAsync();

        foreach (Airline airline in airlineList)
        {
            Console.WriteLine($"Airline: {airline.Name} {airline.Description}");
        }

        Console.WriteLine();

        return airlineList;
    }
    public async Task<List<Airline>> GetAllAirlinesFromDB(string searchParam, string searchFilter)
    {
        List<Airline> airlineList = await _airlineRepository.GetAllAirlinesAsync(searchParam, searchFilter);

        foreach (Airline airline in airlineList)
        {
            Console.WriteLine($"Airline: {airline.Name} {airline.Description}");
        }

        Console.WriteLine();

        return airlineList;
    }
    public async Task<Airline> GetAirlineByIdFromDB(int id)
    {
        Airline? airline = await _airlineRepository.GetAirlineByIdAsync(id);

        if (airline == null)
        {
            Console.WriteLine($"Airline with ID {id} not found.");
        }
        else
        {
            Console.WriteLine($"Airline: {airline.ID} {airline.Name} found.");
        }

        return airline!;
    }
    public async Task<Airline?> AddAirlineToDB(AirlineDTO airlineDTO)
    {
        // Validate the AirlineDTO properties
        List<ValidationResult> validationResults = ValidateAirlineDTOProperties(airlineDTO);

        if (validationResults.Count > 0)
        {
            foreach (var validationResult in validationResults)
            {
                Console.WriteLine(validationResult.ErrorMessage);
            }

            return null;
        }

        AirlineMapper airlineMapper = new();
        Airline airline = airlineMapper.MapAirline(airlineDTO);

        bool success = await _airlineRepository.AddAirlineAsync(airline);

        if (success)
        {
            Console.WriteLine("Airline added successfully.");
            return airline;
        }
        else
        {
            Console.WriteLine("Airline is not added");
            return null;
        }
    }

    public async Task<Airline?> UpdateAirlineToDB(int id, AirlineDTO airlineDTO)
    {
        AirlineMapper airlineMapper = new();
        Airline airline = airlineMapper.MapAirline(airlineDTO);

        bool success = await _airlineRepository.UpdateAirlineAsync(id, airline);

        if (success)
        {
            Console.WriteLine("Airline updated successfully.");
            return airline;
        }
        else
        {
            Console.WriteLine("Airline is not updated");
            return null;
        }
    }

    public async Task<bool> DeleteAirlineFromDB(int id)
    {
        bool success = await _airlineRepository.DeleteAirlineAsync(id);

        if (success)
        {
            Console.WriteLine("Airline deleted successfully.");
            return true;
        }
        else
        {
            Console.WriteLine("Airline is not deleted");
            return false;
        }
    }

    private List<ValidationResult> ValidateAirlineDTOProperties(AirlineDTO airlineDTO)
    {
        List<ValidationResult> validationResults = [];

        // Validate Name property
        if (string.IsNullOrEmpty(airlineDTO.Name))
        {
            validationResults.Add(new ValidationResult("Name is required"));
        }
        else if (airlineDTO.Name.Length > 5)
        {
            validationResults.Add(new ValidationResult("Name should be at most 5 characters long"));
        }

        // Validate Founded property
        if (airlineDTO.Founded == default)
        {
            validationResults.Add(new ValidationResult("Founded date is required"));
        }

        // Validate FleetSize property
        if (airlineDTO.FleetSize <= 0)
        {
            validationResults.Add(new ValidationResult("Fleet size should be a positive integer"));
        }

        // Validate Description property
        if (string.IsNullOrEmpty(airlineDTO.Description))
        {
            validationResults.Add(new ValidationResult("Description is required"));
        }
        else if (airlineDTO.Description.Length > 1000)
        {
            validationResults.Add(new ValidationResult("Description should be at most 1000 characters long"));
        }

        return validationResults;
    }
}
