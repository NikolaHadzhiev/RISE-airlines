﻿using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;

namespace Airlines.Business.Services;
public interface IAirlineService
{
    public Task<List<Airline>> GetAllAirlinesFromDB();

    public Task<List<Airline>> GetAllAirlinesFromDB(string searchParam, string searchFilter);
    public Task<Airline> GetAirlineByIdFromDB(int id);
    public Task<Airline?> AddAirlineToDB(AirlineDTO airlineDTO);

    public Task<Airline?> UpdateAirlineToDB(int id, AirlineDTO airlineDTO);

    public Task<bool> DeleteAirlineFromDB(int id);
}
