﻿using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;

namespace Airlines.Business.Services;
public interface IAirportService
{
    public Task<List<Airport>> GetAllAirportsFromDB();
    public Task<List<Airport>> GetAllAirportsFromDB(string searchParam, string searchFilter);
    public Task<Airport> GetAirportByIdFromDB(int id);
    public Task<List<Airport>> GetAirportsByCountryFromDB(string country);
    public Task<Airport?> AddAirportToDB(AirportDTO airportDTO);
    public Task<Airport?> UpdateAirportToDB(int id, AirportDTO airportDTO);
    public Task<bool> DeleteAirportFromDB(int id);
}
