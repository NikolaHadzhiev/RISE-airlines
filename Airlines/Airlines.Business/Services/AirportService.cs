﻿using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;
using Airlines.Persistence.Basic.Profiles;
using Airlines.Persistence.Basic.Repositories.AirportRepository;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Airlines.Business.Services;

#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable CA1822 // Mark members as static

public class AirportService : IAirportService
{
    private readonly IAirportRepositoryDB _airportRepository;

    public AirportService(IAirportRepositoryDB repositoryFactory)
    {
        _airportRepository = repositoryFactory;
    }

    public async Task<List<Airport>> GetAllAirportsFromDB()
    {
        List<Airport> airportList = await _airportRepository.GetAllAirportsAsync();

        foreach (Airport airport in airportList)
        {
            Console.WriteLine($"Airport: {airport.Name}");
        }

        Console.WriteLine();

        return airportList;
    }

    public async Task<List<Airport>> GetAllAirportsFromDB(string searchParam, string searchFilter)
    {
        List<Airport> airportList = await _airportRepository.GetAllAirportsAsync(searchParam, searchFilter);

        foreach (Airport airport in airportList)
        {
            Console.WriteLine($"Airport: {airport.Name}");
        }

        Console.WriteLine();

        return airportList;
    }

    public async Task<Airport> GetAirportByIdFromDB(int id)
    {
        Airport? airport = await _airportRepository.GetAirportByIdAsync(id);

        if (airport == null)
        {
            Console.WriteLine($"Airport with ID {id} not found.");
        }
        else
        {
            Console.WriteLine($"Aiport: {airport.ID} {airport.Name} found.");
        }

        return airport!;
    }

    public async Task<List<Airport>> GetAirportsByCountryFromDB(string country)
    {
        List<Airport> airportsByCountry = await _airportRepository.GetAirportsByCountryAsync(country);

        foreach (Airport airport in airportsByCountry)
        {
            Console.WriteLine($"Airport: {airport.Name} {airport.Country}");
        }

        Console.WriteLine();

        return airportsByCountry;
    }

    public async Task<Airport?> AddAirportToDB(AirportDTO airportDTO)
    {
        // Validate the AirlineDTO properties
        List<ValidationResult> validationResults = ValidateAirportDTOProperties(airportDTO);

        if (validationResults.Count > 0)
        {
            foreach (var validationResult in validationResults)
            {
                Console.WriteLine(validationResult.ErrorMessage);
            }

            return null;
        }

        AirportMapper airportMapper = new();
        Airport airport = airportMapper.MapAirport(airportDTO);

        bool success = await _airportRepository.AddAirportAsync(airport);

        if (success)
        {
            Console.WriteLine("Airport added successfully.");
            return airport;
        }
        else
        {
            Console.WriteLine("Airport is not added");
            return null;
        }
    }

    public async Task<Airport?> UpdateAirportToDB(int id, AirportDTO airportDTO)
    {
        AirportMapper airportMapper = new();
        Airport airport = airportMapper.MapAirport(airportDTO);

        bool success = await _airportRepository.UpdateAirportAsync(id, airport);

        if (success)
        {
            Console.WriteLine("Airport updated successfully.");

            return airport;
        }
        else
        {
            Console.WriteLine("Airport is not updated");
            return null;
        }
    }

    public async Task<bool> DeleteAirportFromDB(int id)
    {
        bool success = await _airportRepository.DeleteAirportAsync(id);

        if (success)
        {
            Console.WriteLine("Airport deleted successfully.");
            return true;
        }
        else
        {
            Console.WriteLine("Airport is not deleted");
            return false;
        }
    }

    private List<ValidationResult> ValidateAirportDTOProperties(AirportDTO airportDTO)
#pragma warning restore CA1822 // Mark members as static
    {
        var validationResults = new List<ValidationResult>();

        // Validate Name property
        if (string.IsNullOrEmpty(airportDTO.Name))
        {
            validationResults.Add(new ValidationResult("Name is required"));
        }
        else if (!Regex.IsMatch(airportDTO.Name, @"^[A-Za-z\s]+$"))
        {
            validationResults.Add(new ValidationResult("Name should only contain letters and spaces"));
        }

        // Validate Country property
        if (string.IsNullOrEmpty(airportDTO.Country))
        {
            validationResults.Add(new ValidationResult("Country is required"));
        }
        else if (!Regex.IsMatch(airportDTO.Country, @"^[A-Za-z\s]+$"))
        {
            validationResults.Add(new ValidationResult("Country should only contain letters and spaces"));
        }

        // Validate City property
        if (string.IsNullOrEmpty(airportDTO.City))
        {
            validationResults.Add(new ValidationResult("City is required"));
        }
        else if (!Regex.IsMatch(airportDTO.City, @"^[A-Za-z\s]+$"))
        {
            validationResults.Add(new ValidationResult("City should only contain letters and spaces"));
        }

        // Validate Code property
        if (string.IsNullOrEmpty(airportDTO.Code))
        {
            validationResults.Add(new ValidationResult("Code is required"));
        }
        else if (!Regex.IsMatch(airportDTO.Code, @"^[A-Za-z]+$"))
        {
            validationResults.Add(new ValidationResult("Code should only contain letters"));
        }
        else if (airportDTO.Code.Length != 3)
        {
            validationResults.Add(new ValidationResult("Code should be exactly 3 characters long"));
        }

        // Validate RunwaysCount property
        if (airportDTO.RunwaysCount <= 0)
        {
            validationResults.Add(new ValidationResult("Runways count should be a positive integer"));
        }

        // Validate Founded property
        if (airportDTO.Founded == DateOnly.MinValue)
        {
            validationResults.Add(new ValidationResult("Founded date is required"));
        }

        return validationResults;
    }
}
