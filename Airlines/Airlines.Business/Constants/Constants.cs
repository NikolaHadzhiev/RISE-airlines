﻿namespace Airlines.Business.Constants;

#pragma warning disable CA1707 // Identifiers should not contain underscores
public static class PROJECT_CONSTANTS
{
    public const int START_INDEX = 6;
    public const string AIRPORTS_STRING = "Airports";
    public const string AIRLINES_STRING = "Airlines";
    public const string FLIGHTS_STRING = "Flights";
    public const string AIRPORTS_FILE_PATH = "../../../Resource/airports.csv";
    public const string AIRLINES_FILE_PATH = "../../../Resource/airlines.csv";
    public const string FLIGHTS_FILE_PATH = "../../../Resource/flights.csv";
    public const string AIRCRAFTS_FILE_PATH = "../../../Resource/aircrafts.csv";
    public const string FLIGHT_ROUTE_FILE_PATH = "../../../Resource/flightRoute.csv";
    public static readonly char[] SEPARATOR = [' '];

    public const int SMALL_BAGGAGE_MAX_WEIGHT = 15;
    public const double SMALL_BAGGAGE_MAX_VOLUME = 0.045;
    public const int LARGE_BAGGAGE_MAX_WEIGHT = 30;
    public const double LARGE_BAGGAGE_MAX_VOLUME = 0.090;
}
