﻿using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Custom.Tree;

#pragma warning disable CA1822 // Mark members as static
public class FlightRouteTree
{
    public string StartAirport { get; }
    public Dictionary<string, FlightCSV> Flights { get; }
    private FlightRouteTree(string startAirport, Dictionary<string, FlightCSV> flights)
    {
        StartAirport = startAirport;
        Flights = flights;
    }
    internal FlightNode PopulateFlightRouteTree()
    {
        Dictionary<string, FlightNode> airportNodes = [];

        // Create a node for each airport
        foreach (FlightCSV flight in Flights.Values)
        {
            if (!airportNodes.ContainsKey(flight.DepartureAirport))
            {
                airportNodes[flight.DepartureAirport] = FlightNode.CreateFlightNode(flight.DepartureAirport);
            }

            if (!airportNodes.ContainsKey(flight.ArrivalAirport))
            {
                airportNodes[flight.ArrivalAirport] = FlightNode.CreateFlightNode(flight.ArrivalAirport);
            }
        }

        // Connect nodes based on flights
        foreach (FlightCSV flight in Flights.Values)
        {
            airportNodes[flight.DepartureAirport].Destinations.Add(airportNodes[flight.ArrivalAirport]);
        }

        return airportNodes[StartAirport];
    }

    internal bool FindRouteBFS(FlightNode root, string destination, out List<string>? route)
    {
        Queue<(FlightNode node, List<string> path)> queue = new Queue<(FlightNode node, List<string> path)>();

        queue.Enqueue((root, new List<string> { root.AirportCode }));

        while (queue.Count > 0)
        {
            (FlightNode currentNode, List<string> currentPath) = queue.Dequeue();

            if (currentNode.AirportCode == destination && root.AirportCode != destination)
            {
                route = currentPath;
                return true;
            }

            if (root.AirportCode == destination)
            {
                Console.WriteLine();
                Console.WriteLine($"Destination can't be the starting airport {destination}");
                Console.WriteLine();

                break;
            }

            foreach (FlightNode child in currentNode.Destinations)
            {
                List<string> newPath = new List<string>(currentPath) { child.AirportCode };
                queue.Enqueue((child, newPath));
            }
        }


        route = null;

        return false;
    }

    internal void PrintFlightRouteTree(FlightNode node, string indent = "")
    {
        Console.WriteLine($"{indent}└── {node.AirportCode}"); // Print the current airport code with tree symbols

        // Recursively print the destinations of the current node
        for (int i = 0; i < node.Destinations.Count; i++)
        {
            string? childIndent = (i == node.Destinations.Count - 1) ? "    " : "│   ";
            PrintFlightRouteTree(node.Destinations[i], indent + childIndent); // Adjust indentation for child nodes
        }
    }
    internal static FlightRouteTree CreateFlightRouteTree(string startAirport, Dictionary<string, FlightCSV> flights) => new(startAirport, flights);
}
