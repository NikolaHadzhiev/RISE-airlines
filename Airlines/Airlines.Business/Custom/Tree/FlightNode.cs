﻿namespace Airlines.Business.Custom.Tree;
public class FlightNode
{
    public string AirportCode { get; }
    public List<FlightNode> Destinations { get; }

    private FlightNode(string airportCode)
    {
        AirportCode = airportCode;
        Destinations = [];
    }
    internal static FlightNode CreateFlightNode(string airportCode) => new(airportCode);
}
