﻿using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Custom;
public static class SortFlightLinkedList
{
    // Merge sort for linked list by changing next pointers
    // (not data)
    internal static void MergeSort(ref FlightCSV headRef, string order)
    {
        FlightCSV head = headRef;
        FlightCSV a;
        FlightCSV b;

        // Base case -- length 0 or 1
        if (head == null || head.NextFlight == null)
            return;

        // Split head into 'a' and 'b' sublists
        FrontBackSplit(head, out a, out b);

        // Recursively sort the sublists
        MergeSort(ref a, order);
        MergeSort(ref b, order);

        // Answer = merge the two sorted lists together
        headRef = SortedMerge(a, b, order);
    }

    private static FlightCSV SortedMerge(FlightCSV a, FlightCSV b, string order)
    {
        // Base cases
        if (a == null)
            return b;
        else if (b == null)
            return a;

        FlightCSV result;

        // Pick either a or b, and recur

        bool determineOrder = order == "ascending" ?
                                          string.Compare(a.DepartureAirport, b.DepartureAirport) <= 0 :
                                          string.Compare(a.DepartureAirport, b.DepartureAirport) >= 0;

        if (determineOrder)
        {
            result = a;
            result.NextFlight = SortedMerge(a.NextFlight!, b, order);
        }
        else
        {
            result = b;
            result.NextFlight = SortedMerge(a, b.NextFlight!, order);
        }

        return result;
    }

    // Split the nodes of the given list into front and back halves
    // and return the two lists using the reference parameters
    // If the length is odd, the extra node should go in the front list.
    // Uses the fast/slow pointer strategy.
    private static void FrontBackSplit(FlightCSV source, out FlightCSV frontRef, out FlightCSV backRef)
    {
        FlightCSV fast;
        FlightCSV slow;

        slow = source;
        fast = source.NextFlight!;

        // Advance 'fast' two nodes, and advance 'slow' one node
        while (fast != null)
        {
            fast = fast.NextFlight!;
            if (fast != null)
            {
                slow = slow.NextFlight!;
                fast = fast.NextFlight!;
            }
        }

        // 'slow' is before the midpoint in the list, so split it in two at that point
        frontRef = source;
        backRef = slow.NextFlight!;
        slow.NextFlight = null;
    }
}
