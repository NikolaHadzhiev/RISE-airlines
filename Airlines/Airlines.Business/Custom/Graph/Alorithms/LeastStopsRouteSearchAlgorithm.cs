﻿using Airlines.Business.Custom.Graph.Interfaces;

namespace Airlines.Business.Custom.Graph.Alorithms;
public class LeastStopsRouteSearchAlgorithm : IRouteSearch
{
    public List<string>? SearchPath(string startAirport, string endAirport, Dictionary<string, List<(string Destination, double Price, double Time)>> adjacencyList)
    {
        Queue<string> queue = new Queue<string>();
        Dictionary<string, string?> previousNodes = [];
        List<string> shortestPath = [];

        queue.Enqueue(startAirport);
        previousNodes[startAirport] = null;

        while (queue.Count > 0)
        {
            if (startAirport == endAirport)
            {
                Console.WriteLine();
                Console.WriteLine($"Destination can't be the starting airport {startAirport}");
                Console.WriteLine();

                break;
            }

            string currentAirport = queue.Dequeue();

            if (currentAirport == endAirport)
            {
                break;
            }

            if (adjacencyList.TryGetValue(currentAirport, out List<(string, double, double)>? neighbors))
            {
                foreach ((string neighborAirport, _, _) in neighbors)
                {
                    if (!previousNodes.ContainsKey(neighborAirport))
                    {
                        previousNodes[neighborAirport] = currentAirport;
                        queue.Enqueue(neighborAirport);
                    }
                }
            }
        }

        if (!previousNodes.ContainsKey(endAirport) || startAirport == endAirport)
        {
            // If destination airport is not included as key it means it is not found
            return null;
        }

        string? currentNode = endAirport;

        while (currentNode != null)
        {
            shortestPath.Add(currentNode);
            currentNode = previousNodes[currentNode];
        }

        shortestPath.Reverse(); // Reversing the path to get correct order

        return shortestPath;
    }
}
