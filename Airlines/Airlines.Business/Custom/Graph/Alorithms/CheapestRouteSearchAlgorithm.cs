﻿using Airlines.Business.Custom.Graph.Interfaces;

namespace Airlines.Business.Custom.Graph.Alorithms;

#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable CA1854 // Prefer the 'IDictionary.TryGetValue(TKey, out TValue)' method
public class CheapestRouteSearchAlgorithm : IRouteSearch
{
    public List<string>? SearchPath(string startAirport, string endAirport, Dictionary<string, List<(string Destination, double Price, double Time)>> adjacencyList)
    {
        Dictionary<string, double> bestPrices = [];
        Dictionary<string, string?> previousNodes = [];
        PriorityQueue<string, double> priorityQueue = new PriorityQueue<string, double>();
        HashSet<string> visited = [];

        bestPrices[startAirport] = 0;
        priorityQueue.Enqueue(startAirport, 0);
        previousNodes[startAirport] = null;

        while (priorityQueue.Count > 0)
        {
            if (startAirport == endAirport)
            {
                Console.WriteLine();
                Console.WriteLine($"Destination can't be the starting airport {startAirport}");
                Console.WriteLine();

                break;
            }

            string? currentAirport = priorityQueue.Dequeue();
            visited.Add(currentAirport);

            if (currentAirport == endAirport)
            {
                break;
            }

            if (adjacencyList.TryGetValue(currentAirport, out List<(string, double, double)>? neighbors))
            {
                foreach ((string neighbor, double price, _) in neighbors)
                {
                    if (visited.Contains(neighbor))
                    {
                        continue;
                    }

                    double newPrice = bestPrices[currentAirport] + price;

                    if (!bestPrices.ContainsKey(neighbor) || newPrice < bestPrices[neighbor])
                    {
                        bestPrices[neighbor] = newPrice;
                        priorityQueue.Enqueue(neighbor, newPrice);
                        previousNodes[neighbor] = currentAirport;
                    }
                }
            }
        }

        if (!previousNodes.ContainsKey(endAirport) || startAirport == endAirport)
        {
            return null;
        }

        List<string> path = [];
        string? currentNode = endAirport;

        while (currentNode != null)
        {
            path.Add(currentNode);
            currentNode = previousNodes[currentNode];
        }

        path.Reverse();
        return path;
    }
}
