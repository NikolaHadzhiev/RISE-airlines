﻿namespace Airlines.Business.Custom.Graph.Interfaces;
public interface IRouteSearch
{
    public List<string>? SearchPath(string startAirport, string endAirport, Dictionary<string, List<(string Destination, double Price, double Time)>> adjacencyList);
}
