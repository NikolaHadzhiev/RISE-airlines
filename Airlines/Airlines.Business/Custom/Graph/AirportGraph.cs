﻿using Airlines.Business.Custom.Graph.Alorithms;
using Airlines.Business.Custom.Graph.Interfaces;
using Airlines.Persistence.Basic.Custom;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Custom.Graph;

#pragma warning disable CA1854 // Prefer the 'IDictionary.TryGetValue(TKey, out TValue)' method
#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable CA1822 // Mark members as static
public class AirportGraph
{
    public Dictionary<string, List<(string Destination, double Price, double Time)>> AdjacencyList { get; } = [];

    public IRouteSearch PathAlgorithm { get; set; } = new LeastStopsRouteSearchAlgorithm();

    private AirportGraph(FlightLinkedList flights)
    {
        foreach (FlightCSV flight in flights)
        {
            if (!AdjacencyList.ContainsKey(flight.DepartureAirport))
            {
                AdjacencyList[flight.DepartureAirport] = [];
            }

            AdjacencyList[flight.DepartureAirport].Add((flight.ArrivalAirport, flight.Price, flight.Time));

            // If flights are bi-directional, uncomment the line below
            // if (!adjacencyList.ContainsKey(flight.DestinationAirport))
            // {
            //    adjacencyList[flight.DestinationAirport] = new List<string>();
            // }
            //     
            // adjacencyList[flight.DestinationAirport].Add(flight.DepartureAirport);
        }
    }

    internal bool IsConnected(string startAirport, string endAirport)
    {
        HashSet<string> visited = [];
        Queue<string> queue = new Queue<string>();

        visited.Add(startAirport);
        queue.Enqueue(startAirport);

        while (queue.Count > 0)
        {
            if (startAirport == endAirport)
            {
                Console.WriteLine();
                Console.WriteLine($"Destination can't be the starting airport {startAirport}");
                Console.WriteLine();

                break;
            }

            string currentAirport = queue.Dequeue();

            if (currentAirport == endAirport)
            {
                return true;
            }

            if (AdjacencyList.ContainsKey(currentAirport))
            {
                foreach ((string neighbor, _, _) in AdjacencyList[currentAirport])
                {
                    if (visited.Contains(neighbor))
                    {
                        continue;
                    }

                    visited.Add(neighbor);
                    queue.Enqueue(neighbor);
                }
            }
        }

        return false;
    }
    internal List<string> CalculatePath(string startAirport, string endAirport, string strategy)
    {
        switch (strategy)
        {
            case "stops":
                PathAlgorithm = new LeastStopsRouteSearchAlgorithm();
                break;
            case "cheap":
                PathAlgorithm = new CheapestRouteSearchAlgorithm();
                break;
            case "short":
                PathAlgorithm = new ShortestTimeRouteSearchAlgorithm();
                break;
            default:
                break;
        }

        return PathAlgorithm.SearchPath(startAirport, endAirport, AdjacencyList)!;
    }
    internal void PrintRoute(string strategy, List<string> route)
    {
        switch (strategy)
        {
            case "stops":
                Print("Least stops", route);

                break;
            case "cheap":
                Print("Cheapest", route);

                break;
            case "short":
                Print("Shortest", route);

                break;
            default:
                Console.WriteLine();
                Console.WriteLine($"{strategy} is not a valid strategy");
                Console.WriteLine();

                break;
        }
    }

    private void Print(string strategy, List<string> route)
    {
        Console.WriteLine();
        Console.WriteLine($"{strategy} route found: {string.Join(" -> ", route)}");
        Console.WriteLine();
    }

    internal static AirportGraph CreateAirportGraph(FlightLinkedList flights) => new(flights);
}




