﻿namespace Airlines.Business.Extensions;
public static class SearchExtensions
{
    internal static int LinearSearch<T>(this T[] array, T target) where T : class, IComparable<T>
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i].ToString()?.CompareTo(target.ToString()) == 0)
            {
                return i; // Return the index of the found item
            }
        }

        return -1; // Return -1 if the item is not found
    }

    internal static int LinearSearch(this int[] array, int target) //Overload for int
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i].ToString().CompareTo(target.ToString()) == 0)
            {
                return i; // Return the index of the found item
            }
        }

        return -1; // Return -1 if the item is not found
    }

    internal static int BinarySearch<T>(this T[] array, T target, int min, int max) where T : class, IComparable<T>
    {
        if (min <= max)
        {
            int mid = min + ((max - min) / 2);
            int? comparisonResult = array[mid].ToString()?.CompareTo(target.ToString());

            return comparisonResult == 0
                ? mid
                : comparisonResult < 0 ? BinarySearch(array, target, mid + 1, max)
                : BinarySearch(array, target, min, mid - 1);
        }

        return -1;
    }
}
