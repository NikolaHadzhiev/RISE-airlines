﻿namespace Airlines.Business.Extensions;
public static class ValidateExtensions
{
    internal static bool IsUnique<T>(this T[] array, T item) where T : class, IComparable<T>
    {
        int index = array.LinearSearch(item); // Use LinearSearch method to find the index of the item

        if (index != -1)
        {
            Console.WriteLine($"{item} already exists in the list.");
        }

        return index == -1;
    }

    internal static bool IsUnique(this int[] array, int item) //Overload for int
    {
        int index = array.LinearSearch(item); // Use LinearSearch method to find the index of the item

        if (index != -1)
        {
            Console.WriteLine($"{item} already exists in the list.");
        }

        return index == -1;
    }

    internal static bool IsAirportValid(this string airport)
    {
        if (airport.Length != 3 || !airport.All(char.IsLetter))
        {
            Console.WriteLine($"AirportCSV '{airport}' is invalid.");
            Console.WriteLine("AirportCSV name must have exactly three alphabetic characters and must be unique.");
            return false;
        }

        return true;
    }

    internal static bool IsAirlineValid(this string airline)
    {
        if (airline.Length >= 6 || string.IsNullOrWhiteSpace(airline))
        {
            Console.WriteLine($"Airline '{airline}' is invalid.");
            Console.WriteLine("Airline name must have less than 6 characters and must be unique.");
            return false;
        }

        return true;
    }

    internal static bool IsFlightValid(this string flight)
    {
        if (string.IsNullOrWhiteSpace(flight) || !flight.All(char.IsLetterOrDigit))
        {
            Console.WriteLine($"FlightCSV '{flight}' is invalid.");
            Console.WriteLine("FlightCSV identifier must contain only alphanumeric characters and must be unique.");

            return false;
        }

        return true;
    }
}
