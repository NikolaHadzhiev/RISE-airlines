﻿namespace Airlines.Business.Extensions;

public static class SortExtenstions
{
    internal static void BubbleSort<T>(this T[] array, bool ascending = true) where T : class, IComparable<T>
    {
        int n = array.Length;
        for (int i = 0; i < n - 1; i++)
        {
            for (int j = 0; j < n - i - 1; j++)
            {
                if (ascending ? array[j].ToString()?.CompareTo(array[j + 1].ToString()) > 0
                              : array[j].ToString()?.CompareTo(array[j + 1].ToString()) < 0)
                {
                    (array[j + 1], array[j]) = (array[j], array[j + 1]);
                }
            }
        }
    }

    internal static void SelectionSort<T>(this T[] array, bool ascending = true) where T : class, IComparable<T>
    {
        int n = array.Length;
        for (int i = 0; i < n - 1; i++)
        {
            int minIndex = i;

            for (int j = i + 1; j < n; j++)
            {
                if (ascending ? array[j].ToString()?.CompareTo(array[minIndex].ToString()) < 0
                              : array[j].ToString()?.CompareTo(array[minIndex].ToString()) > 0)
                {
                    minIndex = j;
                }
            }

            if (minIndex != i)
            {
                (array[i], array[minIndex]) = (array[minIndex], array[i]);
            }
        }
    }
}
