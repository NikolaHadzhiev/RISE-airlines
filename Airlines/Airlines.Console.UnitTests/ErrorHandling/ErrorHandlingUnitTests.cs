﻿using Airlines.Business.Custom.Exceptions;
using Airlines.Persistence.Basic.Custom.Exceptions;

namespace Airlines.Console.UnitTests.ErrorHandling;

#pragma warning disable IDE0058 // Expression value is never used

[Collection("Sequential")]
public class ErrorHandlingUnitTests
{
    [Fact]
    public void DummyFunction_Thow_ThrowsInvalidAirportException()
    {
        //Arrange
        static void Dummy()
        {
            throw new InvalidAirportException("This is an invalid airport exception");
        }

        InvalidAirportException exception;

        try
        {
            Dummy();
        }
        catch (InvalidAirportException ex)
        {
            exception = ex;
        }
        //Act & Assert

        Assert.Throws<InvalidAirportException>(Dummy);
        Assert.Equal("This is an invalid airport exception", exception.Message);
        Assert.Equal("Identifier can contain only alphanumeric characters between 2 and 4 alphanumeric characters", exception.ValidIdentifier);
        Assert.Equal("Name can contain only alphabet and space characters", exception.ValidName);
        Assert.Equal("Country can contain only alphabet and space characters", exception.ValidCountry);
        Assert.Equal("AirportCSV with such identifier already exists. Must be unique", exception.UniqueAirport);
    }

    [Fact]
    public void DummyFunction_Thow_ThrowsInvalidAirlineException()
    {
        //Arrange
        static void Dummy()
        {
            throw new InvalidAirlineException("This is an invalid airline exception");
        }

        InvalidAirlineException exception;

        try
        {
            Dummy();
        }
        catch (InvalidAirlineException ex)
        {
            exception = ex;
        }
        //Act & Assert

        Assert.Throws<InvalidAirlineException>(Dummy);
        Assert.Equal("This is an invalid airline exception", exception.Message);
        Assert.Equal("Name should be between 1 and 5 characters and not empty", exception.ValidName);
    }

    [Fact]
    public void DummyFunction_Thow_ThrowsInvalidAircraftException()
    {
        //Arrange
        static void Dummy()
        {
            throw new InvalidAircraftException("This is an invalid aircraft exception");
        }

        InvalidAircraftException exception;

        try
        {
            Dummy();
        }
        catch (InvalidAircraftException ex)
        {
            exception = ex;
        }
        //Act & Assert

        Assert.Throws<InvalidAircraftException>(Dummy);
        Assert.Equal("This is an invalid aircraft exception", exception.Message);
    }

    [Fact]
    public void DummyFunction_Thow_ThrowsInvalidFlightException()
    {
        //Arrange
        static void Dummy()
        {
            throw new InvalidFlightException("This is an invalid flight exception");
        }

        InvalidFlightException exception;

        try
        {
            Dummy();
        }
        catch (InvalidFlightException ex)
        {
            exception = ex;
        }
        //Act & Assert

        Assert.Throws<InvalidFlightException>(Dummy);
        Assert.Equal("This is an invalid flight exception", exception.Message);
    }

    [Fact]
    public void DummyFunction_Thow_ThrowsInvalidCommandException()
    {
        //Arrange
        static void Dummy()
        {
            throw new InvalidCommandException("This is an invalid command exception");
        }

        InvalidCommandException exception;

        try
        {
            Dummy();
        }
        catch (InvalidCommandException ex)
        {
            exception = ex;
        }
        //Act & Assert

        Assert.Throws<InvalidCommandException>(Dummy);
        Assert.Equal("This is an invalid command exception", exception.Message);
    }
}
