﻿using Airlines.Persistence.Basic.Entities.Aircrafts;
using Airlines.Business.Utils.ReadUtilsMethods;

namespace Airlines.Console.UnitTests.Data;

#pragma warning disable xUnit2013 // Do not use equality check to check for collection size.

[Collection("Sequential")]
public class AircraftDataTests : IDisposable
{
    private readonly string _testFilePath;
    public AircraftDataTests() => _testFilePath = "test_aircraft.csv";
    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePath))
        {
            File.Delete(_testFilePath);
        }
    }

    [Fact]
    public void ReadAircraftData_WithValidData_ShouldPopulateDictionary()
    {
        // Arrange
        Dictionary<string, Aircraft> aircrafts;

        // Creating a test CSV file with valid data
        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        File.WriteAllLines(_testFilePath, testData);

        // Act
        ReadUtils.ReadAircraftData(_testFilePath, out aircrafts!);
        CargoAircraft cargoAircraft = (CargoAircraft)aircrafts["Boeing 747"];
        PassengerAircraft passengerAircraft = (PassengerAircraft)aircrafts["Airbus A320"];
        PrivateAircraft privateAircraft = (PrivateAircraft)aircrafts["Gulfstream G650"];

        // Assert
        Assert.Equal(3, aircrafts.Count);
        Assert.True(aircrafts.ContainsKey("Boeing 747"));
        Assert.True(aircrafts.ContainsKey("Airbus A320"));
        Assert.True(aircrafts.ContainsKey("Gulfstream G650"));

        Assert.Equal("CargoAircraft", aircrafts["Boeing 747"].GetType().Name);
        Assert.Equal("PassengerAircraft", aircrafts["Airbus A320"].GetType().Name);
        Assert.Equal("PrivateAircraft", aircrafts["Gulfstream G650"].GetType().Name);

        Assert.Equal(14000, cargoAircraft.CargoWeight);
        Assert.Equal(20000, passengerAircraft.CargoWeight);
        Assert.Equal(18, privateAircraft.Seats);
    }

    [Fact]
    public void ReadAircraftData_WithEmptyArgs_ShouldPopulateAircraftWithEmptyValues()
    {
        // Arrange
        Dictionary<string, Aircraft> aircrafts;

        // Creating a test CSV file with valid data
        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Airbus A320, -, -, -"
        ];

        File.WriteAllLines(_testFilePath, testData);

        // Act
        ReadUtils.ReadAircraftData(_testFilePath, out aircrafts!);
        PassengerAircraft passengerAircraft = (PassengerAircraft)aircrafts["Airbus A320"];

        // Assert
        Assert.Equal(1, aircrafts.Count);
        Assert.True(aircrafts.ContainsKey("Airbus A320"));

        Assert.Equal("PassengerAircraft", aircrafts["Airbus A320"].GetType().Name);

        Assert.Equal(0, passengerAircraft.CargoWeight);
        Assert.Equal(0, passengerAircraft.CargoVolume);
        Assert.Equal(0, passengerAircraft.Seats);
    }

    [Fact]
    public void ReadAircraftData_WithEmptyData_ShouldNotPopulateAicrafts()
    {
        // Arrange
        Dictionary<string, Aircraft> aircrafts;

        // Creating a test CSV file with valid data
        string[] testData = [];

        File.WriteAllLines(_testFilePath, testData);

        // Act
        ReadUtils.ReadAircraftData(_testFilePath, out aircrafts!);

        // Assert
        Assert.Equal(0, aircrafts.Count);
    }

    [Fact]
    public void ReadAircraftData_WithInvalidArgs_ShouldThrowIndexOutOfRange()
    {

        // Creating a test CSV file with valid data
        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Airbus A320, 20000"
        ];

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        File.WriteAllLines(_testFilePath, testData);

        // Act
        ReadUtils.ReadAircraftData(_testFilePath, out _);

        //Assert
        Assert.Contains("Not all needed values are provided in the aircrafts.csv", output.ToString());
    }
}
