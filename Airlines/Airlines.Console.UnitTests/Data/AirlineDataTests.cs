﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;

namespace Airlines.Console.UnitTests.Data.AirlineDataTests;

#pragma warning disable IDE0058 // Expression value is never used
public class AirlineDataTests : IDisposable
{
    private readonly string _testFilePath;

    public AirlineDataTests() => _testFilePath = "test_airlines.csv";
    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePath))
        {
            File.Delete(_testFilePath);
        }
    }

    [Fact]
    public void ReadAirlineData_WithValidData_ShouldPopulateDictionary()
    {
        // Arrange
        Dictionary<string, AirlineCSV> airlines;

        // Creating a test CSV file with valid data
        string[] testData = [
            "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
        ];

        File.WriteAllLines(_testFilePath, testData);

        // Act
        ReadUtils.ReadAirlineData(_testFilePath, out airlines!);

        // Assert
        Assert.Equal(2, airlines.Count);
        Assert.True(airlines.ContainsKey("JetX"));
        Assert.True(airlines.ContainsKey("AirX"));
    }

    [Fact]
    public void ReadAirlineData_WithInvalidData_ShouldSkipInvalidEntries()
    {
        // Arrange
        Dictionary<string, AirlineCSV> airlines;

        // Creating a test CSV file with one invalid entry
        string[] testData = [
            "Identifier, Name",
            "AA,JetX",
            "DL," // missing airline name
        ];

        File.WriteAllLines(_testFilePath, testData);

        // Act
        ReadUtils.ReadAirlineData(_testFilePath, out airlines!);

        // Assert
        Assert.Single(airlines);
        Assert.True(airlines.ContainsKey("JetX"));
    }

    [Fact]
    public void ReadAirlineData_WithDuplicateNames_ShouldSkipDuplicateEntries()
    {
        // Arrange
        Dictionary<string, AirlineCSV> airlines;

        // Creating a test CSV file with duplicate entries
        string[] testData = [
            "Identifier, Name",
            "AA,JetX",
            "DL,JetX" // duplicate airline name
        ];

        File.WriteAllLines(_testFilePath, testData);

        // Act
        ReadUtils.ReadAirlineData(_testFilePath, out airlines!);

        // Assert
        Assert.Single(airlines);
        Assert.True(airlines.ContainsKey("JetX"));
    }

    [Fact]
    public void ReadAirlineData_WithEmptyData_ThrowsIndexOutOfRange()
    {
        // Arrange

        // Creating a test CSV file with duplicate entries
        string[] testData = [
            "Identifier, Name",
            "AA"
         ];

        File.WriteAllLines(_testFilePath, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        // Act
        ReadUtils.ReadAirlineData(_testFilePath, out _);

        // Assert
        Assert.Contains($"Not all needed values are provided in the airlines.csv", output.ToString());
    }

    [Fact]
    public void ReadAirlineData_WithInvalidArgs_ShouldNotPopulateAirline()
    {
        // Arrange
        Dictionary<string, AirlineCSV> airlines;

        // Creating a test CSV file with duplicate entries
        string[] testData = [];

        File.WriteAllLines(_testFilePath, testData);

        // Act
        ReadUtils.ReadAirlineData(_testFilePath, out airlines!);

        //Assert
        Assert.Empty(airlines);
    }
}
