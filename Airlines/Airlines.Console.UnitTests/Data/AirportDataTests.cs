﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;

namespace Airlines.Console.UnitTests.Data.AirportDataTests;

#pragma warning disable xUnit2013 // Do not use equality check to check for collection size.

[Collection("Sequential")]
public class AirportDataTests : IDisposable
{
    private readonly string _testFilePath;
    public AirportDataTests() => _testFilePath = "test_airports.csv";

    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePath))
        {
            File.Delete(_testFilePath);
        }
    }

    [Fact]
    public void ReadAirportData_WithValidData_ShouldPopulateDictionaries()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports;
        Dictionary<string, HashSet<AirportCSV>> airportsByCity;
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry;

        // Creating a test CSV file with valid data
        string[] testData = [
            "Identifier, Name, City, Country",
            "JFK, John F. Kennedy International AirportCSV, New York, USA",
            "LAX, Los Angeles International AirportCSV, Los Angeles, USA"
        ];

        File.WriteAllLines(_testFilePath, testData);

        // Act
        ReadUtils.ReadAirportData(_testFilePath, out airports!, out airportsByCity!, out airportsByCountry!);

        // Assert
        Assert.Equal(2, airports.Count);
        Assert.True(airports.ContainsKey("JFK"));
        Assert.True(airports.ContainsKey("LAX"));
        Assert.Equal(2, airportsByCity.Count);
        Assert.True(airportsByCity.ContainsKey("New York"));
        Assert.True(airportsByCity.ContainsKey("Los Angeles"));
        Assert.Equal(1, airportsByCountry.Count);
        Assert.True(airportsByCountry.ContainsKey("USA"));
    }

    [Fact]
    public void ReadAirportData_WithInvalidData_ShouldSkipInvalidEntries()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports;
        Dictionary<string, HashSet<AirportCSV>> airportsByCity;
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry;

        // Creating a test CSV file with one invalid entry
        string[] testData = [
            "Identifier, Name, City, Country",
            "JFK111111, John F. Kennedy International AirportCSV, New York, USA",
            "ICN, Incheon International AirportCSV, Seoul, South Korea",
            "LAX, Los Angeles International AirportCSV, Los Angeles, USA",
        ];

        File.WriteAllLines(_testFilePath, testData);

        // Act
        ReadUtils.ReadAirportData(_testFilePath, out airports!, out airportsByCity!, out airportsByCountry!);

        // Assert
        Assert.Equal(2, airports.Count);
        Assert.True(airports.ContainsKey("LAX"));
        Assert.True(airports.ContainsKey("ICN"));
        Assert.Equal(2, airportsByCity.Count);
        Assert.True(airportsByCity.ContainsKey("Los Angeles"));
        Assert.Equal(2, airportsByCountry.Count);
        Assert.True(airportsByCountry.ContainsKey("South Korea"));
    }

    [Fact]
    public void ReadAirportData_WithEmptyData_ShouldNotPopulateAirport()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports;
        Dictionary<string, HashSet<AirportCSV>> airportsByCity;
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry;

        // Creating a test CSV file with one invalid entry
        string[] testData = [];

        File.WriteAllLines(_testFilePath, testData);

        // Act
        ReadUtils.ReadAirportData(_testFilePath, out airports!, out airportsByCity!, out airportsByCountry!);

        // Assert
        Assert.Empty(airports);
        Assert.Empty(airportsByCity);
        Assert.Empty(airportsByCountry);
    }

    [Fact]
    public void ReadAirportData_WithInvalidArgs_ShouldThrowIndexOutOfRange()
    {
        // Arrange

        // Creating a test CSV file with valid data
        string[] testData = [
           "Identifier, Name, City, Country",
            "JFK11, John F. Kennedy International AirportCSV, New York"
        ];

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        File.WriteAllLines(_testFilePath, testData);

        // Act
        ReadUtils.ReadAirportData(_testFilePath, out _, out _, out _);

        //Assert
        Assert.Contains("Not all needed values are provided in the airports.csv", output.ToString());
    }
}
