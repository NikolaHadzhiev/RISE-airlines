﻿using Airlines.Persistence.Basic.Custom.Exceptions;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Entities.Aircrafts;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Data.FlightDataTests;

[Collection("Sequential")]
public class FlightDataTests : IDisposable
{
    private readonly string _testFilePath;
    private readonly string _aircraftTestFilePath;
    private readonly string _testFlightRouteFilePath;
    public FlightDataTests()
    {
        _testFilePath = "test_flights.csv";
        _aircraftTestFilePath = "test_aircraft.csv";
        _testFlightRouteFilePath = "test_flight_route.csv";
    }

    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePath))
        {
            File.Delete(_testFilePath);
        }

        if (File.Exists(_aircraftTestFilePath))
        {
            File.Delete(_aircraftTestFilePath);
        }

        if (File.Exists(_testFlightRouteFilePath))
        {
            File.Delete(_testFlightRouteFilePath);
        }
    }

    [Fact]
    public void ReadFlightData_WithValidData_ShouldPopulateLinkedList()
    {
        // Arrange
        FlightLinkedList flights;
        Dictionary<string, Aircraft> aircrafts;

        // Creating a test CSV file with valid data
        string[] testData = [
            "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Boeing 747, 350, 4",
            "FL202, SFO, MIA, Boeing 747, 350, 4"
        ];

        string[] aircraftTestData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        File.WriteAllLines(_testFilePath, testData);
        File.WriteAllLines(_aircraftTestFilePath, aircraftTestData);

        // Act
        ReadUtils.ReadAircraftData(_aircraftTestFilePath, out aircrafts!);
        ReadUtils.ReadFlightData(_testFilePath, out flights!, aircrafts, out _);

        // Assert
        Assert.Equal(3, flights.Count());
        Assert.NotNull(flights.SearchFlight("FL789"));
        Assert.NotNull(flights.SearchFlight("FL101"));
        Assert.NotNull(flights.SearchFlight("FL202"));
    }

    [Fact]
    public void ReadFlightData_WithInvalidData_ShouldSkipDuplicateEntries()
    {
        // Arrange
        FlightLinkedList flights;
        Dictionary<string, Aircraft> aircrafts;

        // Creating a test CSV file with one invalid entry
        string[] testData = [
            "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Boeing 747, 350, 4",
            "FL101, SFO, MIA, Boeing 747, 350, 4",
            "FL444, STH, STH, Boeing 747, 350s, 4",
            "FL445, STH, STH, Boeing 747, 350, 4s"
        ];

        string[] aircraftTestData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        File.WriteAllLines(_testFilePath, testData);
        File.WriteAllLines(_aircraftTestFilePath, aircraftTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        // Act
        ReadUtils.ReadAircraftData(_aircraftTestFilePath, out aircrafts!);
        ReadUtils.ReadFlightData(_testFilePath, out flights!, aircrafts, out _);

        // Assert
        Assert.Equal(2, flights.Count());
        Assert.NotNull(flights.SearchFlight("FL789"));
        Assert.NotNull(flights.SearchFlight("FL101"));
        Assert.Contains($"FlightCSV FL101 already exists in the list. It will be skipped.", output.ToString());
        Assert.Contains($"FlightCSV FL444 price/time not in valid format.", output.ToString());
        Assert.Contains($"FlightCSV FL445 price/time not in valid format.", output.ToString());
    }

    [Fact]
    public void ReadFlightData_WithEmptyData_ShouldNotPopulateFlights()
    {
        // Arrange
        FlightLinkedList flights;
        Dictionary<string, FlightCSV> flightsForTree;
        Dictionary<string, Aircraft> aircrafts;

        // Creating a test CSV file with one invalid entry
        string[] testData = [];

        string[] aircraftTestData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        File.WriteAllLines(_testFilePath, testData);
        File.WriteAllLines(_aircraftTestFilePath, aircraftTestData);

        // Act
        ReadUtils.ReadAircraftData(_aircraftTestFilePath, out aircrafts!);
        ReadUtils.ReadFlightData(_testFilePath, out flights!, aircrafts, out flightsForTree!);

        // Assert
        Assert.Equal(0, flights.Count());
        Assert.Empty(flightsForTree);
    }

    [Fact]
    public void ReadFlightRouteData_WithInvalidData_ShouldThrowNullRefference()
    {
        // Arrange
        FlightLinkedList flights = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteList = [];
        Dictionary<string, Aircraft> aircrafts = [];
        // Creating a test CSV file with valid data
        string[] testData = [
            "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747",
            "FL101, DCA, SFO, Boeing 747",
            "FL202, SFO, MIA, Boeing 747"
        ];

        string[] aircraftTestData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        File.WriteAllLines(_testFilePath, testData);
        File.WriteAllLines(_aircraftTestFilePath, aircraftTestData);
        File.WriteAllLines(_testFlightRouteFilePath, []);

        // Act
        ReadUtils.ReadAircraftData(_aircraftTestFilePath, out aircrafts!);
        ReadUtils.ReadFlightData(_testFilePath, out flights!, aircrafts, out flightsForTree!);

        // Assert
        _ = Assert.Throws<NullReferenceException>(() => ReadUtils.ReadFlightRouteData(_testFlightRouteFilePath, out flightRouteList, flights));
    }

    [Fact]
    public void ReadFlightRouteData_WithInvalidData_ReturnsFlightNotFound()
    {
        // Arrange
        FlightLinkedList flights = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteList = [];
        Dictionary<string, Aircraft> aircrafts;
        // Creating a test CSV file with valid data
        string[] testData = [
            "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747",
            "FL101, DCA, SFO, Boeing 747",
            "FL202, SFO, MIA, Boeing 747"
        ];

        string[] aircraftTestData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        string[] flightRouteTestData = [
            "ORD",
            "NotFound",
            "FL202"
        ];

        File.WriteAllLines(_testFilePath, testData);
        File.WriteAllLines(_aircraftTestFilePath, aircraftTestData);
        File.WriteAllLines(_testFlightRouteFilePath, flightRouteTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        // Act
        ReadUtils.ReadAircraftData(_aircraftTestFilePath, out aircrafts!);
        ReadUtils.ReadFlightData(_testFilePath, out flights!, aircrafts, out flightsForTree!);

        // Assert
        InvalidFlightRouteIdentifierException exception = Assert.Throws<InvalidFlightRouteIdentifierException>(() => ReadUtils.ReadFlightRouteData(_testFlightRouteFilePath, out flightRouteList, flights));

        Assert.Equal("FlightCSV with identifier NotFound was not found", exception.Message);
    }

    [Fact]
    public void ReadFlightRouteData_WithInvalidData_ReturnsCorrectOutput()
    {
        // Arrange
        FlightLinkedList flights = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteList = [];
        Dictionary<string, Aircraft> aircrafts = [];
        // Creating a test CSV file with valid data
        string[] testData = [
            "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Boeing 747, 350, 4",
            "FL202, SFO, MIA, Boeing 747, 350, 4"
        ];

        string[] aircraftTestData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        string[] flightRouteTestData = [
            "ORD",
            "FL789",
            "FL202"
        ];

        File.WriteAllLines(_testFilePath, testData);
        File.WriteAllLines(_aircraftTestFilePath, aircraftTestData);
        File.WriteAllLines(_testFlightRouteFilePath, flightRouteTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        // Act
        ReadUtils.ReadAircraftData(_aircraftTestFilePath, out aircrafts!);
        ReadUtils.ReadFlightData(_testFilePath, out flights!, aircrafts, out flightsForTree!);

        // Assert
        InvalidFlightRouteIdentifierException exception = Assert.Throws<InvalidFlightRouteIdentifierException>(() => ReadUtils.ReadFlightRouteData(_testFlightRouteFilePath, out flightRouteList, flights));

        Assert.Equal($"Departure airport of flight FL202 is not ORD", exception.Message);
    }
}
