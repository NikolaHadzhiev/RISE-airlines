﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Commands.ReadMode.ReadModeUnitTests;

[Collection("Sequential")]
public class ReadModeUnitTests : IDisposable
{
    private readonly string _testFilePathAirports;
    private readonly string _testFilePathAirlines;
    private readonly string _testFilePathFlights;
    public ReadModeUnitTests()
    {
        _testFilePathAirports = "test_airports.csv";
        _testFilePathAirlines = "test_airlines.csv";
        _testFilePathFlights = "test_flights.csv";
    }

    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePathAirports))
        {
            File.Delete(_testFilePathAirports);
        }

        if (File.Exists(_testFilePathAirlines))
        {
            File.Delete(_testFilePathAirlines);
        }

        if (File.Exists(_testFilePathFlights))
        {
            File.Delete(_testFilePathFlights);
        }
    }

    [Fact]
    public void ReadUserCommands_Batch_ReturnsInvalidCommand()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
           "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
       ];

        File.WriteAllLines(_testFilePathAirlines, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"batch{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirlineData(_testFilePathAirlines, out airlines!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Invalid command.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Batch_ReturnsSkipCommand()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
           "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
       ];

        File.WriteAllLines(_testFilePathAirlines, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"batch start{Environment.NewLine}" +
                                              $"batch start{Environment.NewLine}" +
                                              $"End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirlineData(_testFilePathAirlines, out airlines!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Batch mode activated", output.ToString());
        Assert.Contains("Command skiped", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Batch_AddCommandsSuccesfully()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
           "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
       ];

        File.WriteAllLines(_testFilePathAirlines, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"batch start{Environment.NewLine}" +
                                              $"exist JetX{Environment.NewLine}" +
                                              $"exist AirX{Environment.NewLine}" +
                                              $"End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirlineData(_testFilePathAirlines, out airlines!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Batch mode activated", output.ToString());
        Assert.Contains("Command added to batch successfully", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Batch_RunCommandsSuccesfully()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
           "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
       ];

        File.WriteAllLines(_testFilePathAirlines, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"batch start{Environment.NewLine}" +
                                              $"exist JetX{Environment.NewLine}" +
                                              $"exist AirX{Environment.NewLine}" +
                                              $"batch run{Environment.NewLine}" +
                                              $"End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirlineData(_testFilePathAirlines, out airlines!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Batch mode activated", output.ToString());
        Assert.Contains("Command added to batch successfully", output.ToString());
        Assert.Contains("Running batch commands...", output.ToString());
        Assert.Contains("Airline with name 'JetX' exists", output.ToString());
        Assert.Contains("Airline with name 'AirX' exists", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Batch_CancelCommandsSuccesfully()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
           "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
       ];

        File.WriteAllLines(_testFilePathAirlines, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"batch start{Environment.NewLine}" +
                                              $"exist JetX{Environment.NewLine}" +
                                              $"exist AirX{Environment.NewLine}" +
                                              $"batch cancel{Environment.NewLine}" +
                                              $"End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirlineData(_testFilePathAirlines, out airlines!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Batch mode activated", output.ToString());
        Assert.Contains("Command added to batch successfully", output.ToString());
        Assert.Contains("Batch queue cleared and canceled", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_ImmediateExecution_ExecutesImmediately()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Boeing 747, 350, 4",
            "FL202, SFO, MIA, Boeing 747, 350, 4"
       ];

        File.WriteAllLines(_testFilePathFlights, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"search FL101{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);
        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains($"FlightCSV with ID: 'FL101' found", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Batch_EnsureSmoothTransitionWhenSwitching()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Boeing 747, 350, 4",
            "FL202, SFO, MIA, Boeing 747, 350, 4"
       ];

        File.WriteAllLines(_testFilePathFlights, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"search FL101{Environment.NewLine}" +
                                              $"batch start{Environment.NewLine}" +
                                              $"search FL202{Environment.NewLine}" +
                                              $"search FL789{Environment.NewLine}" +
                                              $"search NotFound{Environment.NewLine}" +
                                              $"batch run{Environment.NewLine}" +
                                              $"search NotFoundAfterBatch{Environment.NewLine}" +
                                              $"End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);
        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains($"FlightCSV with ID: 'FL101' found", output.ToString());
        Assert.Contains("Batch mode activated", output.ToString());
        Assert.Contains("Command added to batch successfully", output.ToString());
        Assert.Contains("Running batch commands...", output.ToString());
        Assert.Contains($"FlightCSV with ID: 'FL202' found", output.ToString());
        Assert.Contains($"FlightCSV with ID: 'FL789' found", output.ToString());
        Assert.Contains("'NotFound' not found.", output.ToString());
        Assert.Contains("'NotFoundAfterBatch' not found.", output.ToString());
    }
}