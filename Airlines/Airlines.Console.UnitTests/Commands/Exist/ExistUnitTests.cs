﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Commands.Exist.ExistUnitTests;

[Collection("Sequential")]
public class ExistUnitTests : IDisposable
{
    private readonly string _testFilePathAirlines;
    public ExistUnitTests() => _testFilePathAirlines = "test_airlines.csv";

    public void Dispose()
    {
        // Clean up test file after tests

        if (File.Exists(_testFilePathAirlines))
        {
            File.Delete(_testFilePathAirlines);
        }
    }

    [Fact]
    public void ReadUserCommands_Exist_ReturnsInvalidCommand()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
           "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
       ];

        File.WriteAllLines(_testFilePathAirlines, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"exist{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirlineData(_testFilePathAirlines, out airlines!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Invalid exist command.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Exist_ReturnsAirlineExists()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];


        string[] testData = [
           "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
       ];

        File.WriteAllLines(_testFilePathAirlines, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"exist JetX{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirlineData(_testFilePathAirlines, out airlines!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Airline with name 'JetX' exists", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Exist_ReturnsDoesNotExist()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
           "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
       ];

        File.WriteAllLines(_testFilePathAirlines, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"exist NotExist{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirlineData(_testFilePathAirlines, out airlines!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Airline does not exist", output.ToString());
    }
}
