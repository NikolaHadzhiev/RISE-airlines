﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Commands.List.ListUnitTests;

[Collection("Sequential")]
public class ListUnitTests : IDisposable
{
    private readonly string _testFilePathAirports;
    public ListUnitTests() => _testFilePathAirports = "test_airports.csv";

    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePathAirports))
        {
            File.Delete(_testFilePathAirports);
        }
    }

    [Fact]
    public void ReadUserCommands_List_ReturnsInvalidCommand()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports;
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity;
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry;

        string[] airportTestData = [
           "Identifier, Name, City, Country",
            "JFK, John F. Kennedy International AirportCSV, New York, USA",
            "LAX, Los Angeles International AirportCSV, Los Angeles, USA"
       ];

        File.WriteAllLines(_testFilePathAirports, airportTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"list{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirportData(_testFilePathAirports, out airports!, out airportsByCity!, out airportsByCountry!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Invalid list command.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_List_ReturnsCorrectOutput()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports;
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity;
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry;

        string[] airportTestData = [
           "Identifier, Name, City, Country",
            "JFK, John F. Kennedy International AirportCSV, New York, USA",
            "LAX, Los Angeles International AirportCSV, Los Angeles, USA"
       ];

        File.WriteAllLines(_testFilePathAirports, airportTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"list Los Angeles city" +
                                               $"{Environment.NewLine}list USA country" +
                                               $"{Environment.NewLine}list notFound city" +
                                               $"{Environment.NewLine}list notFound country" +
                                               $"{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirportData(_testFilePathAirports, out airports!, out airportsByCity!, out airportsByCountry!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        string airportsCity = $"{Environment.NewLine}JFK-John F. Kennedy International AirportCSV-New York-USA{Environment.NewLine}LAX-Los Angeles International AirportCSV-Los Angeles-USA";
        string airportsCountry = $"{Environment.NewLine}LAX-Los Angeles International AirportCSV-Los Angeles-USA";
        string airportCityNotFound = "No airports found in the city 'notFound'.";
        string aiportCountryNotFound = "No airports found in the country 'notFound'.";

        // Assert
        Assert.Contains(airportsCity, output.ToString());
        Assert.Contains(airportsCountry, output.ToString());
        Assert.Contains(airportCityNotFound, output.ToString());
        Assert.Contains(aiportCountryNotFound, output.ToString());
    }
}
