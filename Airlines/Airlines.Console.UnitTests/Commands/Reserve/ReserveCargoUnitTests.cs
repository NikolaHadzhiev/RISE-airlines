﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Entities.Aircrafts;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Commands.Reserve;

[Collection("Sequential")]
public class ReserveCargoUnitTests : IDisposable
{
    private readonly string _testFilePathAircraft;
    private readonly string _testFilePathFlights;

    public ReserveCargoUnitTests()
    {
        _testFilePathAircraft = "test_aircrafts.csv";
        _testFilePathFlights = "test_flights.csv";
    }

    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePathAircraft))
        {
            File.Delete(_testFilePathAircraft);
        }

        if (File.Exists(_testFilePathFlights))
        {
            File.Delete(_testFilePathFlights);
        }
    }

    [Fact]
    public void ReadUserCommands_Reserve_ReturnsInvalidCommand()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        File.WriteAllLines(_testFilePathAircraft, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"reserve{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAircraftData(_testFilePathAircraft, out _);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Invalid reserve command.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Reserve_ReturnsMissing()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];
        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        File.WriteAllLines(_testFilePathAircraft, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"reserve cargo{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAircraftData(_testFilePathAircraft, out _);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Reserve command is missing flight identifier, cargo weight or cargo volume.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Reserve_ReturnsFlightNotFound()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        File.WriteAllLines(_testFilePathAircraft, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"reserve cargo NotFound 0 0{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAircraftData(_testFilePathAircraft, out _);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("FlightCSV 'NotFound' not found.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Reserve_ReturnsIncompatibleAircraft()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        Dictionary<string, Aircraft> aircrafts;

        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Airbus A320, 350, 4",
            "FL202, SFO, MIA, Airbus A320, 350, 4"
       ];

        File.WriteAllLines(_testFilePathAircraft, testData);
        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"reserve cargo FL101 0 0{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act

        ReadUtils.ReadAircraftData(_testFilePathAircraft, out aircrafts!);
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, aircrafts, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Aircraft 'Airbus A320' is not compatible for cargo requests.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Reserve_ReturnsInsufficientAircraft()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        Dictionary<string, Aircraft> aircrafts;

        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Airbus A320, 350, 4",
            "FL202, SFO, MIA, Airbus A320, 350, 4"
       ];

        File.WriteAllLines(_testFilePathAircraft, testData);
        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"reserve cargo FL789 25000 10000{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act

        ReadUtils.ReadAircraftData(_testFilePathAircraft, out aircrafts!);
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, aircrafts, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Aircraft Boeing 747 cannot be reserved due to insufficient weight/volume", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Reserve_ReservesCargoSuccesfully()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        Dictionary<string, Aircraft> aircrafts;

        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Airbus A320, 350, 4",
            "FL202, SFO, MIA, Airbus A320, 350, 4"
       ];

        File.WriteAllLines(_testFilePathAircraft, testData);
        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"reserve cargo FL789 14000 854.5{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act

        ReadUtils.ReadAircraftData(_testFilePathAircraft, out aircrafts!);
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, aircrafts, out flightsForTree!);

        FlightCSV? cargoFlight = flights.SearchFlight("FL789");
        CargoAircraft cargoAircraft = (CargoAircraft)cargoFlight!.Aircraft!;

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("FL789 succesfully reserved", output.ToString());
        Assert.Equal(0, cargoAircraft.CargoWeight);
        Assert.Equal(0, cargoAircraft.CargoVolume);
    }
}
