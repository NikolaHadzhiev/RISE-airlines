﻿using Airlines.Persistence.Basic.Entities.Aircrafts;
using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Commands.Reserve;


[Collection("Sequential")]
public class ReserveTicketUnitTests : IDisposable
{
    private readonly string _testFilePathAircraft;
    private readonly string _testFilePathFlights;

    public ReserveTicketUnitTests()
    {
        _testFilePathAircraft = "test_aircrafts.csv";
        _testFilePathFlights = "test_flights.csv";
    }

    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePathAircraft))
        {
            File.Delete(_testFilePathAircraft);
        }

        if (File.Exists(_testFilePathFlights))
        {
            File.Delete(_testFilePathFlights);
        }
    }

    [Fact]
    public void ReadUserCommands_Reserve_ReturnsMissing()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];
        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        File.WriteAllLines(_testFilePathAircraft, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"reserve ticket{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAircraftData(_testFilePathAircraft, out _);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Reserve command is missing flight identifier, seats, small baggage count or large baggage count.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Reserve_ReturnsFlightNotFound()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        File.WriteAllLines(_testFilePathAircraft, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"reserve ticket NotFound 0 0 0{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAircraftData(_testFilePathAircraft, out _);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("FlightCSV 'NotFound' not found.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Reserve_ReturnsIncompatibleAircraft()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        Dictionary<string, Aircraft> aircrafts;

        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Airbus A320, 350, 4",
            "FL202, SFO, MIA, Airbus A320, 350, 4"
       ];

        File.WriteAllLines(_testFilePathAircraft, testData);
        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"reserve ticket FL789 0 0 0{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act

        ReadUtils.ReadAircraftData(_testFilePathAircraft, out aircrafts!);
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, aircrafts, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Aircraft 'Boeing 747' is not compatible for passenger requests.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Reserve_ReturnsInsufficientAircraft()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        Dictionary<string, Aircraft> aircrafts;

        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Airbus A320, 350, 4",
            "FL202, SFO, MIA, Airbus A320, 350, 4"
       ];

        File.WriteAllLines(_testFilePathAircraft, testData);
        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"reserve ticket FL101 25000 10000 10000{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act

        ReadUtils.ReadAircraftData(_testFilePathAircraft, out aircrafts!);
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, aircrafts, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Aircraft Airbus A320 cannot be reserved due to insufficient weight/volume/seats", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Reserve_ReservesPassengerSuccesfully()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        Dictionary<string, Aircraft> aircrafts;

        string[] testData = [
            "Aircraft Model, Cargo Weight, Cargo Volume, Seats",
            "Boeing 747, 14000, 854.5, -",
            "Airbus A320, 20000, 37.4, 150",
            "Gulfstream G650, -, -, 18"
        ];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Airbus A320, 350, 4",
            "FL202, SFO, MIA, Airbus A320, 350, 4"
       ];

        File.WriteAllLines(_testFilePathAircraft, testData);
        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"reserve ticket FL101 1 1 1{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act

        ReadUtils.ReadAircraftData(_testFilePathAircraft, out aircrafts!);
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, aircrafts, out flightsForTree!);

        FlightCSV? passengerFlight = flights.SearchFlight("FL101");
        PassengerAircraft passengerAircraft = (PassengerAircraft)passengerFlight!.Aircraft!;

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("FL101 succesfully reserved", output.ToString());
        Assert.Equal(19955, passengerAircraft.CargoWeight);
        Assert.Equal(37.265, passengerAircraft.CargoVolume);
        Assert.Equal(149, passengerAircraft.Seats);
    }
}
