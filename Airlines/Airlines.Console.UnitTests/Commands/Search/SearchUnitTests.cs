﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Commands.Search.SearchUnitTests;

[Collection("Sequential")]
public class SearchUnitTests : IDisposable
{
    private readonly string _testFilePathAirports;
    private readonly string _testFilePathAirlines;
    private readonly string _testFilePathFlights;
    public SearchUnitTests()
    {
        _testFilePathAirports = "test_airports.csv";
        _testFilePathAirlines = "test_airlines.csv";
        _testFilePathFlights = "test_flights.csv";
    }

    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePathAirports))
        {
            File.Delete(_testFilePathAirports);
        }

        if (File.Exists(_testFilePathAirlines))
        {
            File.Delete(_testFilePathAirlines);
        }

        if (File.Exists(_testFilePathFlights))
        {
            File.Delete(_testFilePathFlights);
        }
    }

    [Fact]
    public void ReadUserCommands_EndCommand_ExitsLoop()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader("end");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("End of program. Press any key to exit.", output.ToString());
        Assert.DoesNotContain("Invalid command.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Search_ReturnsSearchedAirport()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports;
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity;
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry;


        string[] testData = [
           "Identifier, Name, City, Country",
            "JFK, John F. Kennedy International AirportCSV, New York, USA",
            "LAX, Los Angeles International AirportCSV, Los Angeles, USA"
       ];

        File.WriteAllLines(_testFilePathAirports, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"search JFK{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirportData(_testFilePathAirports, out airports!, out airportsByCity!, out airportsByCountry!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("AirportCSV with ID: 'JFK' found", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Search_ReturnsSearchedAirline()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];


        string[] testData = [
           "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
       ];

        File.WriteAllLines(_testFilePathAirlines, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"search AirX{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirlineData(_testFilePathAirlines, out airlines!);
        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Airline with Name: 'AirX' found", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Search_ReturnsSearchedFlight()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];


        string[] testData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Boeing 747, 350, 4",
            "FL202, SFO, MIA, Boeing 747, 350, 4"
       ];

        File.WriteAllLines(_testFilePathFlights, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"search FL101{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);
        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains($"FlightCSV with ID: 'FL101' found", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Search_ReturnsNotFound()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];


        string[] testData = [
           "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
       ];

        File.WriteAllLines(_testFilePathAirlines, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"search NotFound{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirlineData(_testFilePathAirlines, out airlines!);
        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("'NotFound' not found.", output.ToString());
    }
}
