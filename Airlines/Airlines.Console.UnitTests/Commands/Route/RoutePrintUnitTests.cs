﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Commands.Route.RoutePrintUnitTests;

[Collection("Sequential")]
public class RoutePrintUnitTests : IDisposable
{
    private readonly string _testFilePathFlights;
    public RoutePrintUnitTests() => _testFilePathFlights = "test_flights.csv";
    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePathFlights))
        {
            File.Delete(_testFilePathFlights);
        }
    }

    [Fact]
    public void ReadUserCommands_RoutePrint_DisplaysEmptyMessage()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747",
            "FL101, DCA, SFO, Boeing 747",
            "FL202, SFO, MIA, Boeing 747"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route print{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Equal(flightRoute?.Count(), 0);
        Assert.Contains("Route is empty.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RoutePrint_PrintsFlights()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Boeing 747, 350, 4",
            "FL202, SFO, MIA, Boeing 747, 350, 4"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route add FL101{Environment.NewLine}" +
                                     $"route add FL202{Environment.NewLine}" +
                                     $"route print{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        string expectedFlights = $"{Environment.NewLine}FlightCSV FL101: DCA to SFO" +
                         $"{Environment.NewLine}FlightCSV FL202: SFO to MIA";

        // Assert
        Assert.Equal(flightRoute?.Count(), 2);
        Assert.Contains(expectedFlights, output.ToString());
    }
}
