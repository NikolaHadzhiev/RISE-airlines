﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Commands.Route.RouteSearchUnitTests;

[Collection("Sequential")]
public class RouteSearchUnitTests : IDisposable
{
    private readonly string _testFilePathFlights;

    public RouteSearchUnitTests() => _testFilePathFlights = "test_flights.csv";
    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePathFlights))
        {
            File.Delete(_testFilePathFlights);
        }
    }

    [Fact]
    public void ReadUserCommands_RouteSearch_ReturnsInvalidCommand()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport, Aircraft Model, Price, Time",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 200, 6",
            "FL000, JFK, BOS, Airbus A320, 180, 2.5",
            "FL707, SEA, BOS, Airbus A320, 180, 2.5",
            "FL808, BOS, DCA, Airbus A320, 100, 1.5",
            "FL101, DCA, SFO, Airbus A320, 300, 6.5",
            "FL909, SFO, LAX, Airbus A320, 150, 1.5",
            "FL456, LAX, ORD, Airbus A320, 100, 1",
            "FL010, ORD, ATL, Airbus A320, 150, 2.5",
            "FL111, ATL, MIA, Airbus A320, 200, 3.5",
            "FL202, SFO, MIA, Boeing 747, 400, 7",
            "FL303, BOS, POR, Airbus A320, 160, 2.5",
            "FL404, SEA, ARG, Boeing 747, 380, 6.5",
            "FL192, JFK, ORD, Boeing 747, 450, 8",
            "FL412, ORD, ARG, Airbus A320, 280, 4.5",
            "FL444, DCA, BOS, Airbus A320, 120, 1.8"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route search{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Route search command is missing start airport/end airport/strategy option.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RouteSearch_ReturnsInvalidStrategy()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport, Aircraft Model, Price, Time",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 200, 6",
            "FL000, JFK, BOS, Airbus A320, 180, 2.5",
            "FL707, SEA, BOS, Airbus A320, 180, 2.5",
            "FL808, BOS, DCA, Airbus A320, 100, 1.5",
            "FL101, DCA, SFO, Airbus A320, 300, 6.5",
            "FL909, SFO, LAX, Airbus A320, 150, 1.5",
            "FL456, LAX, ORD, Airbus A320, 100, 1",
            "FL010, ORD, ATL, Airbus A320, 150, 2.5",
            "FL111, ATL, MIA, Airbus A320, 200, 3.5",
            "FL202, SFO, MIA, Boeing 747, 400, 7",
            "FL303, BOS, POR, Airbus A320, 160, 2.5",
            "FL404, SEA, ARG, Boeing 747, 380, 6.5",
            "FL192, JFK, ORD, Boeing 747, 450, 8",
            "FL412, ORD, ARG, Airbus A320, 280, 4.5",
            "FL444, DCA, BOS, Airbus A320, 120, 1.8"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route search DCA BOS INVALID{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains($"INVALID is not a valid strategy", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RouteSearch_ReturnsRouteNotConnectedWithInvalidAirport()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport, Aircraft Model, Price, Time",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 200, 6",
            "FL000, JFK, BOS, Airbus A320, 180, 2.5",
            "FL707, SEA, BOS, Airbus A320, 180, 2.5",
            "FL808, BOS, DCA, Airbus A320, 100, 1.5",
            "FL101, DCA, SFO, Airbus A320, 300, 6.5",
            "FL909, SFO, LAX, Airbus A320, 150, 1.5",
            "FL456, LAX, ORD, Airbus A320, 100, 1",
            "FL010, ORD, ATL, Airbus A320, 150, 2.5",
            "FL111, ATL, MIA, Airbus A320, 200, 3.5",
            "FL202, SFO, MIA, Boeing 747, 400, 7",
            "FL303, BOS, POR, Airbus A320, 160, 2.5",
            "FL404, SEA, ARG, Boeing 747, 380, 6.5",
            "FL192, JFK, ORD, Boeing 747, 450, 8",
            "FL412, ORD, ARG, Airbus A320, 280, 4.5",
            "FL444, DCA, BOS, Airbus A320, 120, 1.8"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route search DFW DFW stops" +
                                              $"{Environment.NewLine}route search ORD ORD cheap" +
                                              $"{Environment.NewLine}route search ATL ATL short" +
                                              $"{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Destination can't be the starting airport DFW", output.ToString());
        Assert.Contains("Destination can't be the starting airport ORD", output.ToString());
        Assert.Contains("Destination can't be the starting airport ATL", output.ToString());
        Assert.Contains("No route found.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RouteSearch_ReturnsRouteConnectedIndirectly()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport, Aircraft Model, Price, Time",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 200, 6",
            "FL000, JFK, BOS, Airbus A320, 180, 2.5",
            "FL707, SEA, BOS, Airbus A320, 180, 2.5",
            "FL808, BOS, DCA, Airbus A320, 100, 1.5",
            "FL101, DCA, SFO, Airbus A320, 300, 6.5",
            "FL909, SFO, LAX, Airbus A320, 150, 1.5",
            "FL456, LAX, ORD, Airbus A320, 100, 1",
            "FL010, ORD, ATL, Airbus A320, 150, 2.5",
            "FL111, ATL, MIA, Airbus A320, 200, 3.5",
            "FL202, SFO, MIA, Boeing 747, 400, 7",
            "FL303, BOS, POR, Airbus A320, 160, 2.5",
            "FL404, SEA, ARG, Boeing 747, 380, 6.5",
            "FL192, JFK, ORD, Boeing 747, 450, 8",
            "FL412, ORD, ARG, Airbus A320, 280, 4.5",
            "FL444, DCA, BOS, Airbus A320, 120, 1.8"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route search DFW ARG stops" +
                                              $"{Environment.NewLine}route search DFW POR cheap" +
                                              $"{Environment.NewLine}route search DFW POR short" +
                                              $"{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Least stops route found: DFW -> SEA -> ARG", output.ToString());
        Assert.Contains("Cheapest route found: DFW -> SEA -> BOS -> POR", output.ToString());
        Assert.Contains("Shortest route found: DFW -> JFK -> BOS -> POR", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RouteSearch_ReturnsRouteConnectedDirectly()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport, Aircraft Model, Price, Time",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 200, 6",
            "FL000, JFK, BOS, Airbus A320, 180, 2.5",
            "FL707, SEA, BOS, Airbus A320, 180, 2.5",
            "FL808, BOS, DCA, Airbus A320, 100, 1.5",
            "FL101, DCA, SFO, Airbus A320, 300, 6.5",
            "FL909, SFO, LAX, Airbus A320, 150, 1.5",
            "FL456, LAX, ORD, Airbus A320, 100, 1",
            "FL010, ORD, ATL, Airbus A320, 150, 2.5",
            "FL111, ATL, MIA, Airbus A320, 200, 3.5",
            "FL202, SFO, MIA, Boeing 747, 400, 7",
            "FL303, BOS, POR, Airbus A320, 160, 2.5",
            "FL404, SEA, ARG, Boeing 747, 380, 6.5",
            "FL192, JFK, ORD, Boeing 747, 450, 8",
            "FL412, ORD, ARG, Airbus A320, 280, 4.5",
            "FL444, DCA, BOS, Airbus A320, 120, 1.8"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route search DFW SEA stops" +
                                              $"{Environment.NewLine}route search DFW JFK cheap" +
                                              $"{Environment.NewLine}route search DFW JFK short" +
                                              $"{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Least stops route found: DFW -> SEA", output.ToString());
        Assert.Contains("Cheapest route found: DFW -> JFK", output.ToString());
        Assert.Contains("Shortest route found: DFW -> JFK", output.ToString());
    }
}
