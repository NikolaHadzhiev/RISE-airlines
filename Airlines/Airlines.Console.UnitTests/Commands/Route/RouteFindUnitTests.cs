﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Commands.Route.RouteFindUnitTests;

[Collection("Sequential")]
public class RouteFindUnitTests : IDisposable
{
    private readonly string _testFilePathFlights;
    private readonly string _testFileFlightRoute;

    public RouteFindUnitTests()
    {
        _testFilePathFlights = "test_flights.csv";
        _testFileFlightRoute = "test_flightRoute.csv";
    }

    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePathFlights))
        {
            File.Delete(_testFilePathFlights);
        }

        if (File.Exists(_testFileFlightRoute))
        {
            File.Delete(_testFileFlightRoute);
        }
    }

    [Fact]
    public void ReadUserCommands_RouteFind_ReturnsInvalidFindCommand()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747",
            "FL101, DCA, SFO, Boeing 747",
            "FL202, SFO, MIA, Boeing 747"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route find{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Find command is missing desired destination airport.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RouteFind_PopulatesRouteTree()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree;

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 350, 4",
            "FL707, SEA, BOS, Airbus A320, 350, 4"
       ];

        string[] flightRoutTestData = [
            "DFW",
            "FL505",
            "FL606"
       ];

        string expectedResult = $"└── DFW{Environment.NewLine}│   └── JFK{Environment.NewLine}    └── SEA{Environment.NewLine}        └── BOS";

        File.WriteAllLines(_testFilePathFlights, flightTestData);
        File.WriteAllLines(_testFileFlightRoute, flightRoutTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route find MIA{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);
        ReadUtils.ReadFlightRouteData(_testFileFlightRoute, out flightRouteListForTree, flights);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains(expectedResult, output.ToString());
        Assert.Equal(3, flightsForTree.Count);
    }

    [Fact]
    public void ReadUserCommands_RouteFind_ReturnsRouteNotFoundWithInvalidAirport()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree;

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 350, 4",
            "FL707, SEA, BOS, Airbus A320, 350, 4"
       ];

        string[] flightRoutTestData = [
            "DFW",
            "FL505",
            "FL606"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);
        File.WriteAllLines(_testFileFlightRoute, flightRoutTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route find DFW{Environment.NewLine}End");
        System.Console.SetIn(input);

        // Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);
        ReadUtils.ReadFlightRouteData(_testFileFlightRoute, out flightRouteListForTree, flights);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Destination can't be the starting airport DFW", output.ToString());
        Assert.Contains("No route found.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RouteFind_ReturnsRouteNotFound()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree;

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 350, 4",
            "FL707, SEA, BOS, Airbus A320, 350, 4"
       ];

        string[] flightRoutTestData = [
            "DFW",
            "FL505",
            "FL606"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);
        File.WriteAllLines(_testFileFlightRoute, flightRoutTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route find NotFound{Environment.NewLine}End");
        System.Console.SetIn(input);

        // Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);
        ReadUtils.ReadFlightRouteData(_testFileFlightRoute, out flightRouteListForTree, flights);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("No route found.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RouteFind_ReturnsNotFoundForParentAirport()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree;

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 350, 4",
            "FL707, SEA, BOS, Airbus A320, 350, 4"
       ];

        string[] flightRoutTestData = [
            "SEA",
            "FL707"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);
        File.WriteAllLines(_testFileFlightRoute, flightRoutTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route find DFW{Environment.NewLine}End");
        System.Console.SetIn(input);

        // Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);
        ReadUtils.ReadFlightRouteData(_testFileFlightRoute, out flightRouteListForTree, flights);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("No route found.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RouteFind_ReturnsDirectRoute()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree;

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 350, 4",
            "FL707, SEA, BOS, Airbus A320, 350, 4"
       ];

        string[] flightRoutTestData = [
            "DFW",
            "FL505",
            "FL606"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);
        File.WriteAllLines(_testFileFlightRoute, flightRoutTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route find SEA{Environment.NewLine}End");
        System.Console.SetIn(input);

        // Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);
        ReadUtils.ReadFlightRouteData(_testFileFlightRoute, out flightRouteListForTree, flights);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("DFW -> SEA", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RouteFind_ReturnsIndirectRoute()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree;

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 350, 4",
            "FL707, SEA, BOS, Airbus A320, 350, 4"
       ];

        string[] flightRoutTestData = [
            "DFW",
            "FL505",
            "FL606"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);
        File.WriteAllLines(_testFileFlightRoute, flightRoutTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route find BOS{Environment.NewLine}End");
        System.Console.SetIn(input);

        // Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);
        ReadUtils.ReadFlightRouteData(_testFileFlightRoute, out flightRouteListForTree, flights);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("DFW -> SEA -> BOS", output.ToString());
    }
}
