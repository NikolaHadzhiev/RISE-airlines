﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Commands.Route.RouteCheckUnitTests;

[Collection("Sequential")]
public class RouteCheckUnitTests : IDisposable
{
    private readonly string _testFilePathFlights;

    public RouteCheckUnitTests() => _testFilePathFlights = "test_flights.csv";
    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePathFlights))
        {
            File.Delete(_testFilePathFlights);
        }
    }

    [Fact]
    public void ReadUserCommands_RouteCheck_ReturnsInvalidCommand()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport, Aircraft Model, Price, Time",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 200, 6",
            "FL000, JFK, BOS, Airbus A320, 180, 2.5",
            "FL707, SEA, BOS, Airbus A320, 180, 2.5",
            "FL808, BOS, DCA, Airbus A320, 100, 1.5",
            "FL101, DCA, SFO, Airbus A320, 300, 6.5",
            "FL909, SFO, LAX, Airbus A320, 150, 1.5",
            "FL456, LAX, ORD, Airbus A320, 100, 1",
            "FL010, ORD, ATL, Airbus A320, 150, 2.5",
            "FL111, ATL, MIA, Airbus A320, 200, 3.5",
            "FL202, SFO, MIA, Boeing 747, 400, 7",
            "FL303, BOS, POR, Airbus A320, 160, 2.5",
            "FL404, SEA, ARG, Boeing 747, 380, 6.5",
            "FL192, JFK, ORD, Boeing 747, 450, 8",
            "FL412, ORD, ARG, Airbus A320, 280, 4.5",
            "FL444, DCA, BOS, Airbus A320, 120, 1.8"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route check{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Route check command is missing start/end airport.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RouteCheck_ReturnsRouteNotConnectedWithInvalidAirport()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport, Aircraft Model, Price, Time",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 200, 6",
            "FL000, JFK, BOS, Airbus A320, 180, 2.5",
            "FL707, SEA, BOS, Airbus A320, 180, 2.5",
            "FL808, BOS, DCA, Airbus A320, 100, 1.5",
            "FL101, DCA, SFO, Airbus A320, 300, 6.5",
            "FL909, SFO, LAX, Airbus A320, 150, 1.5",
            "FL456, LAX, ORD, Airbus A320, 100, 1",
            "FL010, ORD, ATL, Airbus A320, 150, 2.5",
            "FL111, ATL, MIA, Airbus A320, 200, 3.5",
            "FL202, SFO, MIA, Boeing 747, 400, 7",
            "FL303, BOS, POR, Airbus A320, 160, 2.5",
            "FL404, SEA, ARG, Boeing 747, 380, 6.5",
            "FL192, JFK, ORD, Boeing 747, 450, 8",
            "FL412, ORD, ARG, Airbus A320, 280, 4.5",
            "FL444, DCA, BOS, Airbus A320, 120, 1.8"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route check DFW DFW{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Destination can't be the starting airport DFW", output.ToString());
        Assert.Contains("DFW is not connected to DFW", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RouteCheck_ReturnsRouteNotConnected()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport, Aircraft Model, Price, Time",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 200, 6",
            "FL000, JFK, BOS, Airbus A320, 180, 2.5",
            "FL707, SEA, BOS, Airbus A320, 180, 2.5",
            "FL808, BOS, DCA, Airbus A320, 100, 1.5",
            "FL101, DCA, SFO, Airbus A320, 300, 6.5",
            "FL909, SFO, LAX, Airbus A320, 150, 1.5",
            "FL456, LAX, ORD, Airbus A320, 100, 1",
            "FL010, ORD, ATL, Airbus A320, 150, 2.5",
            "FL111, ATL, MIA, Airbus A320, 200, 3.5",
            "FL202, SFO, MIA, Boeing 747, 400, 7",
            "FL303, BOS, POR, Airbus A320, 160, 2.5",
            "FL404, SEA, ARG, Boeing 747, 380, 6.5",
            "FL192, JFK, ORD, Boeing 747, 450, 8",
            "FL412, ORD, ARG, Airbus A320, 280, 4.5",
            "FL444, DCA, BOS, Airbus A320, 120, 1.8"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route check DFW NotFound{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("DFW is not connected to NotFound", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_RouteCheck_ReturnsRouteConnected()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport, Aircraft Model, Price, Time",
            "FL505, DFW, JFK, Boeing 747, 350, 4",
            "FL606, DFW, SEA, Airbus A320, 200, 6",
            "FL000, JFK, BOS, Airbus A320, 180, 2.5",
            "FL707, SEA, BOS, Airbus A320, 180, 2.5",
            "FL808, BOS, DCA, Airbus A320, 100, 1.5",
            "FL101, DCA, SFO, Airbus A320, 300, 6.5",
            "FL909, SFO, LAX, Airbus A320, 150, 1.5",
            "FL456, LAX, ORD, Airbus A320, 100, 1",
            "FL010, ORD, ATL, Airbus A320, 150, 2.5",
            "FL111, ATL, MIA, Airbus A320, 200, 3.5",
            "FL202, SFO, MIA, Boeing 747, 400, 7",
            "FL303, BOS, POR, Airbus A320, 160, 2.5",
            "FL404, SEA, ARG, Boeing 747, 380, 6.5",
            "FL192, JFK, ORD, Boeing 747, 450, 8",
            "FL412, ORD, ARG, Airbus A320, 280, 4.5",
            "FL444, DCA, BOS, Airbus A320, 120, 1.8"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route check DFW ARG{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("DFW is connected to ARG", output.ToString());
    }
}
