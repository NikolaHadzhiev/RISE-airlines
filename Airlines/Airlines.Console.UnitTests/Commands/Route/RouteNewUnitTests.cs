﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Commands.Route.RouteNewUnitTests;

[Collection("Sequential")]
public class RouteNewUnitTests : IDisposable
{
    private readonly string _testFilePathFlights;

    public RouteNewUnitTests() => _testFilePathFlights = "test_flights.csv";
    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePathFlights))
        {
            File.Delete(_testFilePathFlights);
        }
    }

    [Fact]
    public void ReadUserCommands_RouteNew_ReturnsInitializedRoute()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines = [];

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] flightTestData = [
           "Identifier, DepartureAirport, ArrivalAirport",
            "FL789, ORD, ATL, Boeing 747",
            "FL101, DCA, SFO, Boeing 747",
            "FL202, SFO, MIA, Boeing 747"
       ];

        File.WriteAllLines(_testFilePathFlights, flightTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"route new{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Null(flightRoute.Head);
        Assert.Contains("New route initialized.", output.ToString());
    }
}
