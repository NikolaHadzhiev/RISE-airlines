﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;

namespace Airlines.Console.UnitTests.Commands.Sort.SortUnitTests;

[Collection("Sequential")]
public class SortUnitTests : IDisposable
{
    private readonly string _testFilePathAirports;
    private readonly string _testFilePathAirlines;
    private readonly string _testFilePathFlights;
    public SortUnitTests()
    {
        _testFilePathAirports = "test_airports.csv";
        _testFilePathAirlines = "test_airlines.csv";
        _testFilePathFlights = "test_flights.csv";
    }

    public void Dispose()
    {
        // Clean up test file after tests
        if (File.Exists(_testFilePathAirports))
        {
            File.Delete(_testFilePathAirports);
        }

        if (File.Exists(_testFilePathAirlines))
        {
            File.Delete(_testFilePathAirlines);
        }

        if (File.Exists(_testFilePathFlights))
        {
            File.Delete(_testFilePathFlights);
        }
    }

    [Fact]
    public void ReadUserCommands_Sort_ReturnsInvalidCommand()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports = [];
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights = new FlightLinkedList();
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree = [];
        List<string> flightRouteListForTree = [];


        Dictionary<string, HashSet<AirportCSV>> airportsByCity = [];
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry = [];

        string[] testData = [
           "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
       ];

        File.WriteAllLines(_testFilePathAirlines, testData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);

        StringReader input = new StringReader($"sort{Environment.NewLine}End");
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirlineData(_testFilePathAirlines, out airlines!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        // Assert
        Assert.Contains("Invalid sort command.", output.ToString());
    }

    [Fact]
    public void ReadUserCommands_Sort_ReturnsCorrectOutput()
    {
        // Arrange
        Dictionary<string, AirportCSV> airports;
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree = [];

        Dictionary<string, HashSet<AirportCSV>> airportsByCity;
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry;

        string[] airportTestData = [
           "Identifier, Name, City, Country",
            "JFK, John F. Kennedy International AirportCSV, New York, USA",
            "LAX, Los Angeles International AirportCSV, Los Angeles, USA"
       ];

        string[] airlineTestData = [
           "Identifier, Name",
            "AA, JetX",
            "DL, AirX"
       ];

        string[] flightsTestData = [
           "Identifier, Name",
            "FL456, LAX, ORD, Boeing 747, 350, 4",
            "FL789, ORD, ATL, Boeing 747, 350, 4",
            "FL101, DCA, SFO, Boeing 747, 350, 4"
       ];

        File.WriteAllLines(_testFilePathAirports, airportTestData);
        File.WriteAllLines(_testFilePathAirlines, airlineTestData);
        File.WriteAllLines(_testFilePathFlights, flightsTestData);

        StringWriter output = new StringWriter();
        System.Console.SetOut(output);


        string command = $"sort airports ascending" +
                                                $"{Environment.NewLine}sort airports descending" +
                                                $"{Environment.NewLine}sort airlines ascending" +
                                                $"{Environment.NewLine}sort airlines descending" +
                                                $"{Environment.NewLine}sort flights ascending" +
                                                $"{Environment.NewLine}sort flights descending" +
                                                $"{Environment.NewLine}End";

        StringReader input = new StringReader(command);
        System.Console.SetIn(input);

        //Act
        ReadUtils.ReadAirportData(_testFilePathAirports, out airports!, out airportsByCity!, out airportsByCountry!);
        ReadUtils.ReadAirlineData(_testFilePathAirlines, out airlines!);
        ReadUtils.ReadFlightData(_testFilePathFlights, out flights!, null, out flightsForTree!);

        ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);

        string ascAirports = $"{Environment.NewLine}- JFK-John F. Kennedy International AirportCSV-New York-USA" +
                             $"{Environment.NewLine}- LAX-Los Angeles International AirportCSV-Los Angeles-USA";

        string descAirports = $"{Environment.NewLine}- LAX-Los Angeles International AirportCSV-Los Angeles-USA" +
                              $"{Environment.NewLine}- JFK-John F. Kennedy International AirportCSV-New York-USA";

        string ascAirlines = $"{Environment.NewLine}- DL-AirX" +
                             $"{Environment.NewLine}- AA-JetX";

        string descAirlines = $"{Environment.NewLine}- AA-JetX" +
                              $"{Environment.NewLine}- DL-AirX";

        string ascFlights = $"{Environment.NewLine}FlightCSV FL101: DCA to SFO" +
                            $"{Environment.NewLine}FlightCSV FL456: LAX to ORD" +
                            $"{Environment.NewLine}FlightCSV FL789: ORD to ATL";

        string descFlights = $"{Environment.NewLine}FlightCSV FL789: ORD to ATL" +
                             $"{Environment.NewLine}FlightCSV FL456: LAX to ORD" +
                             $"{Environment.NewLine}FlightCSV FL101: DCA to SFO";

        // Assert
        Assert.Contains(ascAirports, output.ToString());
        Assert.Contains(descAirports, output.ToString());
        Assert.Contains(ascAirlines, output.ToString());
        Assert.Contains(descAirlines, output.ToString());
        Assert.Contains(ascFlights, output.ToString());
        Assert.Contains(descFlights, output.ToString());
    }
}
