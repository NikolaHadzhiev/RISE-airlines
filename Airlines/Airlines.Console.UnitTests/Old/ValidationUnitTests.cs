﻿//using Airlines.Persistence.Basic.Entities;
//using Airlines.Business.Extensions;

//namespace Airlines.Console.UnitTests.Old;

//[Collection("Sequential")]
//public class ValidationUnitTests
//{
//    [Theory]
//    [InlineData("AAA", new string[] { "BBB", "CCC" }, true)] // Valid airport
//    [InlineData("AA", new string[] { "BBB", "CCC" }, false)] // Invalid airport (not 3 characters)
//    [InlineData("AAA", new string[] { "BBB", "AAA" }, false)] // Invalid airport (not unique)
//    [InlineData("", new string[] { "BBB", "CCC" }, false)] // Invalid airport (empty string)
//    [InlineData("123", new string[] { "BBB", "CCC" }, false)] // Invalid airport (numeric characters)
//    [InlineData("AB1", new string[] { "BBB", "CCC" }, false)] // Invalid airport (alphanumeric characters)
//    [InlineData("ABC", new string[] { }, true)] // Valid airport (no existing airports)
//    [InlineData("ABC", new string[] { "ABC" }, false)] // Invalid airport (existing airport)
//    [InlineData("abc", new string[] { "BBB", "CCC" }, true)] // Valid airport (case-insensitive)
//    [InlineData("DEF", new string[] { "DEF" }, false)] // Invalid airport (existing airport, case-insensitive)
//    [InlineData("PQR", new string[] { "pqr" }, true)] // Valid airport (existing airport, case-insensitive)
//    public void IsAirportValid_WithDifferentData_ReturnsExpectedResult(string name, string[] existingAirports, bool expectedResult)
//    {
//        // Arrange
//
//
//        airport = new AirportCSV(name);

//        // Act
//        var result = airport.Name.IsAirportValid() && existingAirports.IsUnique(airport.Name);

//        // Assert
//        Assert.Equal(expectedResult, result);
//    }

//    [Theory]
//    [InlineData("Emi", new string[] { "Lufthansa", "Delta" }, true)] // Valid airline
//    [InlineData("Qatar Airways", new string[] { "Lufthansa", "Delta" }, false)] // Invalid airline (more than 6 characters)
//    [InlineData("", new string[] { "Lufthansa", "Delta" }, false)] // Invalid airline (empty string)
//    [InlineData("AA1", new string[] { "Lufthansa", "Delta" }, true)] // Valid airline (alphanumeric characters)
//    [InlineData("A", new string[] { }, true)] // Valid airline (no existing airlines)
//    [InlineData("A", new string[] { "A" }, false)] // Invalid airline (existing airline)
//    [InlineData("lufthansa", new string[] { "Lufthansa" }, false)] // Invalid airline (existing airline, case-insensitive)
//    [InlineData("Delta", new string[] { "DELTA" }, true)] // Valid airline (existing airline, case-insensitive)
//    [InlineData("EMIRATES", new string[] { "Emirates" }, false)] // Invalid airline (more than 6 characters)
//    [InlineData("United", new string[] { "united" }, false)] // Invalid airline (more than 6 characters)
//    [InlineData("BB1", new string[] { "bb1" }, true)] //Valid airline (existing airline, case-insensitive)
//    public void IsAirlineValid_WithDifferentData_ReturnsExpectedResult(string name, string[] existingAirlines, bool expectedResult)
//    {
//        // Arrange
//        var airline = new Airline(name);

//        // Act
//        var result = airline.Name.IsAirlineValid() && existingAirlines.IsUnique(airline.Name);

//        // Assert
//        Assert.Equal(expectedResult, result);
//    }

//    [Theory]
//    [InlineData("EK123", new string[] { "LH456", "DL789" }, true)] // Valid flight
//    [InlineData("ABC@", new string[] { "LH456", "DL789" }, false)] // Invalid flight (contains special characters)
//    [InlineData(" ", new string[] { }, false)] // Invalid flight (empty string)
//    [InlineData("123", new string[] { "LH456", "DL789" }, true)] // Valid flight (numeric characters)
//    [InlineData("ABC", new string[] { }, true)] // Valid flight (no existing flights)
//    [InlineData("ABC", new string[] { "ABC" }, false)] // Invalid flight (existing flight)
//    [InlineData("lH123", new string[] { "LH123" }, true)] // Valid flight (existing flight, case-insensitive)
//    [InlineData("DL456", new string[] { "DL456" }, false)] // Invalid flight (existing flight, case-insensitive)
//    [InlineData("EK123", new string[] { "ek123" }, true)] // Valid flight (existing flight, case-insensitive)
//    [InlineData("xy123", new string[] { "XY123" }, true)] // Valid flight (existing flight, case-insensitive)
//    public void IsFlightValid_WithDifferentData_ReturnsExpectedResult(string code, string[] existingFlights, bool expectedResult)
//    {
//        // Arrange
//        var flight = new FlightCSV(code);

//        // Act
//        var result = flight.Code.IsFlightValid() && existingFlights.IsUnique(flight.Code);

//        // Assert
//        Assert.Equal(expectedResult, result);
//    }

//    [Fact]
//    public void IsUnique_WhenItemIsUnique_ReturnsTrue()
//    {
//        // Arrange
//        int[] array = [1, 2, 3, 4, 5];
//        var item = 6;

//        // Act
//        var result = array.IsUnique(item);

//        // Assert
//        Assert.True(result);
//    }

//    [Fact]
//    public void IsUnique_WhenItemIsNotUnique_ReturnsFalse()
//    {
//        // Arrange
//        int[] array = [1, 2, 3, 4, 5];
//        var item = 3;

//        // Act
//        var result = array.IsUnique(item);

//        // Assert
//        Assert.False(result);
//    }

//    [Fact]
//    public void IsUnique_WhenItemIsNotUnique_WritesErrorMessage()
//    {
//        // Arrange
//        int[] array = [1, 2, 3, 4, 5];
//        var item = 3;
//        var output = new StringWriter();
//        System.Console.SetOut(output);
//        var expectedOutput = true;
//        // Act
//        _ = array.IsUnique(item);
//        var actualOutput = output.ToString().IndexOf($"{item} already exists in the list." + Environment.NewLine) > -1;

//        // Assert
//        Assert.Equal(expectedOutput, actualOutput);
//    }

//    [Fact]
//    public void IsUnique_WhenArrayIsEmpty_ReturnsTrue()
//    {
//        // Arrange
//        int[] array = [];
//        var item = 1;

//        // Act
//        var result = array.IsUnique(item);

//        // Assert
//        Assert.True(result);
//    }
//}
