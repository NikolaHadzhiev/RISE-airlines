﻿//using Airlines.Persistence.Basic.Entities;
//using Airlines.Business.Extensions;

//namespace Airlines.Console.UnitTests.DataUnitTests;

//[Collection("Sequential")]
//public class DataUnitTests
//{
//    [Theory]
//    [InlineData("Air")]
//    [InlineData("Delta")]
//    [InlineData("Unite")]
//    public void Airline_ValidName_ReturnsName(string name)
//    {
//        // Arrange & Act
//        Airline airline = new Airline(name);

//        // Assert
//        Assert.Equal(name, airline.Name);
//    }

//    [Theory]
//    [InlineData("")]
//    [InlineData("  ")]
//    [InlineData("InvalidAirlineNameWithMoreThanSixCharacters")]
//    public void Airline_InvalidName_ReturnsFalse(string name)
//    {
//        // Arrange & Act
//        Airline airline = new Airline(name);

//        // Assert
//        Assert.False(airline.Name.IsAirlineValid());
//    }

//    [Theory]
//    [InlineData("AirlineNameWithMoreThanSixCharacters")]
//    [InlineData("   Airline")]
//    public void Airline_InvalidName_WritesErrorMessage(string name)
//    {
//        // Arrange & Act
//
//        output = new StringWriter();
//        System.Console.SetOut(output);
//        string expectedErrorMessage = $"Airline '{name}' is invalid." + Environment.NewLine + $"Airline name must have less than 6 characters and must be unique.{Environment.NewLine}";

//        Airline airline = new Airline(name);
//        _ = airline.Name.IsAirlineValid();

//        // Assert
//        Assert.Equal(expectedErrorMessage, output.ToString());
//    }

//    [Theory]
//    [InlineData("JFK")]
//    [InlineData("LAX")]
//    [InlineData("ORD")]
//    public void Airport_ValidName_ReturnsName(string name)
//    {
//        // Arrange & Act
//        AirportCSV airport = new AirportCSV(name);

//        // Assert
//        Assert.Equal(name, airport.Name);
//    }

//    [Theory]
//    [InlineData("")]
//    [InlineData("  ")]
//    [InlineData("ABCD")]
//    [InlineData("12A")]
//    public void Airport_InvalidName_ReturnsFalse(string name)
//    {
//        // Arrange & Act
//        AirportCSV airport = new AirportCSV(name);

//        // Assert
//        Assert.False(airport.Name.IsAirportValid());
//    }

//    [Theory]
//    [InlineData("ABCD")]
//    [InlineData("1A")]
//    [InlineData("CD")]
//    public void Airport_InvalidName_WritesErrorMessage(string name)
//    {
//        // Arrange & Act
//        StringWriter output = new StringWriter();
//        System.Console.SetOut(output);
//        string expectedErrorMessage = $"AirportCSV '{name}' is invalid." + Environment.NewLine + $"AirportCSV name must have exactly three alphabetic characters and must be unique.{Environment.NewLine}";

//        AirportCSV airport = new AirportCSV(name);
//        bool airportIsValid = airport.Name.IsAirportValid();

//        // Assert
//        Assert.Equal(expectedErrorMessage, output.ToString());
//        Assert.False(airportIsValid);
//    }

//    [Theory]
//    [InlineData("ABC123")]
//    [InlineData("FLIGHT001")]
//    [InlineData("XYZ789")]
//    public void Flight_ValidCode_ReturnsCode(string code)
//    {
//        // Arrange & Act
//        FlightCSV flight = new FlightCSV(code);

//        // Assert
//        Assert.Equal(code, flight.Code);
//    }

//    [Theory]
//    [InlineData("")]
//    [InlineData("  ")]
//    [InlineData("!@#$%")]
//    [InlineData("FlightCSV with space")]
//    public void Flight_InvalidCode_ReturnsFalse(string code)
//    {
//        // Arrange & Act
//        FlightCSV flight = new FlightCSV(code);

//        // Assert
//        Assert.False(flight.Code.IsFlightValid());
//    }

//    [Theory]
//    [InlineData("!@#$%")]
//    [InlineData("FlightCSV with space")]
//    public void Flight_InvalidCode_WritesErrorMessage(string code)
//    {
//        // Arrange & Act
//        StringWriter output = new StringWriter();
//        System.Console.SetOut(output);
//        string expectedErrorMessage = $"FlightCSV '{code}' is invalid." + Environment.NewLine + $"FlightCSV identifier must contain only alphanumeric characters and must be unique.{Environment.NewLine}";

//        FlightCSV flight = new FlightCSV(code);
//        _ = flight.Code.IsFlightValid();

//        // Assert
//        Assert.Equal(expectedErrorMessage, output.ToString());
//    }
//}
