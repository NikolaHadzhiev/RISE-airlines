﻿//using AirlinesConsole.Utils;

//namespace Airlines.Console.UnitTests.HelperUnitTests;

//[Collection("Sequential")]
//public class HelperUnitTests
//{
//    [Fact]
//    public void PrintData_EmptyData_PrintsCorrectly()
//    {
//        // Arrange
//        string dataType = "Test Type";
//        string[] data = [];

//        StringWriter sw = new StringWriter();
//        System.Console.SetOut(sw);

//        bool expectedOutput = true;

//        // Act
//        FileDataUtils.PrintDictionaryData(dataType, data);
//        bool actualOutput = sw.ToString().IndexOf("Test Type count: 0") > -1;

//        // Assert
//        Assert.Equal(expectedOutput, actualOutput);
//    }

//    [Fact]
//    public void PrintData_PrintsCorrectly()
//    {
//        // Arrange
//        string dataType = "Test Type";
//        string[] data = ["Data1", "Data2", "Data3"];
//        StringWriter sw = new StringWriter();
//        System.Console.SetOut(sw);
//        bool expectedResult = true;

//        // Act
//        FileDataUtils.PrintDictionaryData(dataType, data);
//        bool actualResult = sw.ToString().IndexOf($"{dataType} count: {data.Length}") > -1;

//        // Assert
//        Assert.Equal(expectedResult, actualResult);
//    }

//    [Fact]
//    public void PrintData_NullData_ThrowsArgumentNullException()
//    {
//        // Arrange
//        string dataType = "Test Type";
//        string[]? data = null;

//        // Act & Assert
//        _ = Assert.Throws<NullReferenceException>(() => FileDataUtils.PrintDictionaryData(dataType, data));
//    }
//}
