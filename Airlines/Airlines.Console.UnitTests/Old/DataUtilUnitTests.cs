﻿//using AirlinesConsole.Utils;

//namespace Airlines.Console.UnitTests.DataUtilTests;

//[Collection("Sequential")]
//public class DataUtilTests
//{
//    [Fact]
//    public void SortAndDisplayData_SortsAirportsInDescendingOrder()
//    {
//        // Arrange
//        StringReader input = new StringReader("sort Airports descending\nEndSort\n");
//        System.Console.SetIn(input);
//        StringWriter output = new StringWriter();
//        System.Console.SetOut(output);

//        string[] airportsToSort = ["JFK", "LAX", "DFW", "ORD", "ATL"];
//        string[] airlinesToSort = ["United", "Delta", "American", "Southwest", "JetBlue"];
//        string[] flightsToSort = ["UA789", "DL123", "AA456", "SW101", "JB202"];
//        string expectedOutput = $"{Environment.NewLine}- ORD{Environment.NewLine}- LAX{Environment.NewLine}" +
//                                $"- JFK{Environment.NewLine}- DFW{Environment.NewLine}- ATL";

//        bool expectedResult = true;

//        // Act
//        FileDataUtils.SortAndDisplayData(airportsToSort, airlinesToSort, flightsToSort);
//        bool actualResult = output.ToString().IndexOf(expectedOutput) > -1;

//        // Assert
//        Assert.Equal(expectedResult, actualResult);
//    }

//    [Fact]
//    public void SearchData_FindsSearchTermInAirlines()
//    {
//        // Arrange
//
//        input = new StringReader("search Unit\nEnd Search\n");
//        System.Console.SetIn(input);
//        var output = new StringWriter();
//        System.Console.SetOut(output);

//        string[] airportsNames = ["JFK", "LAX", "DFW", "ORD", "ATL"];
//        string[] airlinesNames = ["Unit", "Delta", "Ame", "South", "Jet"];
//        string[] flightsNames = ["UA789", "DL123", "AA456", "SW101", "JB202"];

//        string expectedOutput = $"{Environment.NewLine}Found 'Unit' in Airlines.{Environment.NewLine}{Environment.NewLine}";
//        bool expectedResult = true;
//        // Act
//        Array.Sort(airportsNames);
//        Array.Sort(airlinesNames);
//        Array.Sort(flightsNames);

//        FileDataUtils.SearchData(airportsNames, airlinesNames, flightsNames);
//        bool actualResult = output.ToString().IndexOf(expectedOutput) > -1;

//        // Assert
//        Assert.Equal(expectedResult, actualResult);
//    }
//}
