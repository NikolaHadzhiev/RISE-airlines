﻿using Airlines.Business.Services;
using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;
using Airlines.Persistence.Basic.Repositories.AirportRepository;
using Moq;

namespace Airlines.Console.UnitTests.Services;

#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable IDE0058 // Expression value is never used

[Collection("Sequential")]
public class AirportServiceTests
{
    private readonly Mock<IAirportRepositoryDB> _mockRepository;

    public AirportServiceTests()
    {
        // Initialize mock repository and service for each test
        _mockRepository = new Mock<IAirportRepositoryDB>();
    }

    [Fact]
    public async Task GetAllAirportsFromDB_Success()
    {
        // Arrange
        List<Airport> airports =
        [
            new() { ID = 1, Name = "Airport 1" },
            new() { ID = 2, Name = "Airport 2" }
        ];

        _mockRepository.Setup(r => r.GetAllAirportsAsync()).ReturnsAsync(airports);

        AirportService airportService = new AirportService(_mockRepository.Object);

        // Act
        var result = await airportService.GetAllAirportsFromDB();

        // Assert
        Assert.Equal(airports, result);
    }

    [Fact]
    public async Task GetAirportFromDB_Success()
    {
        // Arrange
        int id = 2;
        Airport airport = new() { ID = 2, Name = "Airport 2" };

        _mockRepository.Setup(r => r.GetAirportByIdAsync(id)).ReturnsAsync(airport);

        AirportService airportService = new AirportService(_mockRepository.Object);

        // Act
        var result = await airportService.GetAirportByIdFromDB(id);

        // Assert
        Assert.Equal(airport, result);
    }

    [Fact]
    public async Task AddAirportToDB_ShouldAddAirport_WhenRepositoryReturnsTrue()
    {
        // Arrange
        var airportDTO = new AirportDTO("Test Airport", "USA", "Dallas", "DFW", 5, DateOnly.FromDateTime(DateTime.Now));
        var expectedAirport = new Airport { Name = "Test Airport" };

        _mockRepository.Setup(repo => repo.AddAirportAsync(It.IsAny<Airport>()))
                             .ReturnsAsync(true);

        var service = new AirportService(_mockRepository.Object); // Assuming the method is in AirportService

        // Act
        var result = await service.AddAirportToDB(airportDTO);

        // Assert
        Assert.Equal(expectedAirport.Name, result!.Name);
        _mockRepository.Verify(repo => repo.AddAirportAsync(It.IsAny<Airport>()), Times.Once());
    }
}
