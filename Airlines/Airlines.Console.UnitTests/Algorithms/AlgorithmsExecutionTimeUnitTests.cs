﻿using Airlines.Business.Extensions;
using System.Diagnostics;
using Xunit.Abstractions;

namespace Airlines.Console.UnitTests.Algorithms.AlgorithmsExecutionTimeUnitTests;

[Collection("Sequential")]
public class AlgorithmsExecutionTimeUnitTests(ITestOutputHelper testOutputHelper)
{
    private static readonly Random _random = new();
    private static readonly int _dataSize = _random.Next(1, 1000);
    private static readonly string[] _randomAirports = GenerateRandomAirports(_dataSize); // used only in BubbleSort_PerformanceTest_SortsRandomDataWithinOneSecond
    private static readonly string[] _randomAirlines = GenerateRandomAirlines(_dataSize); // used only in SelectionSort_PerformanceTest_SortsRandomDataWithinOneSecond
    private static readonly string[] _randomFlights = GenerateRandomFlights(_dataSize); //used only in BinarySearch_PerformanceTest_SearchesRandomDataWithinOneSecond

    private readonly ITestOutputHelper _testOutputHelper = testOutputHelper;

    [Fact]
    public void BubbleSort_PerformanceTest_SortsRandomDataWithinOneSecond()
    {
        // Arrange
        int iterations = 0;

        // Act
        Stopwatch stopwatch = Stopwatch.StartNew();

        while (stopwatch.ElapsedMilliseconds <= 1000)
        {
            string[] newRandomAirports = (string[])_randomAirports.Clone();
            newRandomAirports.BubbleSort();

            iterations++;
        }

        stopwatch.Stop();

        // Assert
        Assert.True(true);
        _testOutputHelper.WriteLine($"Bubble Sort Iterations: {iterations}");
        _testOutputHelper.WriteLine($"Data count: {_dataSize}");
    }


    [Fact]
    public void SelectionSort_PerformanceTest_SortsRandomDataWithinOneSecond()
    {
        // Arrange
        int iterations = 0;

        // Act
        Stopwatch stopwatch = Stopwatch.StartNew();

        while (stopwatch.ElapsedMilliseconds <= 1000)
        {
            string[] newRandomAirlines = (string[])_randomAirlines.Clone();

            newRandomAirlines.SelectionSort();

            iterations++;
        };

        stopwatch.Stop();

        // Assert
        Assert.True(true);

        _testOutputHelper.WriteLine($"Selection Sort Iterations: {iterations}");
        _testOutputHelper.WriteLine($"Data count: {_dataSize}");
    }


    [Fact]
    public void BinarySearch_PerformanceTest_SearchesRandomDataWithinOneSecond()
    {
        // Arrange
        Array.Sort(_randomFlights); // Ensure data is sorted for binary search

        int iterations = 0;

        // Act
        Stopwatch stopwatch = Stopwatch.StartNew();

        while (stopwatch.ElapsedMilliseconds <= 1000)
        {
            string[] newRandomFlights = (string[])_randomFlights.Clone();
            _ = newRandomFlights.BinarySearch(newRandomFlights[_random.Next(newRandomFlights.Length)], 0, newRandomFlights.Length - 1);

            iterations++;
        }

        stopwatch.Stop();

        // Assert
        Assert.True(true);
        _testOutputHelper.WriteLine($"Binary Search Iterations: {iterations}");
        _testOutputHelper.WriteLine($"Data count: {_dataSize}");
    }


    [Fact]
    public void LinearSearch_PerformanceTest_SearchesRandomDataWithinOneSecond()
    {
        // Arrange
        string[] randomAirports = GenerateRandomAirports(_dataSize);
        int iterations = 0;

        // Act
        Stopwatch stopwatch = Stopwatch.StartNew();

        while (stopwatch.ElapsedMilliseconds <= 1000)
        {
            string[] newRandomAirports = (string[])randomAirports.Clone();
            _ = newRandomAirports.LinearSearch(newRandomAirports[_random.Next(newRandomAirports.Length)]);

            iterations++;
        }

        stopwatch.Stop();

        // Assert
        Assert.True(true);
        _testOutputHelper.WriteLine($"Linear Search Iterations: {iterations}");
        _testOutputHelper.WriteLine($"Data count: {_dataSize}");
    }


    [Fact]
    public void BubbleSort_vs_SelectionSort_PerformanceTest_SortsRandomDataWithinOneSecond()
    {
        // Arrange
        string[] randomAirports = GenerateRandomAirports(_dataSize);

        int bubbleSortIterations = 0;
        int selectionSortIterations = 0;

        // Act - Bubble Sort
        Stopwatch bubbleSortStopwatch = Stopwatch.StartNew();

        while (bubbleSortStopwatch.ElapsedMilliseconds <= 1000)
        {
            string[] newRandomAirportsBubble = (string[])randomAirports.Clone();

            newRandomAirportsBubble.BubbleSort();

            bubbleSortIterations++;
        }

        bubbleSortStopwatch.Stop();

        // Act - Selection Sort
        Stopwatch selectionSortStopwatch = Stopwatch.StartNew();

        while (selectionSortStopwatch.ElapsedMilliseconds <= 1000)
        {
            string[] newRandomAirportsSelection = (string[])randomAirports.Clone();

            newRandomAirportsSelection.SelectionSort();

            selectionSortIterations++;
        }

        selectionSortStopwatch.Stop();

        // Assert
        Assert.True(true);

        _testOutputHelper.WriteLine($"Bubble Sort Iterations: {bubbleSortIterations}");
        _testOutputHelper.WriteLine($"Selection Sort Iterations: {selectionSortIterations}");
        _testOutputHelper.WriteLine($"Data count: {_dataSize}");
    }


    [Fact]
    public void LinearSearch_vs_BinarySearch_PerformanceTest_SearchesRandomDataWithinOneSecond()
    {
        // Arrange
        string[] randomAirports = GenerateRandomAirports(_dataSize);
        string[] randomAirportsSorted = (string[])randomAirports.Clone();
        Array.Sort(randomAirportsSorted);

        int randomAirportIndex = _random.Next(randomAirports.Length);
        int linearSearchIterations = 0;
        int binarySearchIterations = 0;

        // Act - Linear Search
        Stopwatch linearSearchStopwatch = Stopwatch.StartNew();

        while (linearSearchStopwatch.ElapsedMilliseconds <= 1000)
        {
            string[] newRandomAirportsLinear = (string[])randomAirports.Clone();

            _ = newRandomAirportsLinear.LinearSearch(newRandomAirportsLinear[randomAirportIndex]);

            linearSearchIterations++;
        }

        linearSearchStopwatch.Stop();

        // Act - Binary Search
        Stopwatch binarySearchStopwatch = Stopwatch.StartNew();

        while (binarySearchStopwatch.ElapsedMilliseconds <= 1000)
        {
            string[] newRandomAirportsBinary = (string[])randomAirportsSorted.Clone();

            _ = newRandomAirportsBinary.BinarySearch(newRandomAirportsBinary[randomAirportIndex], 0, newRandomAirportsBinary.Length - 1);

            binarySearchIterations++;
        }

        binarySearchStopwatch.Stop();

        // Assert
        Assert.True(true);
        _testOutputHelper.WriteLine($"Linear Search Iterations: {linearSearchIterations}");
        _testOutputHelper.WriteLine($"Binary Search Iterations: {binarySearchIterations}");
        _testOutputHelper.WriteLine($"Target element: {randomAirports[randomAirportIndex]}");
        _testOutputHelper.WriteLine($"Data count: {_dataSize}");
    }


    [Fact]
    public void Algorithms_PerformanceTest_WithEmptyArray()
    {
        // Arrange
        int iterations = 0;
        string[] emptyArray = [];

        // Act - Bubble Sort
        Stopwatch bubbleSortStopwatch = Stopwatch.StartNew();

        while (bubbleSortStopwatch.ElapsedMilliseconds <= 1000)
        {
            string[] newArray = (string[])emptyArray.Clone();

            newArray.BubbleSort();

            iterations++;
        }

        bubbleSortStopwatch.Stop();

        _testOutputHelper.WriteLine($"Bubble Sort Iterations with empty array: {iterations}");

        // Reset iterations counter
        iterations = 0;

        // Act - Selection Sort
        Stopwatch selectionSortStopwatch = Stopwatch.StartNew();

        while (selectionSortStopwatch.ElapsedMilliseconds <= 1000)
        {
            string[] newArray = (string[])emptyArray.Clone();

            newArray.SelectionSort();

            iterations++;
        }

        selectionSortStopwatch.Stop();

        _testOutputHelper.WriteLine($"Selection Sort Iterations with empty array: {iterations}");

        // Reset iterations counter
        iterations = 0;

        // Act - Linear Search
        Stopwatch linearSearchStopwatch = Stopwatch.StartNew();

        while (linearSearchStopwatch.ElapsedMilliseconds <= 1000)
        {
            string[] newArray = (string[])emptyArray.Clone();

            _ = newArray.LinearSearch("random"); // Search any random value

            iterations++;
        }

        linearSearchStopwatch.Stop();

        _testOutputHelper.WriteLine($"Linear Search Iterations with empty array: {iterations}");

        // Reset iterations counter
        iterations = 0;

        // Act - Binary Search
        Stopwatch binarySearchStopwatch = Stopwatch.StartNew();

        while (binarySearchStopwatch.ElapsedMilliseconds <= 1000)
        {
            string[] newArray = (string[])emptyArray.Clone();

            _ = newArray.BinarySearch("random", 0, newArray.Length - 1); // Search any random value

            iterations++;
        }

        binarySearchStopwatch.Stop();

        _testOutputHelper.WriteLine($"Binary Search Iterations with empty array: {iterations}");
    }


    private static string[] GenerateRandomAirports(int count)
    {
        string[] randomAirports = new string[count];
        for (int i = 0; i < count; i++)
            randomAirports[i] = GenerateRandomAirport();
        return randomAirports;
    }

    private static string GenerateRandomAirport()
        => $"{(char)('A' + _random.Next(26))}{(char)('A' + _random.Next(26))}{(char)('A' + _random.Next(26))}";

    private static string[] GenerateRandomAirlines(int count)
    {
        string[] randomAirlines = new string[count];
        for (int i = 0; i < count; i++)
            randomAirlines[i] = GenerateRandomAirline();
        return randomAirlines;
    }

    private static string GenerateRandomAirline()
        => $"{(char)('A' + _random.Next(26))}{(char)('a' + _random.Next(26))}" +
           $"{(char)('a' + _random.Next(26))}{(char)('a' + _random.Next(26))}";

    private static string[] GenerateRandomFlights(int count)
    {
        string[] randomFlights = new string[count];
        for (int i = 0; i < count; i++)
            randomFlights[i] = GenerateRandomFlight();
        return randomFlights;
    }

    private static string GenerateRandomFlight()
        => $"{(char)('A' + _random.Next(26))}" +
           $"{(char)('A' + _random.Next(26))}{_random.Next(1000, 10000)}";
}
