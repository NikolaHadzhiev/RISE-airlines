using Airlines.Business.Extensions;

namespace Airlines.Console.UnitTests.Algorithms.AlgorithmsUnitTests;

[Collection("Sequential")]
public class AlgorithmsUnitTests
{
    private readonly string[] _airports = ["JFK", "LAX", "ORD", "ATL", "DFW"];
    private readonly string[] _airlines = ["Delta", "American", "United", "Southwest", "JetBlue"];
    private readonly string[] _flights = ["DL123", "AA456", "UA789", "SW101", "JB202"];

    [Theory]
    [InlineData(new string[] { "LAX", "JFK", "DFW", "ORD", "ATL" },
                new string[] { "ATL", "DFW", "JFK", "LAX", "ORD" }
                )] // Unsorted airports

    [InlineData(new string[] { "ORD", "JFK", "ATL", "LAX", "DFW" },
                new string[] { "ATL", "DFW", "JFK", "LAX", "ORD" }
                )] // Another unsorted airports

    [InlineData(new string[] { "LAX", "ORD", "JFK", "ATL", "DFW" },
                new string[] { "ATL", "DFW", "JFK", "LAX", "ORD" }
                )] // Yet another unsorted airports
    public void BubbleSort_UnsortedAirports_SortsAirportsInAscendingOrder(string[] unsortedAirports, string[] expectedSortedAirports)
    {
        // Act
        unsortedAirports.BubbleSort();

        // Assert
        Assert.Equal(expectedSortedAirports, unsortedAirports);
    }

    [Theory]
    [InlineData(
                new string[] { "United", "Delta", "American", "Southwest", "JetBlue" },
                new string[] { "American", "Delta", "JetBlue", "Southwest", "United" }
                )] // Unsorted airlines

    [InlineData(new string[] { "Delta", "United", "American", "Southwest", "JetBlue" },
                new string[] { "American", "Delta", "JetBlue", "Southwest", "United" }
                )] // Another unsorted airlines

    [InlineData(
                new string[] { "JetBlue", "Southwest", "American", "United", "Delta" },
                new string[] { "American", "Delta", "JetBlue", "Southwest", "United" }
                )] // Yet another unsorted airlines
    public void SelectionSort_UnsortedAirlines_SortsAirlinesInAscendingOrder(string[] unsortedAirlines, string[] expectedSortedAirlines)
    {
        // Act
        unsortedAirlines.SelectionSort();

        // Assert
        Assert.Equal(expectedSortedAirlines, unsortedAirlines);
    }

    [Theory]
    [InlineData(new string[] { "JFK", "LAX", "DFW", "ORD", "ATL" },
                    new string[] { "ORD", "LAX", "JFK", "DFW", "ATL" }
                    )] // Unsorted airports
    public void BubbleSort_UnsortedAirports_SortsAirportsInDescendingOrder(string[] unsortedAirports, string[] expectedSortedAirports)
    {
        // Act
        unsortedAirports.BubbleSort(ascending: false);

        // Assert
        Assert.Equal(expectedSortedAirports, unsortedAirports);
    }

    [Theory]
    [InlineData(new string[] { "United", "Delta", "American", "Southwest", "JetBlue" },
                new string[] { "United", "Southwest", "JetBlue", "Delta", "American" }
                )] // Unsorted airlines
    public void SelectionSort_UnsortedAirlines_SortsAirlinesInDescendingOrder(string[] unsortedAirlines, string[] expectedSortedAirlines)
    {
        // Act
        unsortedAirlines.SelectionSort(ascending: false);

        // Assert
        Assert.Equal(expectedSortedAirlines, unsortedAirlines);
    }

    [Theory]
    [InlineData("ATL", 0)] // Existing airport
    [InlineData("LAS", -1)] // Non-existing airport
    public void BinarySearch_Airports_ExistingOrNonExisting_ReturnsCorrectIndex(string target, int expectedIndex)
    {
        //Arrange
        Array.Sort(_airports);

        // Act
        int resultIndex = _airports.BinarySearch(target, 0, _airports.Length - 1);

        // Assert
        Assert.Equal(expectedIndex, resultIndex);
    }

    [Theory]
    [InlineData("American", 0)] // Existing airline
    [InlineData("Emirates", -1)] // Non-existing airline
    public void BinarySearch_Airlines_ExistingOrNonExisting_ReturnsCorrectIndex(string target, int expectedIndex)
    {
        //Arrange
        Array.Sort(_airlines);

        // Act
        int resultIndex = _airlines.BinarySearch(target, 0, _airlines.Length - 1);

        // Assert
        Assert.Equal(expectedIndex, resultIndex);
    }

    [Theory]
    [InlineData("UA789", 2)] // Existing flight
    [InlineData("EK123", -1)] // Non-existing flight
    public void BinarySearch_Flights_ExistingOrNonExisting_ReturnsCorrectIndex(string target, int expectedIndex)
    {
        // Act
        int resultIndex = _flights.BinarySearch(target, 0, _flights.Length - 1);

        // Assert
        Assert.Equal(expectedIndex, resultIndex);
    }

    [Theory]
    [InlineData("ATL", 3)] // Existing airport
    [InlineData("LAS", -1)] // Non-existing airport
    public void LinearSearch_Airports_ExistingOrNonExisting_ReturnsCorrectIndex(string target, int expectedIndex)
    {
        // Act
        int resultIndex = _airports.LinearSearch(target);

        // Assert
        Assert.Equal(expectedIndex, resultIndex);
    }

    [Theory]
    [InlineData("Emirates", -1)] // Non-existing airline
    [InlineData("Delta", 0)] // First airline
    [InlineData("JetBlue", 4)] // Last airline
    public void LinearSearch_ExistingOrNonExistingAirline_ReturnsCorrectIndex(string targetAirline, int expectedIndex)
    {
        // Act
        int resultIndex = _airlines.LinearSearch(targetAirline);

        // Assert
        Assert.Equal(expectedIndex, resultIndex);
    }

    [Theory]
    [InlineData("UA789", 2)] // Existing flight
    [InlineData("EK123", -1)] // Non-existing flight
    public void LinearSearch_Flights_ExistingOrNonExisting_ReturnsCorrectIndex(string target, int expectedIndex)
    {
        // Act
        int resultIndex = _flights.LinearSearch(target);

        // Assert
        Assert.Equal(expectedIndex, resultIndex);
    }

    [Fact]
    public void BubbleSort_EmptyArray_ReturnsEmptyArray()
    {
        // Arrange
        string[] emptyArray = [];

        // Act
        emptyArray.BubbleSort();

        // Assert
        Assert.Empty(emptyArray);
    }

    [Fact]
    public void SelectionSort_EmptyArray_ReturnsEmptyArray()
    {
        // Arrange
        string[] emptyArray = [];

        // Act
        emptyArray.SelectionSort();

        // Assert
        Assert.Empty(emptyArray);
    }

    [Fact]
    public void BubbleSort_ArrayWithDuplicates_SortsArrayCorrectly()
    {
        // Arrange
        string[] array = ["a", "c", "b", "c", "a"];
        string[] expectedSortedArray = ["a", "a", "b", "c", "c"];

        // Act
        array.BubbleSort();

        // Assert
        Assert.Equal(expectedSortedArray, array);
    }

    [Fact]
    public void SelectionSort_ArrayWithDuplicates_SortsArrayCorrectly()
    {
        // Arrange
        string[] array = ["a", "c", "b", "c", "a"];
        string[] expectedSortedArray = ["a", "a", "b", "c", "c"];

        // Act
        array.SelectionSort();

        // Assert
        Assert.Equal(expectedSortedArray, array);
    }

    [Fact]
    public void BubbleSort_SortedArray_ReturnsSameArray()
    {
        // Arrange
        string[] sortedArray = ["a", "b", "c", "d", "e"];

        // Act
        sortedArray.BubbleSort();

        // Assert
        Assert.Equal(sortedArray, sortedArray);
    }

    [Fact]
    public void SelectionSort_SortedArray_ReturnsSameArray()
    {
        // Arrange
        string[] sortedArray = ["a", "b", "c", "d", "e"];

        // Act
        sortedArray.SelectionSort();

        // Assert
        Assert.Equal(sortedArray, sortedArray);
    }

    [Theory]
    [InlineData("ATL", 0)] // Existing airport, middle element
    [InlineData("JFK", 2)] // Existing airport, last element
    public void BinarySearch_Airports_Existing_MiddleOrLastElement_ReturnsCorrectIndex(string target, int expectedIndex)
    {
        //Arrange
        Array.Sort(_airports);

        // Act
        int resultIndex = _airports.BinarySearch(target, 0, _airports.Length - 1);

        // Assert
        Assert.Equal(expectedIndex, resultIndex);
    }

    [Fact]
    public void BinarySearch_Airports_OneElementArray_ReturnsCorrectIndex()
    {
        // Arrange
        string[] oneElementArray = ["JFK"];

        // Act
        int resultIndex = oneElementArray.BinarySearch("JFK", 0, oneElementArray.Length - 1);

        // Assert
        Assert.Equal(0, resultIndex);
    }

    [Fact]
    public void BinarySearch_Airports_NoElements_ReturnsNegativeOne()
    {
        // Arrange
        string[] emptyArray = [];

        // Act
        int resultIndex = emptyArray.BinarySearch("JFK", 0, emptyArray.Length - 1);

        // Assert
        Assert.Equal(-1, resultIndex);
    }

    [Theory]
    [InlineData("Delta", 1)] // Existing airline, middle element
    [InlineData("JetBlue", 2)] // Existing airline, last element
    public void BinarySearch_Airlines_Existing_MiddleOrLastElement_ReturnsCorrectIndex(string target, int expectedIndex)
    {
        //Arrange
        Array.Sort(_airlines);

        // Act
        int resultIndex = _airlines.BinarySearch(target, 0, _airlines.Length - 1);

        // Assert
        Assert.Equal(expectedIndex, resultIndex);
    }

    [Fact]
    public void BinarySearch_Airlines_OneElementArray_ReturnsCorrectIndex()
    {
        // Arrange
        string[] oneElementArray = ["Delta"];

        // Act
        int resultIndex = oneElementArray.BinarySearch("Delta", 0, oneElementArray.Length - 1);

        // Assert
        Assert.Equal(0, resultIndex);
    }

    [Fact]
    public void BinarySearch_Airlines_NoElements_ReturnsNegativeOne()
    {
        // Arrange
        string[] emptyArray = [];

        // Act
        int resultIndex = emptyArray.BinarySearch("Delta", 0, emptyArray.Length - 1);

        // Assert
        Assert.Equal(-1, resultIndex);
    }

    [Theory]
    [InlineData("UA789", 4)] // Existing flight, middle element
    [InlineData("JB202", 2)] // Existing flight, last element
    public void BinarySearch_Flights_Existing_MiddleOrLastElement_ReturnsCorrectIndex(string target, int expectedIndex)
    {
        // Arrange
        Array.Sort(_flights);

        // Act
        int resultIndex = _flights.BinarySearch(target, 0, _flights.Length - 1);

        // Assert
        Assert.Equal(expectedIndex, resultIndex);
    }

    [Fact]
    public void BinarySearch_Flights_OneElementArray_ReturnsCorrectIndex()
    {
        // Arrange
        string[] oneElementArray = ["SW101"];

        // Act
        int resultIndex = oneElementArray.BinarySearch("SW101", 0, oneElementArray.Length - 1);

        // Assert
        Assert.Equal(0, resultIndex);
    }

    [Fact]
    public void BinarySearch_Flights_NoElements_ReturnsNegativeOne()
    {
        // Arrange
        string[] emptyArray = [];

        // Act
        int resultIndex = emptyArray.BinarySearch("SW101", 0, emptyArray.Length - 1);

        // Assert
        Assert.Equal(-1, resultIndex);
    }

    [Theory]
    [InlineData("ORD", 2)] // Existing airport, middle element
    [InlineData("DFW", 4)] // Existing airport, last element
    public void LinearSearch_Airports_Existing_MiddleOrLastElement_ReturnsCorrectIndex(string target, int expectedIndex)
    {
        // Act
        int resultIndex = _airports.LinearSearch(target);

        // Assert
        Assert.Equal(expectedIndex, resultIndex);
    }

    [Fact]
    public void LinearSearch_Airports_OneElementArray_ReturnsCorrectIndex()
    {
        // Arrange
        string[] oneElementArray = ["JFK"];

        // Act
        int resultIndex = oneElementArray.LinearSearch("JFK");

        // Assert
        Assert.Equal(0, resultIndex);
    }

    [Fact]
    public void LinearSearch_Airports_NoElements_ReturnsNegativeOne()
    {
        // Arrange
        string[] emptyArray = [];

        // Act
        int resultIndex = emptyArray.LinearSearch("JFK");

        // Assert
        Assert.Equal(-1, resultIndex);
    }

    [Theory]
    [InlineData("Delta", 0)] // Existing airline, middle element
    [InlineData("JetBlue", 4)] // Existing airline, last element
    public void LinearSearch_Airlines_Existing_MiddleOrLastElement_ReturnsCorrectIndex(string target, int expectedIndex)
    {
        // Act
        int resultIndex = _airlines.LinearSearch(target);

        // Assert
        Assert.Equal(expectedIndex, resultIndex);
    }


    [Fact]
    public void LinearSearch_Airlines_OneElementArray_ReturnsCorrectIndex()
    {
        // Arrange
        string[] oneElementArray = ["Delta"];

        // Act
        int resultIndex = oneElementArray.LinearSearch("Delta");

        // Assert
        Assert.Equal(0, resultIndex);
    }


    [Fact]
    public void LinearSearch_Airlines_NoElements_ReturnsNegativeOne()
    {
        // Arrange
        string[] emptyArray = [];

        // Act
        int resultIndex = emptyArray.LinearSearch("Delta");

        // Assert
        Assert.Equal(-1, resultIndex);
    }

    [Theory]
    [InlineData("SW101", 3)] // Existing flight, middle element
    [InlineData("JB202", 4)] // Existing flight, last element
    public void LinearSearch_Flights_Existing_MiddleOrLastElement_ReturnsCorrectIndex(string target, int expectedIndex)
    {
        // Act
        int resultIndex = _flights.LinearSearch(target);

        // Assert
        Assert.Equal(expectedIndex, resultIndex);
    }


    [Fact]
    public void LinearSearch_Flights_OneElementArray_ReturnsCorrectIndex()
    {
        // Arrange
        string[] oneElementArray = ["SW101"];

        // Act
        int resultIndex = oneElementArray.LinearSearch("SW101");

        // Assert
        Assert.Equal(0, resultIndex);
    }


    [Fact]
    public void LinearSearch_Flights_NoElements_ReturnsNegativeOne()
    {
        // Arrange
        string[] emptyArray = [];

        // Act
        int resultIndex = emptyArray.LinearSearch("SW101");

        // Assert
        Assert.Equal(-1, resultIndex);
    }
}