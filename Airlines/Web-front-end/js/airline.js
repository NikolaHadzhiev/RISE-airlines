window.addEventListener('load', function () {
    let formSection = document.getElementById('form-section');
    let showTableButton = document.getElementById('show-table-button');

    formSection.style.display = 'none'; // Hide form initially

    let showFormButton = document.getElementById('show-form-button');
    showFormButton.onclick = function () {
        toggleFormVisibility();
    };
    showTableButton.onclick = function () {
        toggleTableVisibility();
    };

    let table = document.querySelector('.scrollable-table table');
    let rows = table.getElementsByTagName('tr');

    if (rows.length > 3) { // Check if more than 3 items in table
        createShowMoreButton(table, rows);
    }
});

function toggleFormVisibility() {
    let formSection = document.getElementById('form-section');
    let showFormButton = document.getElementById('show-form-button');
    formSection.style.display = (formSection.style.display === 'none') ? 'flex' : 'none';
    showFormButton.textContent = (formSection.style.display === 'none') ? 'Show Form' : 'Hide Form';
}

function toggleTableVisibility() {
    let tableSection = document.querySelector('.table-section');
    let showTableButton = document.getElementById('show-table-button');
    tableSection.style.display = (tableSection.style.display === 'none') ? 'flex' : 'none';
    showTableButton.textContent = (tableSection.style.display === 'none') ? 'Show Table' : 'Hide Table';
}

function createShowMoreButton(table, rows) {
    let showMoreButton = document.createElement('button');
    showMoreButton.id = 'show-more-button';
    showMoreButton.textContent = 'Show more';
    showMoreButton.style.display = 'block'; // Display "Show more" button
    showMoreButton.classList.add("orange-button");
    table.parentNode.insertBefore(showMoreButton, table.nextSibling);

    for (let i = 4; i < rows.length; i++) {
        rows[i].style.display = 'none'; // Hide rows initially after the third one
    }

    showMoreButton.onclick = function () {
        for (let i = 4; i < rows.length; i++) {
            rows[i].style.display = (rows[i].style.display === 'none') ? '' : 'none';
        }
        showMoreButton.textContent = (showMoreButton.textContent === 'Show more') ? 'Hide' : 'Show more';
    };
}
