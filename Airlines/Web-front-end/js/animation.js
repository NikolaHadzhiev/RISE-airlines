// Fade-in effect for specific section when the page loads
window.addEventListener('load', function () {
    let sectionToAnimate = document.querySelector('.scrollable-table');
    fadeIn(sectionToAnimate, 2000); // Adjust duration as needed
});

// Function to animate fade-in effect
function fadeIn(element, duration) {
    let opacity = 0;
    let interval = 50;
    let gap = interval / duration;

    function fadeInLoop() {
        opacity += gap;
        element.style.opacity = opacity;

        if (opacity >= 1) {
            clearInterval(fading);
        }
    }

    let fading = setInterval(fadeInLoop, interval);
}

// Scroll listener for the table
document.querySelector('.scrollable-table').addEventListener('scroll', function () {
    let goUpButton = document.getElementById('go-up-button');

    // Check if the scroll position of the table is greater than 500px
    if (this.scrollTop > 500) {
        goUpButton.classList.remove('hidden');
    } else {
        goUpButton.classList.add('hidden');
    }
});

// Smooth scroll to the top of the table when "go up" button is clicked
document.getElementById('go-up-button').addEventListener('click', function () {
    document.querySelector('.scrollable-table').scrollTo({
        top: 0,
        behavior: 'smooth'
    });
});



// Function to show the full-page loader and disable scrolling for 2 seconds
function showFullPageLoader() {
    // Disable scrolling by setting overflow to hidden
    document.body.style.overflow = 'hidden';

    // Create the backdrop element
    let backdrop = document.createElement('div');
    backdrop.classList.add('loader-backdrop');

    // Create the loader element
    let loader = document.createElement('div');
    loader.classList.add('loader');

    // Append the loader to the backdrop
    backdrop.appendChild(loader);

    // Append the backdrop to the document body
    document.body.appendChild(backdrop);

    // Set a timeout to hide the loader after 2 seconds
    setTimeout(hideFullPageLoader, 2000);
}

// Function to hide the full-page loader
function hideFullPageLoader() {
    document.body.style.overflow = 'auto';

    // Remove the backdrop element from the document body
    let backdrop = document.querySelector('.loader-backdrop');
    if (backdrop) {
        backdrop.remove();
    }
}

showFullPageLoader();