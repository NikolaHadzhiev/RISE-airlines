import { getAllAirports, getAirportById, createAirport, updateAirport, deleteAirport } from '../httpRequests.js';

document.addEventListener('DOMContentLoaded', () => {
    const airportTableBody = document.getElementById('airport-table-body');
    const form = document.querySelector('.form-container');
    const submitButton = form.querySelector('.form-submit');
    let isEditing = false;
    let editingId = null;

    async function loadAirports() {
        try {
            const airports = await getAllAirports();
            airportTableBody.innerHTML = ''; // Clear existing rows
            airports.forEach(airport => {
                const row = document.createElement('tr');
                row.setAttribute('data-id', airport.id);

                row.innerHTML = `
                    <td>${airport.name}</td>
                    <td>${airport.country}</td>
                    <td>${airport.city}</td>
                    <td>${airport.code}</td>
                    <td>${airport.runwaysCount}</td>
                    <td>${new Date(airport.founded).toLocaleDateString()}</td>
                    <td>
                        <button class="edit-button">Edit</button>
                        <button class="delete-button">Delete</button>
                    </td>
                `;

                row.querySelector('.edit-button').addEventListener('click', () => startEditing(airport));
                row.querySelector('.delete-button').addEventListener('click', () => removeAirport(airport.id));
                airportTableBody.appendChild(row);
            });
        } catch (error) {
            console.error('Failed to load airports:', error);
        }
    }

    async function addOrUpdateAirport(event) {
        event.preventDefault();
        const formData = new FormData(form);
        const airportData = Object.fromEntries(formData.entries());

        try {
            if (isEditing) {
                airportData.id = editingId;
                await updateAirport(airportData);
                isEditing = false;
                editingId = null;
            } else {
                await createAirport(airportData);
            }
            form.reset();
            submitButton.value = 'Submit'; // Reset button text
            await loadAirports();
        } catch (error) {
            console.error('Failed to save airport:', error);
        }
    }

    async function startEditing(airport) {
        isEditing = true;
        editingId = airport.id;
        form.querySelector('#name').value = airport.name;
        form.querySelector('#country').value = airport.country;
        form.querySelector('#city').value = airport.city;
        form.querySelector('#code').value = airport.code;
        form.querySelector('#runwaysCount').value = airport.runwaysCount;
        form.querySelector('#founded').value = airport.founded.split('T')[0]; // Format date for input
        submitButton.value = 'Update';
    }

    async function removeAirport(id) {
        if (confirm('Are you sure you want to delete this airport?')) {
            try {
                await deleteAirport(id);
                await loadAirports();
            } catch (error) {
                console.error('Failed to delete airport:', error);
            }
        }
    }

    form.addEventListener('submit', addOrUpdateAirport);

    loadAirports();
});
