// Function to validate flight number field
function validateFlightNumber() {
    let flightNumberInput = document.getElementById('flightNumber');
    let flightNumberError = document.getElementById('flightNumber-error');
    let flightNumber = flightNumberInput.value.trim();

    if (flightNumber === '') {
        flightNumberError.textContent = 'Flight number is required';
        return false;
    } else if (flightNumber.length > 6) {
        flightNumberError.textContent = 'Flight number should be at most 6 characters long';
        return false;
    } else {
        flightNumberError.textContent = '';
        return true;
    }
}

// Function to validate price field
function validatePrice() {
    let priceInput = document.getElementById('price');
    let priceError = document.getElementById('price-error');
    let price = priceInput.value.trim();

    if (price === '') {
        priceError.textContent = 'Price is required';
        return false;
    } else if (isNaN(price) || parseFloat(price) <= 0) {
        priceError.textContent = 'Price should be a positive number';
        return false;
    } else {
        priceError.textContent = '';
        return true;
    }
}

// Function to validate departure date and time field
function validateDepartureDateTime() {
    let departureInput = document.getElementById('departureDatetime');
    let departureError = document.getElementById('departureDateTime-error');
    let departureDateTime = departureInput.value.trim();

    if (departureDateTime === '') {
        departureError.textContent = 'Departure date and time are required';
        return false;
    } else {
        // Validate against custom departure date not past rule
        let departureDate = new Date(departureDateTime);
        let currentDate = new Date();
        if (departureDate < currentDate) {
            departureError.textContent = 'Departure date and time cannot be in the past';
            return false;
        } else {
            departureError.textContent = '';
            return true;
        }
    }
}

// Function to validate arrival date and time field
function validateArrivalDateTime() {
    let arrivalInput = document.getElementById('arrivalDatetime');
    let arrivalError = document.getElementById('arrivalDatetime-error');
    let arrivalDateTime = arrivalInput.value.trim();

    if (!arrivalDateTime) {
        arrivalError.textContent = 'Arrival date and time are required';
        return false;
    } else {
        let currentDate = new Date();
        let arrivalDate = new Date(arrivalDateTime);

        if (arrivalDate < currentDate) {
            arrivalError.textContent = 'Arrival date and time cannot be in the past';
            return false;
        } else {
            arrivalError.textContent = '';
            return true;
        }
    }
}

// Function to validate from airport name field
function validateFromAirportName() {
    let fromAirportInput = document.getElementById('fromAirportName');
    let fromAirportError = document.getElementById('fromAirportName-error');
    let fromAirportName = fromAirportInput.value.trim();

    if (fromAirportName === '') {
        fromAirportError.textContent = 'From airport name is required';
        return false;
    } else if (!/^[A-Za-z\s]+$/.test(fromAirportName)) {
        fromAirportError.textContent = 'From airport name should only contain letters and spaces';
        return false;
    } else {
        fromAirportError.textContent = '';
        return true;
    }
}

// Function to validate to airport name field
function validateToAirportName() {
    let toAirportInput = document.getElementById('toAirportName');
    let toAirportError = document.getElementById('toAirportName-error');
    let toAirportName = toAirportInput.value.trim();

    if (toAirportName === '') {
        toAirportError.textContent = 'To airport name is required';
        return false;
    } else if (!/^[A-Za-z\s]+$/.test(toAirportName)) {
        toAirportError.textContent = 'To airport name should only contain letters and spaces';
        return false;
    } else {
        toAirportError.textContent = '';
        return true;
    }
}

// Function to enable/disable submit button based on form validity
function validateForm() {
    let validators = [validateFlightNumber, validatePrice, validateDepartureDateTime, validateArrivalDateTime, validateFromAirportName, validateToAirportName];
    let isValid = validators.every(function (validator) {
        return validator();
    });

    let submitButton = document.querySelector('.form-submit');
    submitButton.disabled = !isValid;
}

// Bind validation functions to form inputs' events
window.addEventListener('load', function () {
    let inputs = document.querySelectorAll('input[type="text"], input[type="number"], input[type="datetime-local"]');
    inputs.forEach(function (input) {
        input.addEventListener('blur', validateForm);
    });

    let submitButton = document.querySelector('.form-submit');
    submitButton.disabled = true; // Disable submit button initially
});
