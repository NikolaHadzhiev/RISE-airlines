// Function to validate name field
function validateName() {
    let nameInput = document.getElementById('name');
    let nameError = document.getElementById('name-error');
    let name = nameInput.value.trim();

    if (name === '') {
        nameError.textContent = 'Name is required';
        return false;
    } else if (!/^[A-Za-z\s]+$/.test(name)) {
        nameError.textContent = 'Name should only contain letters and spaces';
        return false;
    } else {
        nameError.textContent = '';
        return true;
    }
}

// Function to validate founded date field
function validateFounded() {
    let foundedInput = document.getElementById('founded');
    let foundedError = document.getElementById('founded-error');
    let founded = foundedInput.value.trim();

    if (founded === '') {
        foundedError.textContent = 'Founded date is required';
        return false;
    } else {
        foundedError.textContent = '';
        return true;
    }
}

// Function to validate fleet size field
function validateFleetSize() {
    let fleetSizeInput = document.getElementById('fleetSize');
    let fleetSizeError = document.getElementById('fleetSize-error');
    let fleetSize = fleetSizeInput.value.trim();

    if (fleetSize === '') {
        fleetSizeError.textContent = 'Fleet size is required';
        return false;
    } else if (!/^\d+$/.test(fleetSize)) {
        fleetSizeError.textContent = 'Fleet size should be a positive integer';
        return false;
    } else {
        fleetSizeError.textContent = '';
        return true;
    }
}

// Function to validate description field
function validateDescription() {
    let descriptionInput = document.getElementById('description');
    let descriptionError = document.getElementById('description-error');
    let description = descriptionInput.value.trim();

    if (description === '') {
        descriptionError.textContent = 'Description is required';
        return false;
    } else if (!/^[A-Za-z\s]+$/.test(description)) {
        descriptionError.textContent = 'Description should only contain letters and spaces';
        return false;
    } else {
        descriptionError.textContent = '';
        return true;
    }
}

// Function to enable/disable submit button based on form validity
function validateForm() {
    let validators = [validateName, validateFounded, validateFleetSize, validateDescription];
    let isValid = validators.every(function (validator) {
        return validator();
    });

    let submitButton = document.querySelector('.form-submit');
    submitButton.disabled = !isValid;
}

// Bind validation functions to form inputs' events
window.addEventListener('load', function () {
    let inputs = document.querySelectorAll('input[type="text"], input[type="number"], input[type="date"]');
    inputs.forEach(function (input) {
        input.addEventListener('blur', validateForm);
    });

    let submitButton = document.querySelector('.form-submit');
    submitButton.disabled = true; // Disable submit button initially
});
