// Function to validate name field
function validateName() {
    let nameInput = document.getElementById('name');
    let nameError = document.getElementById('name-error');
    let name = nameInput.value.trim();

    if (name === '') {
        nameError.textContent = 'Name is required';
        return false;
    } else if (!/^[A-Za-z\s]+$/.test(name)) {
        nameError.textContent = 'Name should only contain letters and spaces';
        return false;
    } else {
        nameError.textContent = '';
        return true;
    }
}

// Function to validate country field
function validateCountry() {
    let countryInput = document.getElementById('country');
    let countryError = document.getElementById('country-error');
    let country = countryInput.value.trim();

    if (country === '') {
        countryError.textContent = 'Country is required';
        return false;
    } else if (!/^[A-Za-z\s]+$/.test(country)) {
        countryError.textContent = 'Country should only contain letters and spaces';
        return false;
    } else {
        countryError.textContent = '';
        return true;
    }
}

// Function to validate city field
function validateCity() {
    let cityInput = document.getElementById('city');
    let cityError = document.getElementById('city-error');
    let city = cityInput.value.trim();

    if (city === '') {
        cityError.textContent = 'City is required';
        return false;
    } else if (!/^[A-Za-z\s]+$/.test(city)) {
        cityError.textContent = 'City should only contain letters and spaces';
        return false;
    } else {
        cityError.textContent = '';
        return true;
    }
}

// Function to validate code field
function validateCode() {
    let codeInput = document.getElementById('code');
    let codeError = document.getElementById('code-error');
    let code = codeInput.value.trim();

    if (code === '') {
        codeError.textContent = 'Code is required';
        return false;
    } else if (!/^[A-Za-z]+$/.test(code)) {
        codeError.textContent = 'Code should only contain letters';
        return false;
    } else if (code.length !== 3) {
        codeError.textContent = 'Code should be exactly 3 characters long';
        return false;
    } else {
        codeError.textContent = '';
        return true;
    }
}

// Function to validate runways count field
function validateRunwaysCount() {
    let runwaysCountInput = document.getElementById('runwaysCount');
    let runwaysCountError = document.getElementById('runwaysCount-error');
    let runwaysCount = runwaysCountInput.value.trim();

    if (runwaysCount === '') {
        runwaysCountError.textContent = 'Runways count is required';
        return false;
    } else if (!/^\d+$/.test(runwaysCount)) {
        runwaysCountError.textContent = 'Runways count should be a positive integer';
        return false;
    } else {
        runwaysCountError.textContent = '';
        return true;
    }
}

// Function to validate founded date field
function validateFounded() {
    let foundedInput = document.getElementById('founded');
    let foundedError = document.getElementById('founded-error');
    let founded = foundedInput.value.trim();

    if (founded === '') {
        foundedError.textContent = 'Founded date is required';
        return false;
    } else {
        foundedError.textContent = '';
        return true;
    }
}

// Function to enable/disable submit button based on form validity
function validateForm() {
    let validators = [validateName, validateCountry, validateCity, validateCode, validateRunwaysCount, validateFounded];
    let isValid = validators.every(function (validator) {
        return validator();
    });

    let submitButton = document.querySelector('.form-submit');
    submitButton.disabled = !isValid;
}

// Bind validation functions to form inputs' events
window.addEventListener('load', function () {
    let inputs = document.querySelectorAll('input[type="text"], input[type="number"], input[type="date"]');
    inputs.forEach(function (input) {
        input.addEventListener('blur', validateForm);
    });

    let submitButton = document.querySelector('.form-submit');
    submitButton.disabled = true; // Disable submit button initially
});
