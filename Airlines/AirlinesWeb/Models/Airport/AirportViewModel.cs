﻿using System.ComponentModel.DataAnnotations;

namespace AirlinesWeb.Models.Airport;

public class AirportViewModel
{
    [Key]
    public int ID { get; set; }

    [Required(ErrorMessage = "Name is required")]
    [RegularExpression(@"^[A-Za-z\s]+$", ErrorMessage = "Name should only contain letters and spaces")]
    public string? Name { get; set; }

    [Required(ErrorMessage = "Country is required")]
    [RegularExpression(@"^[A-Za-z\s]+$", ErrorMessage = "Country should only contain letters and spaces")]
    public string? Country { get; set; }

    [Required(ErrorMessage = "City is required")]
    [RegularExpression(@"^[A-Za-z\s]+$", ErrorMessage = "City should only contain letters and spaces")]
    public string? City { get; set; }

    [Required(ErrorMessage = "Code is required")]
    [RegularExpression(@"^[A-Za-z]+$", ErrorMessage = "Code should only contain letters")]
    [StringLength(3, MinimumLength = 3, ErrorMessage = "Code should be exactly 3 characters long")]
    public string? Code { get; set; }

    [Required(ErrorMessage = "Runways count is required")]
    [RegularExpression(@"^\d+$", ErrorMessage = "Runways count should be a positive integer")]
    public int? RunwaysCount { get; set; }

    [DataType(DataType.Date)]
    [Required(ErrorMessage = "Founded date is required")]
    public DateOnly? Founded { get; set; }
}
