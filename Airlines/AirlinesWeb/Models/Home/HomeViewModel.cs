﻿namespace AirlinesWeb.Models.Home;

public class HomeViewModel
{
    public int AirportCount { get; set; }
    public int AirlineCount { get; set; }
    public int FlightCount { get; set; }
}
