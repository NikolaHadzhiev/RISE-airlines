﻿using System.ComponentModel.DataAnnotations;

namespace AirlinesWeb.Models.Airline;

public class AirlineViewModel
{
    [Key]
    public int ID { get; set; }

    [Required(ErrorMessage = "Name is required")]
    [StringLength(5, ErrorMessage = "Name should be at most 5 characters long")]
    [RegularExpression(@"^[A-Za-z\s]+$", ErrorMessage = "Name should only contain letters and spaces")]
    public string? Name { get; set; }

    [DataType(DataType.Date)]
    [Required(ErrorMessage = "Founded date is required")]
    public DateOnly? Founded { get; set; }

    [Required(ErrorMessage = "Fleet size is required")]
    [Range(1, int.MaxValue, ErrorMessage = "Fleet size should be a positive integer")]
    public int FleetSize { get; set; }

    [Required(ErrorMessage = "Description is required")]
    [StringLength(1000, ErrorMessage = "Description should be at most 1000 characters long")]
    [RegularExpression(@"^[A-Za-z\s]+$", ErrorMessage = "Description should only contain letters and spaces")]
    public string? Description { get; set; }
}
