﻿using System.ComponentModel.DataAnnotations;

namespace AirlinesWeb.Models.Flight.Validation;
#pragma warning disable CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).

public class DepartureDateNotPastAttribute : ValidationAttribute
{
    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        DateTime? departureDateTime = (DateTime?)value;

        if (departureDateTime == null || departureDateTime >= DateTime.Today)
        {
            return ValidationResult.Success!;
        }

        return new ValidationResult("Departure date and time cannot be in the past");
    }
}
