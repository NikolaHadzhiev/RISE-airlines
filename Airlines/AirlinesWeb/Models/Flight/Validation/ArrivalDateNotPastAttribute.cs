﻿using System.ComponentModel.DataAnnotations;

namespace AirlinesWeb.Models.Flight.Validation;

#pragma warning disable CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
public class ArrivalDateNotPastAttribute : ValidationAttribute
{
    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        DateTime? arrivalDateTime = (DateTime?)value;

        if (arrivalDateTime == null || arrivalDateTime >= DateTime.Today)
        {
            return ValidationResult.Success!;
        }

        return new ValidationResult("Arrival date and time cannot be in the past");
    }
}
