using Airlines.Business.Services;
using Airlines.Persistence.Basic.DataContext;
using Airlines.Persistence.Basic.Repositories.AirlineRepository;
using Airlines.Persistence.Basic.Repositories.AirportRepository;
using Airlines.Persistence.Basic.Repositories.FlightRepository;
using Microsoft.EntityFrameworkCore;
using System.Text.Json.Serialization;

namespace AirlinesWeb;

#pragma warning disable IDE0058 // Expression value is never used
public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddControllers().AddJsonOptions(options =>
        {
            options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
        });

        // Check if the environment variable is already set
        string runningLocally = Environment.GetEnvironmentVariable("RUNNING_LOCALLY")!;

        // If the environment variable is not set, set it to true
        if (string.IsNullOrEmpty(runningLocally))
        {
            Environment.SetEnvironmentVariable("RUNNING_LOCALLY", "true");
        }

        // Check if running locally
        bool isLocal = Environment.GetEnvironmentVariable("RUNNING_LOCALLY") == "true";

        string connectionString;

        if (isLocal)
        {
            // Use the local connection string
            connectionString = builder.Configuration.GetConnectionString("RISE_Airlines_DB") ?? throw new InvalidOperationException("Connection string 'RISE_Airlines_DB' not found.");
        }
        else
        {
            // Use the container connection string
            connectionString = builder.Configuration.GetConnectionString("RISE_Airlines_DB_Docker") ?? throw new InvalidOperationException("Connection string 'RISE_Airlines_DB_Dockeer' not found.");
        }

        // Add services to the container.
        builder.Services.AddDbContext<RISEAirlinesContext>(options =>
            options.UseSqlServer(connectionString));

        builder.Services.AddControllersWithViews();

        builder.Services.AddScoped<IAirportRepositoryDB, AirportRepositoryDB>();
        builder.Services.AddScoped<IAirlineRepositoryDB, AirlineRepositoryDB>();
        builder.Services.AddScoped<IFlightRepositoryDB, FlightRepositoryDB>();
        builder.Services.AddScoped<IAirportService, AirportService>();
        builder.Services.AddScoped<IAirlineService, AirlineService>();
        builder.Services.AddScoped<IFlightService, FlightService>();

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (!app.Environment.IsDevelopment())
        {
            app.UseExceptionHandler("/Home/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseAuthorization();

        app.MapControllerRoute(
            name: "default",
            pattern: "{controller=Home}/{action=Index}/{id?}");

        app.Run();
    }
}
