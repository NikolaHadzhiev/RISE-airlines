using Airlines.Business.Services;
using AirlinesWeb.Models;
using AirlinesWeb.Models.Home;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace AirlinesWeb.Controllers;

#pragma warning disable IDE0022 // Use expression body for method
#pragma warning disable IDE0052 // Remove unread private members
#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable IDE0290 // Use primary constructor
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly IAirportService _airportService;
    private readonly IAirlineService _airlineService;
    private readonly IFlightService _flightService;
    public HomeController(ILogger<HomeController> logger, IAirportService airportService, IAirlineService airlineService, IFlightService flightService)
    {
        _logger = logger;
        _airportService = airportService;
        _airlineService = airlineService;
        _flightService = flightService;
    }

    public async Task<IActionResult> Index()
    {
        var airport = await _airportService.GetAllAirportsFromDB();
        var airline = await _airlineService.GetAllAirlinesFromDB();
        var flight = await _flightService.GetAllFlightsFromDB();

        HomeViewModel homeViewModel = new HomeViewModel
        {
            AirportCount = airport.Count,
            AirlineCount = airline.Count,
            FlightCount = flight.Count
        };

        return View(homeViewModel);
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
