﻿using Airlines.Business.Services;
using Airlines.Business.Utils;
using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;
using AirlinesWeb.Models.Airport;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesWeb.Controllers;

#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0021 // Use expression body for constructor
public class AirportController : Controller
{
    private readonly IAirportService _airportService;

    public AirportController(IAirportService airportService)
    {
        _airportService = airportService;
    }

    public async Task<IActionResult> Index()
    {
        var nameFilterValue = HttpContext.Request.Query["nameFilter"].ToString();
        var countryFilterValue = HttpContext.Request.Query["countryFilter"].ToString();
        var cityFilterValue = HttpContext.Request.Query["cityFilter"].ToString();
        var codeFilterValue = HttpContext.Request.Query["codeFilter"].ToString();
        var runwaysFilterValue = HttpContext.Request.Query["runwaysFilter"].ToString();
        var foundedFilterValue = HttpContext.Request.Query["foundedFilter"].ToString();

        var searchValue = HttpContext.Request.Query["search"].ToString();
        var searchFilter = HttpContext.Request.Query["searchFilter"].ToString();

        List<Airport> allAirports = !string.IsNullOrEmpty(searchValue)
            ? await _airportService.GetAllAirportsFromDB(searchValue, searchFilter)
            : await _airportService.GetAllAirportsFromDB();

        allAirports = ServicesUtils.ApplySorting(allAirports, nameFilterValue, x => x.Name);
        allAirports = ServicesUtils.ApplySorting(allAirports, countryFilterValue, x => x.Country);
        allAirports = ServicesUtils.ApplySorting(allAirports, cityFilterValue, x => x.City);
        allAirports = ServicesUtils.ApplySorting(allAirports, codeFilterValue, x => x.Code);
        allAirports = ServicesUtils.ApplySorting(allAirports, runwaysFilterValue, x => x.RunwaysCount!);
        allAirports = ServicesUtils.ApplySorting(allAirports, foundedFilterValue, x => x.Founded!);

        List<AirportViewModel> allAiprortsViewModels = allAirports.Select(x => new AirportViewModel
        {
            ID = x.ID,
            Name = x.Name,
            City = x.City,
            Code = x.Code,
            Country = x.Country,
            RunwaysCount = x.RunwaysCount,
            Founded = x.Founded
        }).ToList();


        return View((allAiprortsViewModels, new AirportViewModel()));
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([FromForm] AirportViewModel airportViewModel)
    {
        AirportDTO airportDTO = new AirportDTO(
            airportViewModel.Name,
            airportViewModel.Country,
            airportViewModel.City,
            airportViewModel.Code,
            airportViewModel.RunwaysCount,
            airportViewModel.Founded
           );

        List<Airport> allAirports = await _airportService.GetAllAirportsFromDB();

        List<AirportViewModel> allAiprortsViewModels = allAirports.Select(x => new AirportViewModel
        {
            ID = x.ID,
            Name = x.Name,
            City = x.City,
            Code = x.Code,
            Country = x.Country,
            RunwaysCount = x.RunwaysCount,
            Founded = x.Founded
        }).ToList();


        if (!ModelState.IsValid)
        {
            ViewBag.Error = "Invalid airport create entity";

            return View(nameof(Index), (allAiprortsViewModels, airportViewModel));
        }

        if (await _airportService.AddAirportToDB(airportDTO) == null)
        {
            ViewBag.Error = "Invalid airport create entity";
            return View(nameof(Index), (allAiprortsViewModels, airportViewModel));
        }

        return RedirectToAction(nameof(Index));
    }
}

