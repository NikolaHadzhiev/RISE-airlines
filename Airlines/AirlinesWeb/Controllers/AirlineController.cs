﻿using Airlines.Business.Services;
using Airlines.Business.Utils;
using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;
using AirlinesWeb.Models.Airline;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesWeb.Controllers;

#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0021 // Use expression body for constructor

public class AirlineController : Controller
{
    private readonly IAirlineService _airlineService;

    public AirlineController(IAirlineService airlineService)
    {
        _airlineService = airlineService;
    }

    public async Task<IActionResult> Index()
    {
        var nameFilterValue = HttpContext.Request.Query["nameFilter"].ToString();
        var foundedFilterValue = HttpContext.Request.Query["foundedFilter"].ToString();
        var fleetSizeFilterValue = HttpContext.Request.Query["fleetSizeFilter"].ToString();
        var descriptionFilterValue = HttpContext.Request.Query["descriptionFilter"].ToString();
        var searchValue = HttpContext.Request.Query["search"].ToString();
        var searchFilterValue = HttpContext.Request.Query["searchFilter"].ToString();

        List<Airline> allAirlines = !string.IsNullOrEmpty(searchValue)
            ? await _airlineService.GetAllAirlinesFromDB(searchValue, searchFilterValue)
            : await _airlineService.GetAllAirlinesFromDB();

        allAirlines = ServicesUtils.ApplySorting(allAirlines, nameFilterValue, x => x.Name);
        allAirlines = ServicesUtils.ApplySorting(allAirlines, foundedFilterValue, x => x.Founded!);
        allAirlines = ServicesUtils.ApplySorting(allAirlines, fleetSizeFilterValue, x => x.FleetSize);
        allAirlines = ServicesUtils.ApplySorting(allAirlines, descriptionFilterValue, x => x.Description);

        List<AirlineViewModel> allAirlinesViewModel = allAirlines.Select(x => new AirlineViewModel
        {
            ID = x.ID,
            Name = x.Name,
            Founded = x.Founded,
            FleetSize = x.FleetSize,
            Description = x.Description
        }).ToList();


        return View((allAirlinesViewModel, new AirlineViewModel()));
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([FromForm] AirlineViewModel airlineViewModel)
    {
        AirlineDTO airlineDTO = new AirlineDTO(
            airlineViewModel.Name,
            airlineViewModel.Founded,
            airlineViewModel.FleetSize,
            airlineViewModel.Description
        );

        List<Airline> allAirlines = await _airlineService.GetAllAirlinesFromDB();

        List<AirlineViewModel> allAirlinesViewModel = allAirlines.Select(x => new AirlineViewModel
        {
            ID = x.ID,
            Name = x.Name,
            Founded = x.Founded,
            FleetSize = x.FleetSize,
            Description = x.Description
        }).ToList();

        if (!ModelState.IsValid)
        {
            ViewBag.Error = "Invalid airline create entity";

            return View(nameof(Index), (allAirlinesViewModel, airlineViewModel));
        }

        if (await _airlineService.AddAirlineToDB(airlineDTO) == null)
        {
            ViewBag.Error = "Invalid airline create entity";
            return View(nameof(Index), (allAirlinesViewModel, airlineViewModel));
        }

        return RedirectToAction(nameof(Index));
    }
}
