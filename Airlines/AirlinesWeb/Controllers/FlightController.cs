﻿using Airlines.Business.Services;
using Airlines.Business.Utils;
using Airlines.Persistence.Basic.DTO;
using Airlines.Persistence.Basic.EntitiesDB;
using AirlinesWeb.Models.Airport;
using AirlinesWeb.Models.Flight;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesWeb.Controllers;

#pragma warning disable IDE0290 // Use primary constructor

public class FlightController : Controller
{
    private readonly IFlightService _flightService;
    private readonly IAirportService _airportService;

    public FlightController(IFlightService flightService, IAirportService airportService)
    {
        _flightService = flightService;
        _airportService = airportService;
    }

    public async Task<IActionResult> Index()
    {
        var flightNumberFilterValue = HttpContext.Request.Query["flightNumberFilter"].ToString();
        var fromFilterValue = HttpContext.Request.Query["fromFilter"].ToString();
        var toFilterValue = HttpContext.Request.Query["toFilter"].ToString();
        var priceFilterValue = HttpContext.Request.Query["priceFilter"].ToString();
        var departureFilterValue = HttpContext.Request.Query["departureFilter"].ToString();
        var arrivalFilterValue = HttpContext.Request.Query["arrivalFilter"].ToString();
        var searchValue = HttpContext.Request.Query["search"].ToString();
        var searchFilterValue = HttpContext.Request.Query["searchFilter"].ToString();

        List<Flight> allFlights = !string.IsNullOrEmpty(searchValue)
            ? await _flightService.GetAllFlightsFromDB(searchValue, searchFilterValue)
            : await _flightService.GetAllFlightsFromDB();

        allFlights = ServicesUtils.ApplySorting(allFlights, flightNumberFilterValue, x => x.FlightNumber);
        allFlights = ServicesUtils.ApplySorting(allFlights, fromFilterValue, x => x.FromAirport.Name);
        allFlights = ServicesUtils.ApplySorting(allFlights, toFilterValue, x => x.ToAirport.Name);
        allFlights = ServicesUtils.ApplySorting(allFlights, priceFilterValue, x => x.Price);
        allFlights = ServicesUtils.ApplySorting(allFlights, departureFilterValue, x => x.DepartureDateTime);
        allFlights = ServicesUtils.ApplySorting(allFlights, arrivalFilterValue, x => x.ArrivalDateTime);

        List<FlightViewModel> allFlightsViewModels = allFlights.Select(x => new FlightViewModel
        {
            ID = x.ID,
            FlightNumber = x.FlightNumber,
            Price = x.Price,
            DepartureDateTime = x.DepartureDateTime,
            ArrivalDateTime = x.ArrivalDateTime,
            FromAirport = new AirportViewModel
            {
                ID = x.FromAirport.ID,
                City = x.FromAirport.City,
                Country = x.FromAirport.Country,
                Code = x.FromAirport.Code,
                Founded = x.FromAirport.Founded,
                Name = x.FromAirport.Name,
                RunwaysCount = x.FromAirport.RunwaysCount
            },
            ToAirport = new AirportViewModel
            {
                ID = x.ToAirport.ID,
                City = x.ToAirport.City,
                Country = x.ToAirport.Country,
                Code = x.ToAirport.Code,
                Founded = x.ToAirport.Founded,
                Name = x.ToAirport.Name,
                RunwaysCount = x.ToAirport.RunwaysCount
            }
        }).ToList();

        return View((allFlightsViewModels, new FlightViewModel()));
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([FromForm] FlightViewModel flightViewModel)
    {
        var allAiports = await _airportService.GetAllAirportsFromDB();
        List<Flight> allFlights = await _flightService.GetAllFlightsFromDB();

        flightViewModel.Airports = allAiports.Select(a => new AirportViewModel
        {
            ID = a.ID,
            City = a.City,
            Country = a.Country,
            Code = a.Code,
            Founded = a.Founded,
            Name = a.Name,
            RunwaysCount = a.RunwaysCount
        }).ToList();

        //Since we are not having logic for
        //AircraftModelId and AirlineId for the moment
        //I am assiging 15 as AircraftModelId and 17 for AirlineId for each record

        FlightDTO flightDTO = new FlightDTO(
           flightViewModel.FlightNumber!,
           15,
           17,
           flightViewModel.Airports.FirstOrDefault(a => a.Name!.Contains(flightViewModel.FromAirportName!, StringComparison.OrdinalIgnoreCase))!.ID,
           flightViewModel.Airports.FirstOrDefault(a => a.Name!.Contains(flightViewModel.ToAirportName!, StringComparison.OrdinalIgnoreCase))!.ID,
           flightViewModel.Price,
           flightViewModel.DepartureDateTime,
           flightViewModel.ArrivalDateTime
           );

        List<FlightViewModel> allFlightsViewModel = allFlights.Select(x => new FlightViewModel
        {
            ID = x.ID,
            FlightNumber = x.FlightNumber,
            Price = x.Price,
            DepartureDateTime = x.DepartureDateTime,
            ArrivalDateTime = x.ArrivalDateTime,
            FromAirport = new AirportViewModel
            {
                ID = x.FromAirport.ID,
                City = x.FromAirport.City,
                Country = x.FromAirport.Country,
                Code = x.FromAirport.Code,
                Founded = x.FromAirport.Founded,
                Name = x.FromAirport.Name,
                RunwaysCount = x.FromAirport.RunwaysCount
            },
            ToAirport = new AirportViewModel
            {
                ID = x.ToAirport.ID,
                City = x.ToAirport.City,
                Country = x.ToAirport.Country,
                Code = x.ToAirport.Code,
                Founded = x.ToAirport.Founded,
                Name = x.ToAirport.Name,
                RunwaysCount = x.ToAirport.RunwaysCount
            }
        }).ToList();

        if (!ModelState.IsValid)
        {
            ViewBag.Error = "Invalid flight create entity";

            return View(nameof(Index), (allFlightsViewModel, flightViewModel));
        }

        if (await _flightService.AddFlightToDB(flightDTO) == null)
        {
            ViewBag.Error = "Invalid flight create entity";
            return View(nameof(Index), (allFlightsViewModel, flightViewModel));
        }

        return RedirectToAction(nameof(Index));
    }
}
