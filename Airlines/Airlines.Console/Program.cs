﻿using Airlines.Business.Utils.FileDataUtilsMethods;
using Airlines.Business.Utils.ReadUtilsMethods;
using Airlines.Persistence.Basic.Custom;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Entities.Aircrafts;

namespace AirlinesConsole;

public class Program
{
    private static void Main()
    {
        //IRepositoryFactory repositoryFactory = new RepositoryFactory();

        //AirportService airportService = new AirportService(repositoryFactory.GetAirportRepository());
        //AirlineService airlineService = new AirlineService(repositoryFactory.GetAirlineRepository());
        //FlightService flightService = new FlightService(repositoryFactory.GetFlightRepository());

        //List<Airport> airportList = airportService.GetAllAirportsFromDB().Result;
        //Airport airport = airportService.GetAirportByIdFromDB(7).Result;
        //List<Airport> airportsByCountry = airportService.GetAirportsByCountryFromDB("USA").Result;
        //Airport addedAirport = airportService.AddAirportToDB(new AirportDTO("Test Airport", "USA", "Dallas", "DFW", 5, DateOnly.FromDateTime(DateTime.Now))).Result;
        //Airport updatedAirport = airportService.UpdateAirportToDB(23, new AirportDTO("Test Update Airport", "China", "Beijing", "PEK", 3, DateOnly.FromDateTime(DateTime.Now))).Result;
        //airportService.DeleteAirportFromDB(23);

        //airlineService.GetAllAirlinesFromDB();
        //airlineService.GetAirlineByIdFromDB(7);
        //airlineService.AddAirlineToDB(new AirlineDTO("TestA", DateOnly.FromDateTime(DateTime.Now), 10, "Test Airline Description"));
        //airlineService.UpdateAirlineToDB(25, new AirlineDTO("TestU", DateOnly.FromDateTime(DateTime.Now), 15, "Test Update Airline Description"));
        //airlineService.DeleteAilineFromDB(25);

        //flightService.GetAllFlightsFromDB();
        //flightService.GetFlightByIdFromDB(30);
        //flightService.AddFlightToDB(new FlightDTO("FL999", 10, 3, 5, 6, 300, new DateTime(2024, 5, 20, 8, 30, 0), new DateTime(2024, 5, 20, 15, 30, 0)));
        //flightService.UpdateFlightToDB(30, new FlightDTO(19, 10, 9, 3, 700, new DateTime(2025, 5, 20, 8, 30, 0), new DateTime(2025, 5, 20, 15, 30, 0)));
        //flightService.UpdateFlightDepartureTimeToDB(30);
        //flightService.UpdateFlightArrivalTimeToDB(30);
        //flightService.UpdateFlightAircraftModelToDB(30, 1);
        //flightService.UpdateFlightPricelToDB(30, 100);
        //flightService.DeleteFlightFromDB(30);

        Dictionary<string, AirportCSV> airports;
        Dictionary<string, AirlineCSV> airlines;

        FlightLinkedList flights;
        FlightLinkedList flightRoute = new FlightLinkedList();
        Dictionary<string, FlightCSV> flightsForTree;
        List<string> flightRouteListForTree;

        Dictionary<string, HashSet<AirportCSV>> airportsByCity;
        Dictionary<string, HashSet<AirportCSV>> airportsByCountry;

        Dictionary<string, Aircraft> aircrafts;

        try
        {
            FileDataUtils.ReadDataFromFileAirport(out airports, out airportsByCity, out airportsByCountry);
            FileDataUtils.ReadDataFromFileAirline(out airlines);
            FileDataUtils.ReadDataFromAircraft(out aircrafts);
            FileDataUtils.ReadDataFromFileFlight(out flights, aircrafts, out flightsForTree);
            FileDataUtils.ReadDataFromFlightRoute(out flightRouteListForTree, flights);

            FileDataUtils.PrintDictionaryData(airports);
            FileDataUtils.PrintDictionaryData(airlines);
            FileDataUtils.PrintDictionaryData(aircrafts);
            flights.DisplayFlights();

            Console.WriteLine("FlightCSV Route from flightRoute.csv: ");
            FileDataUtils.PrintListData(flightRouteListForTree);

            ReadUtils.ReadUserCommands(airports, airlines, flights, flightRoute, flightsForTree, flightRouteListForTree, airportsByCity, airportsByCountry);
        }
        catch (FileNotFoundException ex)
        {
            Console.WriteLine(ex.Message);
            Console.WriteLine();
        }
        catch (NullReferenceException)
        {
            Console.WriteLine("Data provided is either empty or invalid. Please check your files.");
            Console.WriteLine();
        }
    }
}
